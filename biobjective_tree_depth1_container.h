#pragma once

#include <vector>

struct BiObjectiveTreeDepth1
{
	BiObjectiveTreeDepth1() :
		root_feature(UINT32_MAX),
		left_label(UINT32_MAX),
		right_label(UINT32_MAX),
		num_false_positives(UINT32_MAX),
		num_false_negatives(UINT32_MAX)
	{}

	BiObjectiveTreeDepth1(uint32_t root_feature, uint32_t left_label, uint32_t right_label, uint32_t num_false_positives, uint32_t num_false_negatives) :
		root_feature(root_feature),
		left_label(left_label),
		right_label(right_label),
		num_false_positives(num_false_positives),
		num_false_negatives(num_false_negatives)
	{}

	uint32_t root_feature;
	uint32_t num_false_positives, num_false_negatives;
	uint32_t left_label, right_label;
};

class BiObjectiveTreeDepth1Container
{
public:
	BiObjectiveTreeDepth1Container();
	void AddTree(BiObjectiveTreeDepth1 &tree); //if the input solution is not dominated by one of the stored solutions, adds it to the contained. Note that this may remove some of the stored solutions if they are dominated by the input solution
	void Clear();
	std::vector<BiObjectiveTreeDepth1>& GetStoresTrees();

private:
	bool IsTreeDominated(BiObjectiveTreeDepth1& sol1, BiObjectiveTreeDepth1& sol2); //returns true if sol1 is dominated by sol2

	std::vector<BiObjectiveTreeDepth1> stored_trees_;
};