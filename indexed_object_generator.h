#pragma once

template <class IndexedObjectType>
class IndexedObjectGenerator
{
	class IndexedObjectIterator;

public:
	explicit IndexedObjectGenerator(uint32_t num_objects) :num_objects_(num_objects) {}

	uint32_t NumObjects() const { return num_objects_; }
	IndexedObjectType operator[](uint32_t index) const { assert(index < num_objects_); return IndexedObjectType(index); }
	typename IndexedObjectIterator begin() const { return IndexedObjectIterator(0, num_objects_); }
	typename IndexedObjectIterator end() const { return IndexedObjectIterator(num_objects_, num_objects_); }
private:
	uint32_t num_objects_;

	class IndexedObjectIterator
	{
	public:
		IndexedObjectIterator(uint32_t starting_index, uint32_t num_objects) :current_index_(starting_index), num_objects_(num_objects) {}

		IndexedObjectType operator*() const { return IndexedObjectType(current_index_); }
		IndexedObjectIterator &operator++() { current_index_++; return *this; }
		bool operator==(const IndexedObjectIterator &right_hand_side) const { assert(num_objects_ == right_hand_side.num_objects_); return current_index_ == right_hand_side.current_index_; }
		bool operator!=(const IndexedObjectIterator &right_hand_side) const { return !(*this == right_hand_side); }
		/*LabelIterator operator++(int)
		{
		LabelIterator return_iterator(current_index_, num_labels_);
		current_index_++;
		return return_iterator;
		}*/

	private:
		uint32_t current_index_;
		uint32_t num_objects_;
	};
};