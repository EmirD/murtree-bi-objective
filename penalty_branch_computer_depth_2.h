#pragma once

#include "frequency_counts_2d.h"
#include "feature_vector_difference_computer.h"
#include "learning_data.h"

#include <stdint.h>

namespace DecisionTreeSolverMur
{
	class PenaltyBranchComputerDepth2
	{
	public:
		PenaltyBranchComputerDepth2(uint32_t num_labels, uint32_t num_features, uint32_t num_total_data_points);

		void Initialise(LearningData &data);

		uint32_t PenaltyBranchOneOne(uint32_t feature1, uint32_t feature2);
		uint32_t PenaltyBranchOneZero(uint32_t feature1, uint32_t feature2);
		uint32_t PenaltyBranchZeroOne(uint32_t feature1, uint32_t feature2);
		uint32_t PenaltyBranchZeroZero(uint32_t feature1, uint32_t feature2);

		//private:

		uint32_t LabelOneOne(uint32_t label, uint32_t feature1, uint32_t feature2);
		uint32_t LabelOneZero(uint32_t label, uint32_t feature1, uint32_t feature2);
		uint32_t LabelZeroOne(uint32_t label, uint32_t feature1, uint32_t feature2);
		uint32_t LabelZeroZero(uint32_t label, uint32_t feature1, uint32_t feature2);

		void UpdateCounts(LearningData &data, int val);
		uint32_t ComputeBranchPenalty(uint32_t feature1, uint32_t feature2, uint32_t (PenaltyBranchComputerDepth2::*count_function)(uint32_t label, uint32_t feature1, uint32_t feature2));

		FeatureVectorDifferenceComputer difference_computer_;
		std::vector<uint32_t> label_counts_;
		uint32_t num_labels_;
		FrequencyCounts2D counts_;
	};
}