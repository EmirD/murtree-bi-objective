#include "penalty_branch_computer_depth_2.h"

namespace DecisionTreeSolverMur
{
	PenaltyBranchComputerDepth2::PenaltyBranchComputerDepth2(uint32_t num_labels, uint32_t num_features, uint32_t num_total_data_points) :
		difference_computer_(num_labels, num_features, num_total_data_points),
		label_counts_(num_labels),
		num_labels_(num_labels),
		counts_(num_labels, num_features)		
	{}

	void PenaltyBranchComputerDepth2::Initialise(LearningData & data)
	{
		bool using_incremental_updates = difference_computer_.seen_last_time_.size() > 0;

		for (uint32_t label = 0; label < num_labels_; label++){ label_counts_[label] = data.NumInstancesForLabel(label); }
		
		double magic_number = 1;
		if (using_incremental_updates) difference_computer_.Update(data);

		if (using_incremental_updates && magic_number * difference_computer_.NumDifferences() < data.Size())
		{//incremental update
			UpdateCounts(difference_computer_.data_to_remove_, -1);
			UpdateCounts(difference_computer_.data_to_add_, +1);			
		}
		else
		{//recompute from scratch
			counts_.ResetToZeros();
			UpdateCounts(data, +1);			
		}
	}

	uint32_t PenaltyBranchComputerDepth2::PenaltyBranchOneOne(uint32_t feature1, uint32_t feature2)
	{
		return ComputeBranchPenalty(feature1, feature2, &PenaltyBranchComputerDepth2::LabelOneOne);
	}

	uint32_t PenaltyBranchComputerDepth2::PenaltyBranchOneZero(uint32_t feature1, uint32_t feature2)
	{
		return ComputeBranchPenalty(feature1, feature2, &PenaltyBranchComputerDepth2::LabelOneZero);
	}

	uint32_t PenaltyBranchComputerDepth2::PenaltyBranchZeroOne(uint32_t feature1, uint32_t feature2)
	{
		return ComputeBranchPenalty(feature1, feature2, &PenaltyBranchComputerDepth2::LabelZeroOne);
	}

	uint32_t PenaltyBranchComputerDepth2::PenaltyBranchZeroZero(uint32_t feature1, uint32_t feature2)
	{
		return ComputeBranchPenalty(feature1, feature2, &PenaltyBranchComputerDepth2::LabelZeroZero);
	}

	uint32_t PenaltyBranchComputerDepth2::LabelOneOne(uint32_t label, uint32_t feature1, uint32_t feature2)
	{
		return counts_(label, feature1, feature2);
	}

	uint32_t PenaltyBranchComputerDepth2::LabelOneZero(uint32_t label, uint32_t feature1, uint32_t feature2)
	{
		return counts_(label, feature1, feature1) - counts_(label, feature1, feature2);
	}

	uint32_t PenaltyBranchComputerDepth2::LabelZeroOne(uint32_t label, uint32_t feature1, uint32_t feature2)
	{
		return counts_(label, feature2, feature2) - counts_(label, feature1, feature2);
	}

	uint32_t PenaltyBranchComputerDepth2::LabelZeroZero(uint32_t label, uint32_t feature1, uint32_t feature2)
	{
		return label_counts_[label] - (LabelOneOne(label, feature1, feature1) + LabelOneOne(label, feature2, feature2) - LabelOneOne(label, feature1, feature2));
	}

	void PenaltyBranchComputerDepth2::UpdateCounts(LearningData & data, int val)
	{
		for(uint32_t label = 0; label < num_labels_; label++)
		{
			for (const FeatureVectorBinary &fv : data[label])
			{
				uint32_t num_present_features = fv.NumPresentFeatures();
				for (uint32_t i = 0; i < num_present_features; i++)
				{
					uint32_t feature1 = fv.GetJthPresentFeature(i);
					for (uint32_t j = i; j < num_present_features; j++)
					{
						uint32_t feature2 = fv.GetJthPresentFeature(j);
						assert(feature1 <= feature2);
						assert(val > 0 || val < 0 && counts_(label, feature1, feature2) > 0);
						counts_(label, feature1, feature2) += val;
					}
				}
			}
		}			
	}

	uint32_t PenaltyBranchComputerDepth2::ComputeBranchPenalty(uint32_t feature1, uint32_t feature2, uint32_t (PenaltyBranchComputerDepth2::*count_function)(uint32_t label, uint32_t feature1, uint32_t feature2))
	{
		uint32_t sum_of_instances = 0;
		for (uint32_t label = 0; label < num_labels_; label++) { sum_of_instances += ((this->*count_function)(label, feature1, feature2)); }

		uint32_t min_penalty = sum_of_instances - ((this->*count_function)(0, feature1, feature2));
		for (uint32_t label = 1; label < num_labels_; label++)
		{
			min_penalty = std::min((sum_of_instances - (this->*count_function)(label, feature1, feature2)), min_penalty);
		}
		return min_penalty;
	}
}


