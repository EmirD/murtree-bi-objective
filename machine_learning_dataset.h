#pragma once

#include "learning_data.h"

#include <vector>
#include <string>
#include <set>
#include <numeric>
#include <iostream>
#include <fstream>
#include <map>

struct CategoricalInstance
{
	std::vector<std::string> features;
	std::string target;
};

struct BinaryInstance
{
	std::vector<bool> features;
	std::string target;
};

class CategoricalDatasetToBinaryDataset
{
public:

	//the file must have a header with "target" and the target column must appear before other columns with 't'
	static std::vector<CategoricalInstance> ReadDataset(std::string file_location, bool verbose = true)
	{
		std::ifstream file(file_location.c_str());
		
		assert(file);

		if (!file) { std::cout << "Error: File " << file_location << " does not exist!\n"; exit(1); }

		if (verbose) std::cout << file_location << std::endl;

		std::vector<CategoricalInstance> data;

		char c('a');
		size_t num_features = 0;
		size_t target_location = UINT32_MAX;
		while (file.get(c)){//c != '\n') {

			if (c == '\n') { break; }

			if (c == 'T' && target_location == UINT32_MAX) { assert(target_location == UINT32_MAX); target_location = num_features; }
			num_features += (c == ',');
		}

		//read the types of features
		uint32_t n_entries_per_row = num_features + 1; //+1 for the label

		std::string s;
		while (file)
		{
			CategoricalInstance instance;
			std::vector<std::string> row;
			for (size_t i = 0; i < n_entries_per_row; i++)
			{
				file >> c; 
				if (c == '\n') { break; } //empty row
				assert(c == '\"');
				getline(file, s, '"');
				row.push_back(s);
				file.get(c);			
				if (!file) { assert(i == 0); break; } //reached the end
				if (i == n_entries_per_row - 1) { assert(c == '\n'); }
				else { assert(c == ','); }
			}

			if (row.size() == 0) { break; }

			instance.target = row[target_location];
			row.erase(row.begin() + target_location);
			instance.features = row;
			
			data.push_back(instance);
		}
		return data;
	}
	
	static std::vector<BinaryInstance> Binarise(std::vector<CategoricalInstance> &data, bool is_classification_problem)
	{
		std::vector<BinaryInstance> binarised_dataset(data.size());

		//for classification we rename classes to go from 0..n-1, where n is the number of labels
		std::map<std::string, std::string> renamed_targets;
		std::set<std::string> targets;
		for (CategoricalInstance &instance : data) { targets.insert(instance.target); }
		if (is_classification_problem)
		{
			uint32_t counter = 0;
			for (auto target : targets)
			{
				std::pair<std::string, std::string> p;
				p.first = target;
				p.second = std::to_string(counter);
				counter++;

				renamed_targets.insert(p);
			}
		}
		else
		{
			//for regression we keep the identity
			for (auto target : targets)
			{
				std::pair<std::string, std::string> p;
				p.first = target;
				p.second = target;

				renamed_targets.insert(p);
			}
		}
		//set the targets in the dataset
		for (uint32_t i = 0; i < data.size(); i++) { binarised_dataset[i].target = renamed_targets[data[i].target]; }

		//now go for each feature, in the same order as in the original dataset, and binarise the features
		uint32_t num_features = data[0].features.size();
		for (size_t feature_index = 0; feature_index < num_features; feature_index++)
		{
			std::cout << "Processing feature: " << feature_index + 1 << "\n";

			std::set<std::string> categorical_values;
			for (CategoricalInstance &instance : data) { categorical_values.insert(instance.features[feature_index]); }
			assert(categorical_values.size() > 0);
			if (categorical_values.size() <= 1)
			{
				std::cout << "Warning: only one value for feature " << feature_index + 1 << ", ignoring this feature\n";
				continue;
			}

			std::cout << "\tValues: " << categorical_values.size() << "\n";
			std::cout << "\t\t";
			for (auto v : categorical_values) { std::cout << v << " "; }
			std::cout << "\n";

			//we are doing a one-hot encoding of categorical features

			//special case for features with only two values: we use one binary feature to represent two binary features
			if (categorical_values.size() == 2)
			{
				//for each dataset, add the binary value to the features
				std::string high_value = *categorical_values.rbegin();
				for (size_t i = 0; i < data.size(); i++)
				{
					bool binary_feature_value = (data[i].features[feature_index] == high_value);
					binarised_dataset[i].features.push_back(binary_feature_value);
				}
			}
			//n features for n categories: the i-th feature is true iff the feature has the i-th value
			else
			{
				//for each dataset, add the appropriate 0-1 features
				for (size_t i = 0; i < data.size(); i++)
				{
					std::string feature_value = data[i].features[feature_index];
					int binarised_index = std::distance(categorical_values.begin(), categorical_values.find(feature_value));
					for (size_t j = 0; j < categorical_values.size(); j++)
					{
						binarised_dataset[i].features.push_back(j == binarised_index);
					}
				}
			}
		}
		return binarised_dataset;
	}

	static void WriteBinaryDatasetToFileDLStyle(std::vector<BinaryInstance> &data, std::string file_location)
	{
		std::ofstream output_file(file_location.c_str());
		for (size_t i = 0; i < data.size(); i++)
		{
			BinaryInstance &instance = data[i];
			output_file << instance.target;
			for (bool b : instance.features) { output_file << " " << b; }
			if (i + 1 != data.size()) { output_file << "\n"; }
		}
	}

};


