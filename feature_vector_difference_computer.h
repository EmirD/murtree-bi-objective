#pragma once

#include "learning_data.h"

#include <stdint.h>

namespace DecisionTreeSolverMur
{
	//todo hax: assumes that the feature vectors have unique IDs in the range [0, num_data_points)
	class FeatureVectorDifferenceComputer
	{
	public:
		FeatureVectorDifferenceComputer(uint32_t num_labels, uint32_t num_features, uint32_t num_feature_vectors);

		void Update(LearningData &data);
		uint32_t ProbeDifference(LearningData &data);

		uint32_t NumDifferences();

		//private:
		std::vector<bool> seen_last_time_, feature_vector_present_; //[i] -> indicates if the feature vector with ID has been seen last time
		LearningData data_last_time_, data_to_remove_, data_to_add_;

		uint32_t num_labels_, num_features_;
	};
}