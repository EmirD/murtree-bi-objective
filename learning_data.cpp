#include "learning_data.h"
#include "DecisionTreeSolverMur/runtime_assert.h"

namespace DecisionTreeSolverMur
{
	LearningData::LearningData(uint32_t num_labels, uint32_t num_features) :
		instances(num_labels),
		num_labels_(num_labels),
		num_features_(num_features)
	{}

	uint32_t LearningData::NumLabels() const
	{
		return num_labels_;
	}

	uint32_t LearningData::NumInstancesForLabel(uint32_t label) const
	{
		return instances[label].size();
	}

	const std::vector<FeatureVectorBinary>& LearningData::operator[](uint32_t label) const
	{
		return instances[label];
	}

	uint32_t LearningData::NumFeatures() const
	{
		return num_features_;
	}

	uint32_t LearningData::MaxFeatureVectorID() const
	{
		uint32_t max_id = 0;
		for (const std::vector<FeatureVectorBinary> &i : instances)
		{
			for (FeatureVectorBinary fv : i)
			{
				max_id = std::max(max_id, fv.GetID());
			}
		}
		return max_id + 1;
	}

	void LearningData::SplitData(uint32_t feature, LearningData & data_without_feature, LearningData & data_with_feature)
	{
		assert(data_without_feature.IsEmpty() && data_with_feature.IsEmpty());

		for (uint32_t label = 0; label < num_labels_; label++)
		{
			for (FeatureVectorBinary fv : instances[label])
			{
				if (fv[feature]) 
				{
					data_with_feature.AddFeatureVector(fv, label); 
				}
				else 
				{ 
					data_without_feature.AddFeatureVector(fv, label); 
				}
			}
		}
	}

	void LearningData::AddFeatureVector(FeatureVectorBinary fv, uint32_t label)
	{
		instances[label].push_back(fv);
	}

	uint32_t LearningData::Size() const
	{
		return std::accumulate(instances.begin(), instances.end(), 0, [](uint32_t sum, auto &v)->uint32_t { return sum + uint32_t(v.size()); });
	}

	bool LearningData::IsEmpty() const
	{
		return Size() == 0;
	}

	uint32_t LearningData::ComputeClassificationCost() const
	{
		uint32_t best_label = GetMajorityLabel();
		return Size() - NumInstancesForLabel(best_label);
	}

	/*uint32_t LearningData::ComputeNumFalsePositives() const
	{
		runtime_assert(NumLabels() == 2);
		if (instances[0].size() > instances[1].size())
		{
			return 0;
		}
		else
		{
			return instances[0].size();
		}
	}

	uint32_t LearningData::ComputeNumFalseNegatives() const
	{
		runtime_assert(NumLabels() == 2);
		if (instances[1].size() > instances[0].size())
		{
			return 0;
		}
		else
		{
			return instances[1].size();
		}
	}*/

	uint32_t LearningData::GetMajorityLabel() const
	{
		uint32_t best_label = 0;
		for (uint32_t label = 1; label < num_labels_; label++)
		{
			if (NumInstancesForLabel(best_label) < NumInstancesForLabel(label))
			{
				best_label = label;
			}
		}
		return best_label;
	}

	void LearningData::Clear()
	{
		for (uint32_t label = 0; label < num_labels_; label++) { instances[label].clear(); }
	}

	double LearningData::ComputeSparsity() const
	{
		if (Size() == 0) { return 0.0; }
		double sum_sparsity = 0.0;
		for (const std::vector<FeatureVectorBinary> &i : instances)
		{
			for (const FeatureVectorBinary &fv : i)
			{
				sum_sparsity += fv.Sparsity();
			}
		}
		return sum_sparsity / (Size());
	}

	void LearningData::PrintStats()
	{
		std::cout << "Num features: " << NumFeatures() << "\n";
		std::cout << "Size: " << Size() << "\n";
		for (uint32_t label = 0; label < num_labels_; label++)
		{
			std::cout << "\tLabel " << label << ": " << instances[label].size() << "\n";
		}
		std::cout << "\tSparsity: " << ComputeSparsity() << "\n";
	}


	std::ostream & operator<<(std::ostream & os, const LearningData & data)
	{
		os << "Number of instances: " << data.Size();

		for (uint32_t label = 0; label < data.NumLabels(); label++)
		{
			uint32_t counter_instances = 0;
			os << "\nNumber of instances with label " << label << ": " << data[label].size();
			for (FeatureVectorBinary fv : data[label])
			{
				os << "\n\t" << counter_instances++ << ":\t" << fv;
			}
		}
		return os;
	}

}