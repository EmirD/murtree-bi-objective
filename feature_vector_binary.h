#pragma once

#include <vector>
#include <fstream>

namespace DecisionTreeSolverMur
{
	//important: the class uses reference counting to avoid deep copies
	class FeatureVectorBinary
	{
	public:
		FeatureVectorBinary(const std::vector<bool> &feature_values);
		FeatureVectorBinary(const FeatureVectorBinary &reference);
		FeatureVectorBinary& operator=(const FeatureVectorBinary &right_hand_side);
		~FeatureVectorBinary();		

		bool operator[](uint32_t feature) const;
		uint32_t GetJthPresentFeature(uint32_t j) const;
		uint32_t NumPresentFeatures() const;
		uint32_t GetID() const;
		double Sparsity() const;

		std::vector<uint32_t>::const_iterator begin() const;
		std::vector<uint32_t>::const_iterator end() const;

		friend std::ostream& operator<<(std::ostream& os, const FeatureVectorBinary& fv);

	private:

		void DecrementReferenceCounterAndCleanUp();

		struct InternalData
		{
			InternalData() :reference_counter(NULL) {};
			~InternalData() { delete reference_counter; }

			std::vector<bool> is_feature_present; //[i] indicates if the feature is true or false, i.e., if it is present in present_Features.
			std::vector<uint32_t> present_features;
			uint32_t id;
			uint32_t *reference_counter;
		} *internal_data_;
	};
}