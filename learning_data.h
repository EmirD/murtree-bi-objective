#pragma once

#include "feature_vector_binary.h"

#include <vector>
#include <iostream>
#include <assert.h>
#include <algorithm>
#include <fstream>
#include <numeric>

namespace DecisionTreeSolverMur
{
	struct LearningData
	{
		LearningData(uint32_t num_labels, uint32_t num_features);

		uint32_t NumLabels() const;
		uint32_t NumInstancesForLabel(uint32_t label) const;
		uint32_t NumFeatures() const;
		uint32_t MaxFeatureVectorID() const;
		uint32_t Size() const;
		bool IsEmpty() const;
		double ComputeSparsity() const;
		const std::vector<FeatureVectorBinary> &operator[](uint32_t label) const;
		uint32_t ComputeClassificationCost() const;
		//uint32_t ComputeNumFalsePositives() const;
		//uint32_t ComputeNumFalseNegatives() const;

		uint32_t GetMajorityLabel() const;

		void Clear();
		void AddFeatureVector(FeatureVectorBinary fv, uint32_t label);
		void SplitData(uint32_t feature, LearningData &data_without_feature, LearningData &data_with_feature);
		
		void PrintStats();

		friend std::ostream& operator<<(std::ostream& os, const LearningData& data);

		std::vector<std::vector<FeatureVectorBinary> > instances;//the learning data is partitioned based on the labels, i.e., instances[label] is the vector of feature vectors with label 'label'
		
		uint32_t num_labels_, num_features_;
	};
}