#pragma once

#include <stdint.h>
#include <fstream>

namespace DecisionTreeSolverMur
{
	//it is implemented as a symmetric matirix
	class FrequencyCounts2D
	{
	public:
		FrequencyCounts2D(uint32_t num_labels, uint32_t num_features);
		~FrequencyCounts2D();

		uint32_t operator()(uint32_t label, uint32_t feature1, uint32_t feature2) const;
		uint32_t &operator()(uint32_t label, uint32_t feature1, uint32_t feature2);

		void ResetToZeros();

		bool operator==(const FrequencyCounts2D &reference);
		friend std::ostream& operator<<(std::ostream& os, const FrequencyCounts2D& fq);

	private:
		uint32_t NumElements() const;
		uint32_t IndexSymmetricMatrix(uint32_t index_row, uint32_t index_column) const;

		uint32_t *counts_;
		uint32_t num_labels_, num_features_;
	};
}