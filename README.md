# README #

This is the code that was used in the following paper: "Optimal Decision Trees for Nonlinear Metrics", Demirović and Stuckey, AAAI'21.

# UPDATE #

We released a newer and more general version of the algorithm, please see the following: https://pypi.org/project/pystreed/.

### How to run the code?

The current code was tested in Windows with Visual Studio. For other platforms, you would need to compile on your own. The code was built entirely from scratch using only native C++ libraries, so there should be no problem compiling for other platforms. Make sure to compile in Release mode with all optimisations, Debug mode can be significantly slower.

Once compiled, compute the Paret front of decision trees with a maximum depth and a maximum number of feature nodes:

exec.exe -depth 4 -max-num-feature-nodes 15 -file anneal.txt

Note that the both the depth and number of feature nodes play a role in the runtime of the algorithm. Usually depth can go up to 3, 4, or 5, anything beyond those values may take too much time. The algorithm is usually fastest when considering 2^{depth} - 1 number of features nodes, and usually takes most time at 2^{depth-1} - 1 feature nodes.

If you would like to do tuning, e.g., find all trees by varying the depth and number of feature nodes and select the best depth-size configuration based on a train-test set, let me know and I will write the instructions. This functionality is already in the code but I still need to write a description on how to do it.

### Datasets

Input files are as follows:

* All features and labels are zero-one. Non-binarised instances must be converted into binary form.
* Each line is one instance. The first number is the label while the rest are features.

Datasets used in the paper were taken from other repositories and binarised/converted to a new format if necessary. 

* DatasetsDL: https://github.com/aia-uclouvain/pydl8.5/tree/master/datasets, repository of the paper "Learning optimal decision trees using caching branch-and-bound search", Aglin, Nijssen, Schaus, AAAI'20 
* DatasetsNina were obtained from Dr Nina Narodytska and their paper "Learning Optimal Decision Trees with SAT", Narodytska, Ignatiev, Pereira, Marques-Silva, IJCAI'18
* Original benchmarks datasetsNL: https://github.com/SiccoVerwer/binoct (or UCI machine learning repository), repository of the paper "Learning optimal classification trees using a binary linear program formulation", Verwer, Zhang, AAAI'19.  Here we provide the binarised version of these datasests.
* DatasetsHu were taken from https://github.com/xiyanghu/OSDT, the repository of the paper "Optimal sparse decision trees", Hu, Rudin, Seltzer, NeurIPS'19.

For issues, let me know at e.demirovic@tudelft.nl.