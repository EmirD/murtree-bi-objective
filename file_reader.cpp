#include "file_reader.h"
#include <fstream>
#include <assert.h>

namespace DecisionTreeSolverMur
{
	std::vector<TrainTestSplit> DecisionTreeSolverMur::FileReader::ReadSplits(std::string data_file, std::string splits_location_prefix)
	{//splits_location_prefix' + '_train[i].txt'
		LearningDataRaw raw_data = ReadDataDL_Raw(data_file);
		std::vector<TrainTestSplit> splits;
		for (int k = 0; k < 5; k++)
		{
			std::string file_test = splits_location_prefix + "_test" + std::to_string(k) + ".txt";
			std::string file_train = splits_location_prefix + "_train" + std::to_string(k) + ".txt";
			TrainTestSplit split(raw_data.num_labels, raw_data.num_features);
			split.testing_set = ReadSplits_ReadPartitionFile(raw_data, file_test);
			split.training_set = ReadSplits_ReadPartitionFile(raw_data, file_train);
			splits.push_back(split);
		}
		return splits;
	}

	LearningData FileReader::ReadSplits_ReadPartitionFile(LearningDataRaw &data, std::string file)
	{
		std::ifstream input(file.c_str());
		LearningData partition(data.num_labels, data.num_features);
		assert(input);
		int index;
		while (input >> index)
		{
			partition.AddFeatureVector(data.data[index].features, data.data[index].label);
		}
		return partition;
	}
}
