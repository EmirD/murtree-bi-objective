#pragma once

#include <vector>
#include "learning_data.h"

namespace DecisionTreeSolverMur
{
	class RandomGeneratorDecisionTreeData
	{
	public:
		static std::vector<bool> GenerateRandomBooleanVector(size_t size)
		{
			std::vector<bool> return_value(size, false);
			for (size_t i = 0; i < return_value.size(); i++)
			{
				return_value[i] = (rand() % 100 >= 50);
			}
			return return_value;
		}

		static LearningData GenerateRandomData(size_t num_points, uint32_t num_labels, size_t num_features)
		{
			LearningData data(num_labels, num_features);

			for (size_t i = 0; i < num_points; i++)
			{
				std::vector<bool> features = GenerateRandomBooleanVector(num_features);
				uint32_t label = (rand() % num_labels);
				data.AddFeatureVector(FeatureVectorBinary(features), label);
			}

			return data;
		}
	};
}