#include "optimal_decision_tree_constructor_depth2.h"
#include "DecisionTreeSolverMur/tester.h"
#include "random_generator_decision_tree_data.h"
#include "file_reader.h"
#include "DecisionTreeSolverMur/solver.h"
#include "DecisionTreeSolverMur/decision_tree_learning.h"
#include "DecisionTreeSolverMur/tester.h"
#include "biobjective_classification_tree_constructor_depth1.h"
#include "DecisionTreeSolverMur/specialised_optimal_binary_classification_tree_solver_depth2.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <time.h>
#include <algorithm>

using DecisionTreeSolverMur::LearningData;
using DecisionTreeSolverMur::FeatureVectorBinary;
using DecisionTreeSolverMur::Parameters;
using DecisionTreeSolverMur::FileReader;
using DecisionTreeSolverMur::SolverParameters;


void PrintTreeIntoFile(DecisionTreeSolverMur::DecisionTree *decision_tree_, std::string dump_location)
{
	std::ofstream out(dump_location.c_str());
	out << "?\n";
	uint32_t num_total_nodes = decision_tree_->nodes_.size(); //I guess this is the highest ID present
	for (uint32_t i = 0; i < num_total_nodes; i++)
	{
		if (decision_tree_->left_child_[i] != NULL)
		{
			out << decision_tree_->left_child_[i]->Index() << " ";
		}
		else
		{
			out << "-1 ";
		}
	}
	out << "\n";

	for (uint32_t i = 0; i < num_total_nodes; i++)
	{
		if (decision_tree_->right_child_[i] != NULL)
		{
			out << decision_tree_->right_child_[i]->Index() << " ";
		}
		else
		{
			out << "-1 ";
		}
	}
	out << "\n";

	for (uint32_t i = 0; i < num_total_nodes; i++)
	{
		uint32_t feature = decision_tree_->nodes_[i]->GetAssignedFeature();
		if (feature == UINT32_MAX) { out << "-2 "; }
		else out << feature << " ";
	}
	out << "\n";

	for (uint32_t i = 0; i < num_total_nodes; i++)
	{
		if (1 == 2 && decision_tree_->nodes_[i]->GetAssignedFeature() == UINT32_MAX)
		{
			out << "0.0 ";
		}
		else
		{
			LearningData& d = decision_tree_->nodes_[i]->data_;
			uint32_t m = 0;
			for (uint32_t label = 0; label < d.NumLabels(); label++)
			{
				m = std::max(d.NumInstancesForLabel(label), m);
			}

			if (d.Size() == 0)
			{
				out << "1.0 ";
			}
			else
			{
				out << double(m) / d.Size() << " ";
			}
			//out << "0.11 "; //impurity...dummy values
		}
	}
	out << "\n";

	out << decision_tree_->GetMaxDepth() << "\n";
	out << decision_tree_->nodes_[0]->data_.NumLabels() << "\n";
	out << decision_tree_->nodes_[0]->data_.NumLabels() << "\n";
	out << decision_tree_->nodes_[0]->data_.NumFeatures() << "\n";

	for (uint32_t i = 0; i < num_total_nodes; i++)
	{
		if (decision_tree_->nodes_[i] != NULL)
		{
			out << decision_tree_->nodes_[i]->data_.Size() << " "; //num samples
		}
		else
		{
			out << "0 ";
		}
	}
	out << "\n";

	out << "1\n"; //num outputs
	out << decision_tree_->nodes_.size() << "\n";

	for (uint32_t i = 0; i < num_total_nodes; i++)
	{
		if (decision_tree_->left_child_[i] != NULL) //thresholds
		{
			out << "0.5 ";
		}
		else
		{
			out << "-2.0 ";
		}
	}
	out << "\n";

	for (uint32_t i = 0; i < num_total_nodes; i++)
	{
		uint32_t num_labels = decision_tree_->nodes_[0]->data_.NumLabels();

		if (decision_tree_->nodes_[i] == NULL)
		{
			for (uint32_t j = 0; j < num_labels; j++)
			{
				out << "7.0 ";
			}
			out << "\n";
		}
		else
		{
			for (uint32_t j = 0; j < num_labels; j++)
			{
				out << double(decision_tree_->nodes_[i]->data_.NumInstancesForLabel(j)) << " ";
			}
			out << "\n";
		}
	}

	for (uint32_t i = 0; i < num_total_nodes; i++)
	{
		if (decision_tree_->nodes_[i] != NULL)
		{
			out << double(decision_tree_->nodes_[i]->data_.Size()) << " "; //num weighted samples
		}
		else
		{
			out << "0 ";
		}
	}
	out << "\n";
}



int main(int argc, char *argv[])
{
	bool manual_activation = false;

	unsigned int seed(3);
	std::string program;
	std::string file_location;
	LearningData *data;
	Parameters params;
	std::vector<DecisionTreeSolverMur::TrainTestSplit> splits;
	std::string splits_location_prefix = "";
	std::string hyper_parameter_stats_file = "";
	
	params.max_depth = 4;
	params.max_size = 15;
	params.unit_tree_depth = 2;
	params.node_selection_strategy = "post_order";//"best_first_variant1";//backtracking_cart1// best_first_variant1;//"best_first";//"post_order";//"best_first";// "random"; post_order_flex_larger_first
	params.feature_ordering_strategy = "in_order";

	if (argc == 1)
	{
		//std::cout << "Manual activation, no parameters supplied monkaS\n";

		manual_activation = true;

		program = "kekw";
		//file_location = "..\\datasetsFL\\parkinsons.txt";
		file_location = "..\\datasetsFL\\ilpd.txt";
		//file_location = "datasetsDL\\anneal.txt";
		//file_location = "datasetsDL\\german-credit.txt";
		//file_location = "datasetsDL\\vehicle.txt";
		//file_location = "datasetsDL\\letter.txt";		
		//file_location = "datasetsDL\\yeast.txt";
		//file_location = "datasetsDL\\pendigits.txt";		
		//file_location = "datasetsDL\\ionosphere.txt";
		//file_location = "datasetsDL\\diabetes.txt";
		//file_location = "datasetsDL\\audiology.txt";
		//file_location = "datasetsDL\\hypothyroid.txt";
		//file_location = "datasetsDL\\hepatitis.txt";
		//file_location = "datasetsDL\\soybean.txt";
		//file_location = "datasetsDL\\australian-credit.txt";
	//	file_location = "datasetsDL\\anneal.txt";
		//file_location = "datasetsNina\\australian-un.csv";
		//file_location = "datasetFair\\taiwan_binarised_caim.txt";
		//file_location = "datasetFair\\taiwan_binarised_ameva.txt";
		//file_location = "UCI\\wine_mdlp_binarised.txt";
		//file_location = "UCI\\bank_binarised.txt";
		//file_location = "DatasetsAnnachan\\rollouts_escape_room_10000_mdpl_binarised.txt";
		//file_location = "monk1_bin.txt";
		//file_location = "datasetsNina\\converted\\irish-un_converted.txt";
		//file_location = "temp_dataset.txt";
		//file_location = "datasetsNL\\binarised\\monk2_bin.txt";
		//file_location = "datasetsHu\\compas-binary.txt";
		//file_location = "datasetsHu\\fico-binary.txt";
		//file_location = "datasetsHu\\monk3-hu.txt";
		//file_location = "datasetsNina\\converted\\appendicitis-un_converted.txt";
		//file_location = "datasetsNina\\converted\\australian-un_converted.txt";
		//file_location = "datasetsNina\\converted\\cleve-un_converted.txt";
		
		params.max_depth = 3;
		params.max_size = 3;
		params.unit_tree_depth = 2;
		params.node_selection_strategy = "post_order";//"best_first_variant1";//backtracking_cart1// best_first_variant1;//"best_first";//"post_order";//"best_first";// "random"; post_order_flex_larger_first
		params.feature_ordering_strategy = "in_order";//misclassification_score_ascending// "in_order";//"information_gain_descending";//"random";
		params.cache_technique = "bucketing_hash";
		params.use_lower_bound_caching = true;
		params.use_upper_bound_caching = false;
		params.use_pruning = true;
		params.branch_length_hash_bucketing = 50;// (params.target_depth - params.unit_tree_depth);
		params.preprocess_data = false;

		//params.use_infeasibility_lower_bounding = false;
		//params.use_upper_bounding = false;
		//params.use_similiarity_lower_bound = false;
		
		//params.upper_bound_constraint = 0;
		//params.time_limit = 3;
		params.use_similiarity_lower_bound = true;
		params.use_incremental_frequency_reconstruction = true;

		if (file_location.find(".csv") != std::string::npos) { data = new LearningData(FileReader::ReadDataHao(file_location, false)); }
		else { data = new LearningData(FileReader::ReadDataDL(file_location)); }

		params.dump_file = "test_dump.txt";

		//params.use_similiarity_lower_bound = false;
		//params.use_incremental_frequency_reconstruction = false;
		
		params.hyper_tuning_mode = 0;
		splits_location_prefix = "datasetsTesting\\MurTreeSplits\\anneal";
		hyper_parameter_stats_file = "datasetsTesting\\MurTreeHyperStats\\anneal.txt_kekw.txt";
		
		//params.use_CART_objective = true;
		//params.CART_alpha = 0.01;
		//params.hyper_tuning_mode = true;		
	}
	else
	{
		program = argv[0];

		for (int i = 1; i < argc; i++)
		{
			std::string s(argv[i]);
			if (s == "-file")
			{
				file_location = argv[i + 1];
				i++;
			}
			else if (s == "-depth")
			{
				params.max_depth = atoi(argv[i + 1]);
				i++;
			}
			else if (s == "-time")
			{
				params.time_limit = atoi(argv[i + 1]);
				i++;
			}
			else if (s == "-max-num-feature-nodes")
			{
				params.max_size = atoi(argv[i + 1]);
				i++;
			}
			else if (s == "-unit-tree-depth")
			{
				params.unit_tree_depth = atoi(argv[i + 1]);
				i++;
			}
			else if (s == "-seed")
			{
				seed = atoi(argv[i + 1]);
				i++;
			}
			else if (s == "-verbose")
			{
				params.verbose = atoi(argv[i + 1]);
				i++;
			}
			else if (s == "-dump-file")
			{
				params.dump_file = std::string(argv[i + 1]);
				i++;
			}
			else if (s == "-node-selection-strategy")
			{
				params.node_selection_strategy = std::string(argv[i + 1]);
				i++;
			}
			else if (s == "-feature-ordering-strategy")
			{
				params.feature_ordering_strategy = std::string(argv[i + 1]);
				i++;
			}
			else if (s == "-similiarity-lower-bound")
			{
				params.use_similiarity_lower_bound = atoi(argv[i + 1]);
				i++;
			}
			else if (s == "-use_infeasibility_lower_bounding")
			{
				params.use_infeasibility_lower_bounding = atoi(argv[i + 1]);
				i++;
			}
			else if (s == "-incremental-frequency")
			{
				params.use_incremental_frequency_reconstruction = atoi(argv[i + 1]);
				i++;
			}
			else if (s == "-CART-objective")
			{
				params.use_CART_objective = atoi(argv[i + 1]);
				i++;
			}
			else if (s == "-CART-alpha")
			{
				params.CART_alpha = std::stod(argv[i + 1]);
				i++;
			}
			else if (s == "-hyper-tuning")
			{
				params.hyper_tuning_mode = atoi(argv[i + 1]);
				i++;
			}
			else if (s == "-hyper-tuning-splits")
			{
				splits_location_prefix = argv[i + 1];
				i++;
			}
			else if (s == "-use-upper-bounding")
			{
				params.use_upper_bounding = argv[i + 1];
			}
			else if (s == "-hyper-tuning-stats-file")
			{
				hyper_parameter_stats_file = argv[i + 1];
				i++;				
			}
			else
			{
				std::cout << "Unknown parameter: " << argv[i] << std::endl;
			}
		}
		if (file_location.find(".csv") != std::string::npos) { data = new LearningData(FileReader::ReadDataHao(file_location, false)); }
		else { data = new LearningData(FileReader::ReadDataDL(file_location)); }
	}

	if (params.max_depth == 0 || params.max_depth >= 50) { std::cout << "Error: strange depth: " << params.max_depth << "\n"; exit(1); }
	if (params.unit_tree_depth > 4) { std::cout << "Error: strange unit tree size: " << params.unit_tree_depth << "\n"; exit(1); }
	if (params.hyper_tuning_mode && splits_location_prefix == "") { std::cout << "Error: hyper tuning specified but no splits given\n"; exit(1); }
	
	srand(seed);

	if (params.verbose)
	{
		std::cout << program << std::endl;
		std::cout << file_location << std::endl;
		std::cout << "depth\n" << params.max_depth << "\n";
		std::cout << "unit_tree_size\n" << params.unit_tree_depth << "\n";
		std::cout << "node_selection_strategy\n" << params.node_selection_strategy << "\n";
		std::cout << "time_limit_in_seconds\n" << params.time_limit << "\n";
		std::cout << "preprocessing\n" << params.preprocess_data << "\n";
		if (!manual_activation) data->PrintStats();
	}

	params.max_depth = std::min(params.max_depth, params.max_size);
	params.max_size = std::min(params.max_size, (uint32_t(1) << params.max_depth) - 1);

	clock_t time_start_hehe = clock();
	time_t time2 = time(0);

	SolverParameters solver_params;
	solver_params.max_depth = params.max_depth;
	solver_params.max_size = params.max_size;
	solver_params.time_limit = params.time_limit;
	solver_params.upper_bound_constraint = params.upper_bound_constraint;
	solver_params.verbose = params.verbose;

	DecisionTreeSolverMur::Solver solver(*data, params);	
	DecisionTreeSolverMur::Result result2;
	
	if (params.use_CART_objective)
	{
		result2 = solver.SolveCARTobjective(solver_params, params.CART_alpha);
	}
	else if (params.hyper_tuning_mode)
	{
		splits = FileReader::ReadSplits(file_location, splits_location_prefix);
		DecisionTreeSolverMur::ResultHyperParameterTuning r = DecisionTreeSolverMur::Solver::SolveHyperParameterTuning(*data, splits, params, solver_params);

		if (params.dump_file != "")
		{
			/*std::ofstream time_print(params.dump_file + "biOpttime.txt");
			time_print << r.total_time;
			PrintTreeIntoFile(r.best_accuracy_tree, params.dump_file);
			PrintTreeIntoFile(r.best_weighted_accuracy_tree, params.dump_file + "w.txt");
			PrintTreeIntoFile(r.best_f1_tree, params.dump_file + "f1.txt");*/
		}

		if (hyper_parameter_stats_file != "")
		{
			std::ofstream out(hyper_parameter_stats_file.c_str());
			out << r.total_time << "\n";
			for (auto &v : r.stats_accuracy)
			{
				for (auto &k : v)
				{
					if (k.testing_accuracy == 0) { continue; }
					out << k.tree_depth << " " << k.tree_size << " " << k.training_time << " " << k.training_accuracy << " " << k.testing_accuracy << " " << k.f1_train << " " << k.f1_test << "\n";
				}
			}
			out << "\n";

			for (auto& v : r.stats_f1)
			{
				for (auto& k : v)
				{
					if (k.testing_accuracy == 0) { continue; }
					out << k.tree_depth << " " << k.tree_size << " " << k.training_time << " " << k.training_accuracy << " " << k.testing_accuracy << " " << k.f1_train << " " << k.f1_test << "\n";
				}
			}
			out << "\n";

			for (auto& v : r.stats_weighted_accuracy)
			{
				for (auto& k : v)
				{
					if (k.testing_accuracy == 0) { continue; }
					out << k.tree_depth << " " << k.tree_size << " " << k.training_time << " " << k.training_accuracy << " " << k.testing_accuracy << " " << k.f1_train << " " << k.f1_test << "\n";
				}
			}
		}
		return 0;
	}
	else
	{
		result2 = solver.Solve(solver_params);
	}
	
	if (1 || params.verbose)
	{
		if (result2.IsFeasible())
		{
			DecisionTreeSolverMur::DecisionTree &hehe = *result2.decision_tree_;

			uint32_t accuracy = 10000000;
			for (auto sol : result2.best_assignments.GetStoredAssignments())
			{
				accuracy = std::min(accuracy, sol.num_false_positives + sol.num_false_negatives);
			}

			std::cout << "total clock time for height variant: " << (double(clock() - time_start_hehe) / CLOCKS_PER_SEC) << std::endl;
			std::cout << "wallclock time: " << time(0) - time2 << "\n";
			std::cout << "Accuracy: " << accuracy << "\n";
			std::cout << "Size of the Pareto frontier: " << result2.best_assignments.Size() << "\n";
			//system("pause");

			if (params.dump_file != "")
			{
				std::ofstream out(params.dump_file.c_str());
				out << time(0) - time2 << "\n";
				out << result2.best_assignments.Size() << "\n";
				if (result2.best_assignments.GetStoredAssignments().size() == 0)
				{
					out << "-1 -1";
				}
				else
				{
					for (int i = 0; i < result2.best_assignments.GetStoredAssignments().size() - 1; i++)
					{
						out << result2.best_assignments.GetStoredAssignments()[i].num_false_positives << " " << result2.best_assignments.GetStoredAssignments()[i].num_false_negatives << "\n";
					}
					out << result2.best_assignments.GetStoredAssignments().back().num_false_positives << " " << result2.best_assignments.GetStoredAssignments().back().num_false_negatives;
				}
			}

			std::cout << "Verifying costs...\n";

			for (auto pt : result2.pareto_trees)
			{
				int false_positives, false_negatives;
				pt.decision_tree_->EvaluateBiObj(*data, false_positives, false_negatives);
				if (false_positives == pt.num_false_positives && false_negatives == pt.num_false_negatives)
				{
					std::cout << "Verified (" << false_positives << ", " << false_negatives << ")\n";
					std::cout << "\tNum nodes: " << pt.decision_tree_->NumFeatureNodes() << "\n";
				}
				else
				{
					std::cout << "Failed verification: " << false_positives << " , " << false_negatives << " vs true " << pt.num_false_positives << " , " << pt.num_false_negatives << "\n";
				}
				//std::cout << "Verified cost: " <<  << std::endl;
				//std::cout << "Verified number of nodes: " << hehe.NumFeatureNodes() << std::endl;
				//std::cout << "Cache entries: " << solver.state_.cache_->NumEntries() << std::endl;
			}
			
		}
		std::cout << "done!\n";
		std::cout << "total clock time for height variant: " << (double(clock() - time_start_hehe) / CLOCKS_PER_SEC) << std::endl;
		std::cout << "wallclock time: " << time(0) - time2 << "\n";
		if (manual_activation) system("pause");
		return 0;
	}

	if (!result2.IsFeasible() && params.dump_file != "")
	{
		std::ofstream out(params.dump_file.c_str());
		out << "UNSAT";
	}
	else if (params.dump_file != "")
	{
		PrintTreeIntoFile(result2.decision_tree_, params.dump_file);
	}

	if (manual_activation) system("pause");
	return 0;
}