#include "feature_vector_binary.h"

#include <iostream>
#include <assert.h>

namespace DecisionTreeSolverMur
{
	FeatureVectorBinary::FeatureVectorBinary(const std::vector<bool>& feature_values) :
		internal_data_(NULL)
	{
		static uint32_t next_id(0); //hax todo; the code relies on using one dataset and one dataset only, since it depends on the Ids, should fix this in the future

		internal_data_ = new InternalData();
		internal_data_->is_feature_present.resize(feature_values.size());
		internal_data_->id = next_id++;
		internal_data_->reference_counter = new uint32_t(1);

		for (size_t feature_index = 0; feature_index < feature_values.size(); feature_index++)
		{
			internal_data_->is_feature_present[feature_index] = feature_values[feature_index];
			if (feature_values[feature_index] == true) { internal_data_->present_features.push_back(feature_index); }
		}
	}

	FeatureVectorBinary::FeatureVectorBinary(const FeatureVectorBinary & reference) :
		internal_data_(reference.internal_data_)
	{
	//	(*internal_data_->reference_counter)++;
	}

	FeatureVectorBinary & FeatureVectorBinary::operator=(const FeatureVectorBinary & right_hand_side)
	{
		if (internal_data_ != right_hand_side.internal_data_)
		{
		//	DecrementReferenceCounterAndCleanUp();
			internal_data_ = right_hand_side.internal_data_;
		//	++(*internal_data_->reference_counter);
		}
		return *this;
	}

	FeatureVectorBinary::~FeatureVectorBinary()
	{
		assert(internal_data_ != NULL && (*internal_data_->reference_counter) > 0);
		//DecrementReferenceCounterAndCleanUp();
	}

	void FeatureVectorBinary::DecrementReferenceCounterAndCleanUp()
	{
		--(*internal_data_->reference_counter);
		if ((*internal_data_->reference_counter) == 0) //clean up if there are no classes left using this data
		{
			delete internal_data_->reference_counter;
			delete internal_data_;
		}
	}

	bool FeatureVectorBinary::operator[](uint32_t feature) const
	{
		return internal_data_->is_feature_present[feature];
	}

	uint32_t FeatureVectorBinary::GetJthPresentFeature(uint32_t j) const
	{
		return internal_data_->present_features[j];
	}

	double FeatureVectorBinary::Sparsity() const
	{
		return double(NumPresentFeatures()) / internal_data_->is_feature_present.size();
	}

	uint32_t FeatureVectorBinary::NumPresentFeatures() const
	{
		return uint32_t(internal_data_->present_features.size());
	}

	uint32_t FeatureVectorBinary::GetID() const
	{
		return internal_data_->id;
	}

	typename std::vector<uint32_t>::const_iterator FeatureVectorBinary::begin() const
	{
		return internal_data_->present_features.begin();
	}

	typename std::vector<uint32_t>::const_iterator FeatureVectorBinary::end() const
	{
		return internal_data_->present_features.end();
	}	

	std::ostream & operator<<(std::ostream & os, const FeatureVectorBinary & fv)
	{
		if (fv.NumPresentFeatures() == 0) { std::cout << "[empty]"; }
		else
		{
			auto iter = fv.begin();
			std::cout << *iter;
			++iter;
			while (iter != fv.end()) 
			{ 
				std::cout << " " << *iter; 
				++iter; 
			}
		}
		return os;
	}

}