#pragma once

#include "penalty_branch_computer_depth_2.h"

#include <vector>

namespace DecisionTreeSolverMur
{
	class OptimalDecisionTreeConstructorDepth2Tester;

	struct ChildrenInformation
	{
		ChildrenInformation():
			left_child_feature(UINT32_MAX),
			right_child_feature(UINT32_MAX),
			left_child_penalty(UINT32_MAX),
			right_child_penalty(UINT32_MAX){}

		uint32_t left_child_feature, right_child_feature;
		uint32_t left_child_penalty, right_child_penalty;
	};

	struct SolutionNode
	{
		SolutionNode() :
			feature(UINT32_MAX),
			nodes_left(0),
			nodes_right(0),
			total_penalty(0),
			feature_left_child(UINT32_MAX),
			feature_right_child(UINT32_MAX) {}

		uint32_t feature;
		uint32_t nodes_left;
		uint32_t nodes_right;
		uint32_t total_penalty;

		uint32_t feature_left_child, feature_right_child;
	};

	class OptimalDecisionTreeConstructorDepth2
	{
		struct BestPenalties
		{
			BestPenalties(uint32_t initial_values) { Initialise(initial_values); }
			
			void Initialise(uint32_t val) { zero_nodes = one_node = two_nodes = three_nodes = val; }

			uint32_t zero_nodes;
			uint32_t one_node;
			uint32_t two_nodes;
			uint32_t three_nodes;
		};

		
	public:
		OptimalDecisionTreeConstructorDepth2(uint32_t num_labels, uint32_t num_features, uint32_t num_total_data_points); //todo make it mandatory not an optional parameter?

		void ComputeOptimalDecisionTree(LearningData &data);
		SolutionNode ComputeOptimalDecisionTreeNodeBudget(LearningData &data, uint32_t max_node_budget);

		uint32_t GetOptimalPenalty() const;
		uint32_t GetOptimalFeatureForRoot() const;

		//private:

		void InitialiseDataStructures(LearningData &data);
		void InitialiseChildrenInfo();

		void UpdateBestLeftChild(uint32_t root_feature, uint32_t left_child_feature, uint32_t penalty);
		void UpdateBestRightChild(uint32_t root_feature, uint32_t right_child_feature, uint32_t penalty);
		void UpdateBestPenalty(uint32_t root_feature);

		uint32_t num_features_;
		uint32_t optimal_penalty_;
		uint32_t optimal_feature_;
		PenaltyBranchComputerDepth2 penalty_computer_;
		std::vector<ChildrenInformation> children_info_;

		SolutionNode last_sol_at_most_two_nodes, last_sol_at_most_three_nodes;

		friend OptimalDecisionTreeConstructorDepth2Tester;
	};
}