#pragma once

#include "../learning_data.h"
#include "penalty_branch_computer_depth_3.h"

#include <vector>

namespace DecisionTreeSolverMur
{
	struct ChildrenInformation2
	{
		uint32_t left_child_feature, right_child_feature;
		uint32_t left_child_penalty, right_child_penalty;
	};

	struct SubtreeInformation
	{
		SubtreeInformation(uint32_t num_features) :left_subtree(num_features), right_subtree(num_features) {}
		std::vector<ChildrenInformation2> left_subtree, right_subtree;
	};

	class OptimalDecisionTreeConstructorDepth3
	{
	public:
		OptimalDecisionTreeConstructorDepth3(size_t num_features);

		void ComputeOptimalDecisionTree(LearningData &data);
		uint32_t GetOptimalPenalty() const;

		//private:

		void InitialiseDataStructures(LearningData &data);
		void InitialiseSubtreeInfo();

		void UpdateTrees(uint32_t f1, uint32_t f2, uint32_t f3);

		void UpdateBestLeftLeftChild(uint32_t f1, uint32_t f2, uint32_t f3, uint32_t penalty);
		void UpdateBestLeftRightChild(uint32_t f1, uint32_t f2, uint32_t f3, uint32_t penalty);
		void UpdateBestRightLeftChild(uint32_t f1, uint32_t f2, uint32_t f3, uint32_t penalty);
		void UpdateBestRightRightChild(uint32_t f1, uint32_t f2, uint32_t f3, uint32_t penalty);

		void UpdateBestPenalty();

		uint32_t num_features_;
		uint32_t optimal_penalty_;
		PenaltyBranchComputerDepth3 penalty_computer_;
		std::vector<SubtreeInformation> subtree_info_;
	};
}