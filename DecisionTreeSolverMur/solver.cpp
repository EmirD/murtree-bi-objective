#include "solver.h"
#include "node_selector_post_order.h"
#include "node_selector_best_first.h"
#include "node_selector_post_order_flex.h"
#include "node_selector_best_first_variant.h"
#include "node_selector_backtracking_cart.h"
#include "specialised_optimal_binary_classification_tree_solver_depth2.h"

namespace DecisionTreeSolverMur
{
	Solver::Solver(LearningData &data, Parameters &params):
		state_(data, params, statistics_),
		node_selection_strategy_(params.node_selection_strategy)
	{
		node_selector_ = 0;
		if (node_selection_strategy_ == "post_order")
		{
			node_selector_ = new NodeSelectorPostOrder(&state_);
		}
		else if (node_selection_strategy_ == "best_first")
		{
			node_selector_ = new NodeSelectorBestFirst(&state_);
		}
		else if (node_selection_strategy_ == "post_order_flex_larger_first")
		{
			node_selector_ = new NodeSelectorPostOrderFlex(&state_, true);
		}
		else if (node_selection_strategy_ == "post_order_flex_smaller_first")
		{
			node_selector_ = new NodeSelectorPostOrderFlex(&state_, false);
		}
		else if (node_selection_strategy_ == "best_first_variant1")
		{
			node_selector_ = new NodeSelectorBestFirstVariant(&state_, true);
		}
		else if (node_selection_strategy_ == "best_first_variant0")
		{
			node_selector_ = new NodeSelectorBestFirstVariant(&state_, false);
		}
		else if (node_selection_strategy_ == "backtracking_cart1")
		{
			node_selector_ = new NodeSelectorBacktrackingCART(&state_, true);
		}
		/*else if (parameters_.node_selection_strategy == "random")
		{
		node_selector_ = new NodeSelectorRandom(tree_);
		}*/
		else
		{
			std::cout << "Unknown node selection strategy: " << "\n";
			exit(1);
		}
	}

	Solver::~Solver()
	{
		delete node_selector_;
	}

	Result Solver::Solve(SolverParameters &parameters)
	{
		Initialise(parameters);

		int iter = 0;

		while (!state_.IsRootNodeExhausted() && IsWithinTimeBudget(parameters.time_limit))
		{
			iter++;
			assert(state_.IsStateCorrect());
		
			DecisionNode * next_node = node_selector_->GetNextNode();
			ProcessNode(next_node);			
			
			UpdateBestAssignment(parameters.verbose);
		}
		Result output = ConstructOutput(parameters.verbose);
		state_.decision_tree_.DeactivateRoot();
		output.best_assignments = best_assignments_;
		return output;
	}

	Result Solver::SolveCARTobjective(SolverParameters & parameters, double alpha)
	{
		assert(parameters.max_depth >= 1);

		auto CARTobjective = [alpha](Result &r, uint32_t num_instances)->double
		{
			assert(num_instances >= r.misclassifications);
			double accuracy = double(r.misclassifications) / num_instances;
			return accuracy + alpha * r.decision_tree_->NumFeatureNodes();
		};

		SolverParameters current_parameters(parameters);
		current_parameters.max_size = 0;
		uint32_t max_depth = parameters.max_depth;
		uint32_t num_instances = state_.data_.Size();
		
		Result best_result = Solve(current_parameters);
		uint32_t max_size = std::min(parameters.max_size, (uint32_t(1) << max_depth) - 1);
		for (uint32_t size = 1; size < max_size; size++)
		{
			current_parameters.max_size = size;
			std::cout << "size: " << size << "\n";
			Result current_result = Solve(current_parameters);

			if (CARTobjective(current_result, num_instances) < CARTobjective(best_result, num_instances))
			{
				best_result = current_result;
			}				
		}
		return best_result;
	}

	void Solver::GetBestTrees(LearningData& data, std::vector<ParetoTree>& pts, DecisionTree** best_accuracy_tree, int &split_misclas, DecisionTree** best_weighted_accuracy_tree, double& split_weighted_accuracy, DecisionTree** best_f1_tree, double & split_f1_score)
	{
		//split trees are the best trees for this split
		split_misclas = INT32_MAX;
		split_weighted_accuracy = INT32_MAX;
		split_f1_score = 0;
		int false_positives = 0, false_negatives = 0;

		double positive_weight = 0, negative_weight = 0;
		SetWeights(data, positive_weight, negative_weight);

		for (auto pt : pts)
		{
			DecisionTree* current_tree = pt.decision_tree_;
			current_tree->EvaluateBiObj(data, false_positives, false_negatives);

			if (false_positives + false_negatives < split_misclas)
			{
				split_misclas = false_positives + false_negatives;
				*best_accuracy_tree = current_tree;
			}

			if (ComputeF1score(data, false_positives, false_negatives) > split_f1_score)
			{
				split_f1_score = ComputeF1score(data, false_positives, false_negatives);
				*best_f1_tree = current_tree;
			}

			if (ComputeWeightedAccuracy(data, false_positives, false_negatives) < split_weighted_accuracy)
			{
				split_weighted_accuracy = ComputeWeightedAccuracy(data, false_positives, false_negatives);
				*best_weighted_accuracy_tree = current_tree;
			}
		}
		runtime_assert(*best_accuracy_tree != 0 && *best_f1_tree != 0 && *best_weighted_accuracy_tree != 0);
	}

	double Solver::ComputeWeightedAccuracy(LearningData& data, int false_positives, int false_negatives)
	{
		double pos_w = 0, neg_w = 0;
		SetWeights(data, pos_w, neg_w);

		double total_score = data.NumInstancesForLabel(1) * pos_w + data.NumInstancesForLabel(0) * neg_w;
		double ret = double(total_score - pos_w * false_positives - neg_w * false_negatives) / total_score;
		return ret;
	}

	void Solver::SetWeights(LearningData& data, double& positive_weight, double& negative_weight)
	{
		//positive_weight = double(data.NumInstancesForLabel(0)) / data.Size();
		//negative_weight = double(data.NumInstancesForLabel(1)) / data.Size();
		positive_weight = double(data.NumInstancesForLabel(1)) / data.Size(); 
		negative_weight = double(data.NumInstancesForLabel(0)) / data.Size();
	}

	double Solver::ComputeF1score(LearningData& data, int false_positives, int false_negatives)
	{
		if (data.NumInstancesForLabel(0) > data.NumInstancesForLabel(1))
		{
			double tp = data.NumInstancesForLabel(1) - false_negatives;
			double fp = false_positives;
			double fn = false_negatives;
			return float(tp) / (tp + 0.5 * (fp + fn));
		}
		else
		{
			double tp = data.NumInstancesForLabel(0) - false_positives;
			double fp = false_negatives;
			double fn = false_positives;
			return float(tp) / (tp + 0.5 * (fp + fn));
		}
	}

	ResultHyperParameterTuning Solver::SolveHyperParameterTuning(LearningData &data, std::vector<TrainTestSplit> &splits, Parameters &global_parameters, SolverParameters &solver_parameters)
	{
		clock_t global_time_start = clock();

		size_t num_splits = splits.size();
		std::vector<Solver*> solvers(num_splits);
		for (size_t i = 0; i < num_splits; i++) { solvers[i] = new Solver(splits[i].training_set, global_parameters); }
		
		SolverParameters current_parameters(solver_parameters);
		current_parameters.max_depth = 0;
		current_parameters.max_size = 0;
		current_parameters.verbose = false;

		ResultHyperParameterTuning ret;
		ret.stats = std::vector<std::vector<TrainTestResult> >(solver_parameters.max_depth + 1, std::vector<TrainTestResult>(solver_parameters.max_size + 1));
		ret.stats_accuracy = std::vector<std::vector<TrainTestResult> >(solver_parameters.max_depth + 1, std::vector<TrainTestResult>(solver_parameters.max_size + 1));
		ret.stats_f1 = std::vector<std::vector<TrainTestResult> >(solver_parameters.max_depth + 1, std::vector<TrainTestResult>(solver_parameters.max_size + 1));
		ret.stats_weighted_accuracy = std::vector<std::vector<TrainTestResult> >(solver_parameters.max_depth + 1, std::vector<TrainTestResult>(solver_parameters.max_size + 1));

		struct BestStats
		{
			double train_score, test_score;
			int depth;
			int size;
		
			BestStats() :
				train_score(0),
				test_score(0),
				depth(0),
				size(0)
			{}
		};

		BestStats best_misclas, best_weighted_misclas, best_f1;
		best_misclas.test_score = INT32_MAX;
		best_weighted_misclas.test_score = INT32_MAX;
		best_f1.test_score = 0;

		/*uint32_t best_depth = 1;
		uint32_t best_size = 0;
		uint32_t best_testing_misclassification = 0;*/
		/*for (int i = 0; i < num_splits; i++) 
		{ 
			//best_testing_misclassification += splits[i].testing_set.ComputeClassificationCost(); 
			best_misclas.test_score += splits[i].testing_set.ComputeClassificationCost();

			if (splits[i].training_set.NumInstancesForLabel(0) > splits[i].training_set.NumInstancesForLabel(1))
			{
				best_f1.test_score += ComputeF1score(splits[i].testing_set, 0, splits[i].training_set.NumInstancesForLabel(1));
			}
			else
			{
				best_f1.test_score += ComputeF1score(splits[i].testing_set, splits[i].training_set.NumInstancesForLabel(0), 0);
			}			
		}*/

		for (uint32_t depth = 1; depth <= solver_parameters.max_depth; depth++)
		{
			uint32_t max_size = std::min(solver_parameters.max_size, (uint32_t(1) << depth) - 1);
			for (uint32_t size = depth; size <= max_size; size++)
			{
				std::cout << "\t(depth, size) = " << "(" << depth << ", " << size << ")\n";
				current_parameters.max_depth = depth;
				current_parameters.max_size = size;
				
				//uint32_t current_testing_misclassifications = 0;
				//DecisionTree* best_accuracy_tree, * best_weighted_accuracy_tree, * best_f1_tree;
				BestStats current_misclas, current_weighted_misclas, current_f1;
				clock_t time_start = clock();
				//double training_accuracy, testing_accuracy, tree_size, tree_depth, training_time;
				for (size_t i = 0; i < splits.size(); i++)
				{					
					if (double(clock() - global_time_start) / CLOCKS_PER_SEC > global_parameters.time_limit)
					{
						goto hehe;
					}

					current_parameters.time_limit = (global_parameters.time_limit - (double(clock() - global_time_start) / CLOCKS_PER_SEC));
					Result result = solvers[i]->Solve(current_parameters);					

					runtime_assert(result.pareto_trees.size() > 0);
					//find the best trees among the Pareto front for this

					int split_misclas;
					double split_weighted_misclas;
					double split_f1_score;
					DecisionTree* split_accuracy_tree = 0, * split_weighted_accuracy_tree = 0, * split_f1_tree = 0;

					GetBestTrees(
						splits[i].testing_set,
						result.pareto_trees, 
						&split_accuracy_tree, 
						split_misclas, 
						&split_weighted_accuracy_tree, 
						split_weighted_misclas, 
						&split_f1_tree,
						split_f1_score
					);

					int fp = 0, fn = 0;
					split_accuracy_tree->EvaluateBiObj(splits[i].training_set, fp, fn);
					current_misclas.train_score = (fp + fn);
					split_f1_tree->EvaluateBiObj(splits[i].training_set, fp, fn);
					current_f1.train_score = ComputeF1score(splits[i].training_set, fp, fn);
					split_weighted_accuracy_tree->EvaluateBiObj(splits[i].training_set, fp, fn);
					double pos_w = 0, neg_w = 0;
					SetWeights(splits[i].training_set, pos_w, neg_w);
					current_weighted_misclas.train_score = ComputeWeightedAccuracy(splits[i].training_set, fp, fn);
															
					current_misclas.test_score = split_misclas;
					current_f1.test_score = split_f1_score;
					current_weighted_misclas.test_score = split_weighted_misclas;

					//uint32_t test_misclass = result.decision_tree_->Evaluate(splits[i].testing_set);
					//uint32_t train_misclass = result.decision_tree_->Evaluate(splits[i].training_set);
					//current_testing_misclassifications += test_misclass;				
					
					ret.stats_accuracy[depth][size].tree_depth += split_accuracy_tree->GetMaxDepth();
					ret.stats_accuracy[depth][size].tree_size += split_accuracy_tree->NumFeatureNodes();
					ret.stats_accuracy[depth][size].training_accuracy += double(splits[i].training_set.Size() - current_misclas.train_score) / splits[i].training_set.Size();
					ret.stats_accuracy[depth][size].testing_accuracy += double(splits[i].testing_set.Size() - current_misclas.test_score) / splits[i].testing_set.Size();
					split_accuracy_tree->EvaluateBiObj(splits[i].training_set, fp, fn);
					ret.stats_accuracy[depth][size].f1_train += ComputeF1score(splits[i].training_set, fp, fn);
					split_accuracy_tree->EvaluateBiObj(splits[i].testing_set, fp, fn);
					ret.stats_accuracy[depth][size].f1_test += ComputeF1score(splits[i].testing_set, fp, fn);

					ret.stats_f1[depth][size].tree_depth += split_accuracy_tree->GetMaxDepth();
					ret.stats_f1[depth][size].tree_size += split_accuracy_tree->NumFeatureNodes();
					split_f1_tree->EvaluateBiObj(splits[i].training_set, fp, fn);
					ret.stats_f1[depth][size].training_accuracy += double(splits[i].training_set.Size() - fp - fn) / splits[i].training_set.Size();
					split_f1_tree->EvaluateBiObj(splits[i].testing_set, fp, fn);
					ret.stats_f1[depth][size].testing_accuracy += double(splits[i].testing_set.Size() - fp - fn) / splits[i].testing_set.Size();
					ret.stats_f1[depth][size].f1_train += current_f1.train_score;
					ret.stats_f1[depth][size].f1_test += current_f1.test_score;

					ret.stats_weighted_accuracy[depth][size].tree_depth += split_accuracy_tree->GetMaxDepth();
					ret.stats_weighted_accuracy[depth][size].tree_size += split_accuracy_tree->NumFeatureNodes();
					split_weighted_accuracy_tree->EvaluateBiObj(splits[i].training_set, fp, fn);
					ret.stats_weighted_accuracy[depth][size].training_accuracy += double(splits[i].training_set.Size() - fp - fn) / splits[i].training_set.Size();
					split_weighted_accuracy_tree->EvaluateBiObj(splits[i].testing_set, fp, fn);
					ret.stats_weighted_accuracy[depth][size].testing_accuracy += double(splits[i].testing_set.Size() - fp - fn) / splits[i].testing_set.Size();
					split_weighted_accuracy_tree->EvaluateBiObj(splits[i].training_set, fp, fn);
					ret.stats_weighted_accuracy[depth][size].f1_train += ComputeF1score(splits[i].training_set, fp, fn);
					split_weighted_accuracy_tree->EvaluateBiObj(splits[i].testing_set, fp, fn);
					ret.stats_weighted_accuracy[depth][size].f1_test += ComputeF1score(splits[i].testing_set, fp, fn);
				}
				/*ret.stats[depth][size].training_time = double(clock() - time_start)/CLOCKS_PER_SEC;
				ret.stats[depth][size].tree_depth = depth;//double(ret.stats[depth][size].tree_depth) / splits.size();
				ret.stats[depth][size].tree_size = size;//double(ret.stats[depth][size].tree_size) / splits.size();
				ret.stats[depth][size].training_accuracy = double(ret.stats[depth][size].training_accuracy) / splits.size();
				ret.stats[depth][size].testing_accuracy = double(ret.stats[depth][size].testing_accuracy) / splits.size();
				*/
				ret.stats_accuracy[depth][size].training_time = double(clock() - time_start) / CLOCKS_PER_SEC;
				ret.stats_accuracy[depth][size].tree_depth = double(ret.stats_accuracy[depth][size].tree_depth) / splits.size();
				ret.stats_accuracy[depth][size].tree_size = double(ret.stats_accuracy[depth][size].tree_size) / splits.size();
				ret.stats_accuracy[depth][size].training_accuracy = double(ret.stats_accuracy[depth][size].training_accuracy) / splits.size();
				ret.stats_accuracy[depth][size].testing_accuracy = double(ret.stats_accuracy[depth][size].testing_accuracy) / splits.size();
				ret.stats_accuracy[depth][size].f1_train /= splits.size();
				ret.stats_accuracy[depth][size].f1_test /= splits.size();

				ret.stats_f1[depth][size].training_time = double(clock() - time_start) / CLOCKS_PER_SEC;
				ret.stats_f1[depth][size].tree_depth = double(ret.stats_f1[depth][size].tree_depth) / splits.size();
				ret.stats_f1[depth][size].tree_size = double(ret.stats_f1[depth][size].tree_size) / splits.size();
				ret.stats_f1[depth][size].training_accuracy = double(ret.stats_f1[depth][size].training_accuracy) / splits.size();
				ret.stats_f1[depth][size].testing_accuracy = double(ret.stats_f1[depth][size].testing_accuracy) / splits.size();
				ret.stats_f1[depth][size].f1_train /= splits.size();
				ret.stats_f1[depth][size].f1_test /= splits.size();

				ret.stats_weighted_accuracy[depth][size].training_time = double(clock() - time_start) / CLOCKS_PER_SEC;
				ret.stats_weighted_accuracy[depth][size].tree_depth = double(ret.stats_weighted_accuracy[depth][size].tree_depth) / splits.size();
				ret.stats_weighted_accuracy[depth][size].tree_size = double(ret.stats_weighted_accuracy[depth][size].tree_size) / splits.size();
				ret.stats_weighted_accuracy[depth][size].training_accuracy = double(ret.stats_weighted_accuracy[depth][size].training_accuracy) / splits.size();
				ret.stats_weighted_accuracy[depth][size].testing_accuracy = double(ret.stats_weighted_accuracy[depth][size].testing_accuracy) / splits.size();
				ret.stats_weighted_accuracy[depth][size].f1_train /= splits.size();
				ret.stats_weighted_accuracy[depth][size].f1_test /= splits.size();

				if (best_misclas.test_score > current_misclas.test_score)
				{
					best_misclas.train_score = current_misclas.train_score;
					best_misclas.test_score = current_misclas.test_score;
					best_misclas.depth = depth;
					best_misclas.size = size;
				}

				if (best_weighted_misclas.test_score > current_weighted_misclas.test_score)
				{
					best_weighted_misclas.train_score = current_weighted_misclas.train_score;
					best_weighted_misclas.test_score = current_weighted_misclas.test_score;
					best_weighted_misclas.depth = depth;
					best_weighted_misclas.size = size;
				}

				if (best_f1.test_score < current_f1.test_score)
				{
					best_f1.train_score = current_f1.train_score;
					best_f1.test_score = current_f1.test_score;
					best_f1.depth = depth;
					best_f1.size = size;
				}

				std::cout << "\ttime: " << double(clock() - time_start) / CLOCKS_PER_SEC << "\n";

				/*if (best_testing_misclassification > current_testing_misclassifications)
				{
					best_testing_misclassification = current_testing_misclassifications;
					best_depth = depth;
					best_size = size;
				}*/
			}
		}

		hehe:

		std::cout << "Chosen (depth, size):\n";
		std::cout << "\tmisclas: (" << best_misclas.depth << ", " << best_misclas.size << ")\n";
		std::cout << "\tweighted misclas: (" << best_weighted_misclas.depth << ", " << best_weighted_misclas.size << ")\n";
		std::cout << "\tf1 score: (" << best_f1.depth << ", " << best_f1.size << ")\n";

		Solver final_solver(data, global_parameters);

		//f1 score
		current_parameters.max_depth = best_f1.depth;
		current_parameters.max_size = best_f1.size;
		auto d = final_solver.Solve(current_parameters);

		DecisionTree* best_f1_tree = 0, * best_misclas_tree = 0, * best_weighted_misclas_tree = 0;
		int a; double b, c;
 		GetBestTrees(
			data,
			d.pareto_trees,
			&best_misclas_tree,
			a,
			&best_weighted_misclas_tree,
			b,
			&best_f1_tree,
			c
		);
		ret.total_time = double(clock() - global_time_start) / CLOCKS_PER_SEC;
		ret.best_f1_tree = best_f1_tree;
		ret.best_accuracy_tree = best_f1_tree;
		ret.best_weighted_accuracy_tree = best_f1_tree;
		return ret;

		//actually...I do not need the rest of the code, the trees any way do not get evaluated

		//misclas
		current_parameters.max_depth = best_misclas.depth;
		current_parameters.max_size = best_misclas.size;
		d = final_solver.Solve(current_parameters);
		GetBestTrees(
			data,
			d.pareto_trees,
			&best_misclas_tree,
			a,
			&best_weighted_misclas_tree,
			b,
			&best_f1_tree,
			c
		);
		ret.best_accuracy_tree = best_misclas_tree;


		//weighted
		current_parameters.max_depth = best_weighted_misclas.depth;
		current_parameters.max_size = best_weighted_misclas.size;
		d = final_solver.Solve(current_parameters);
		GetBestTrees(
			data,
			d.pareto_trees,
			&best_misclas_tree,
			a,
			&best_weighted_misclas_tree,
			b,
			&best_f1_tree,
			c
		);
		ret.best_weighted_accuracy_tree = best_weighted_misclas_tree;

		for (size_t i = 0; i < num_splits; i++) { delete solvers[i]; solvers[i] = 0; }
		return ret;
	}

	void Solver::Initialise(SolverParameters &p)
	{
		clock_start_ = clock();
		best_assignments_.Clear();
		state_.Initialise(p.max_depth, p.max_size, p.upper_bound_constraint);
		UpdateBestAssignment(p.verbose);
	}

	void Solver::ProcessNode(DecisionNode * node)
	{
		if (state_.IsTerminalNode(node))
		{
			//ProcessTerminalNode(node);
			state_.SolveTerminalNode(node);
		}
		else
		{
			ProcessNonTerminalNode(node);
		}
	}

	void Solver::ProcessNonTerminalNode(DecisionNode * node)
	{
		PairFeatureNodeBudgets assignment = state_.PopNextNodeAssignment(node);
		bool should_propagate = state_.PerformAssignmentToNode(node, assignment.feature, assignment.budgets);
		if (should_propagate) { state_.PropagateNode(node); }
	}

	void Solver::UpdateBestAssignment(bool verbose)
	{
		if (state_.GetBestAssignments() != best_assignments_)
		{
			best_assignments_ = state_.GetBestAssignments();
			if (verbose) { std::cout << best_assignments_ << ", time = " << GetRuntimeInSeconds() << "secs \n"; }
		}
		/*if (!state_.GetBestAssignments().IsNull() &&  state_.GetBestAssignments().IsBetterThan(best_assignments_))
		{
			best_assignments_ = state_.GetBestAssignments();
			if (verbose) { std::cout << best_assignments_.misclassifications << " " << best_assignments_.NumNodes() << " " << GetRuntimeInSeconds() <<  "\n"; }
		}*/
	}

	bool Solver::IsWithinTimeBudget(uint32_t time_limit)
	{
		return GetRuntimeInSeconds() < time_limit;
	}

	time_t Solver::GetRuntimeInSeconds()
	{
		return (clock() - clock_start_)/CLOCKS_PER_SEC;
	}

	Result Solver::ConstructOutput(bool verbose)
	{
		if (!best_assignments_.IsEmpty())
		{
			Result result;
			//result.misclassifications = best_assignments_.misclassifications;
			result.is_proven_optimal = state_.IsRootNodeExhausted(); //make this call before deactivating the root, a bit hacky
			for (auto solution : best_assignments_.GetStoredAssignments())
			{
				if (solution.num_false_positives == 25)
				{
					int k = 0;
					k++;
				}


				DecisionTree* tree = state_.ReconstructBestParetoTree(solution);
				ParetoTree pareto_tree;
				pareto_tree.decision_tree_ = tree;
				pareto_tree.num_false_positives = solution.num_false_positives;
				pareto_tree.num_false_negatives = solution.num_false_negatives;
				result.pareto_trees.push_back(pareto_tree);
			}																	 
			//result.decision_tree_ = state_.ReconstructBestParetoTree()
			//if (verbose) std::cout << best_assignments_.misclassifications << " " << best_assignments_.NumNodes() << " " << GetRuntimeInSeconds() << "\n";
			return result;
		}
		else
		{
			runtime_assert(1 == 2); //not supported at this time; trees are always satisfiable
			Result result;
			result.misclassifications = UINT32_MAX;
			result.is_proven_optimal = true;
			result.decision_tree_ = NULL;

			if (verbose) std::cout << "unsat " << GetRuntimeInSeconds() << "\n";

			return result;
		}
	}
}