#include "specialised_optimal_binary_classification_tree_solver_depth2.h"
#include "state.h"

namespace DecisionTreeSolverMur
{
	DecisionTreeSolverMur::SpecialisedOptimalBinaryClassificationTreeSolverDepth2::SpecialisedOptimalBinaryClassificationTreeSolverDepth2(size_t num_features, uint32_t num_total_data_points) :
		num_features_(num_features),
		penalty_computer_(num_features, num_total_data_points),
		best_children_info_(num_features)
	{
	}

	AssignmentContainer SpecialisedOptimalBinaryClassificationTreeSolverDepth2::Solve(LearningData & data, uint32_t max_node_budget)
	{
		assert(max_node_budget == 1 || max_node_budget == 2 || max_node_budget == 3);
		
		//depth one are automatically delegated to the depth 1 solver and no internal structures are updated
		//this is done to avoid cases where a depth one tree is called, and then a depth 3 with the same data is called
		//in that case the solver would believe the previously computed values are relevant for the depth 3 tree but they are not
		if (max_node_budget == 1)
		{
			return tree_solver_depth1_.ComputeOptimalAssignments(data);
		}

		InitialiseDataStructures(data);

		//see if you can just reuse a previous computation
		if (penalty_computer_.difference_computer_.seen_last_time_.size() > 0
			&& penalty_computer_.difference_computer_.NumDifferences() == 0
			)
		{
			if (max_node_budget == 2)
			{
				return optimal_at_most_two_nodes;
			}
			else
			{
				runtime_assert(max_node_budget == 3);
				return optimal_at_most_three_nodes;
			}
		}

		//optimal_at_most_one_node.Nullify();
		optimal_at_most_two_nodes.Clear();
		optimal_at_most_three_nodes.Clear();
		
		//optimal_at_most_one_node.misclassifications = data.ComputeClassificationCost();
		//optimal_at_most_two_nodes.misclassifications = data.ComputeClassificationCost();
		
		DecisionNodeAssignment empty_assignment(UINT32_MAX, 0, 0, data.NumInstancesForLabel(0), 0);
		optimal_at_most_three_nodes.AddAssignment(empty_assignment);
		optimal_at_most_two_nodes.AddAssignment(empty_assignment);
		empty_assignment.num_false_positives = 0;
		empty_assignment.num_false_negatives = data.NumInstancesForLabel(1);
		optimal_at_most_three_nodes.AddAssignment(empty_assignment);
		optimal_at_most_two_nodes.AddAssignment(empty_assignment);

		for (size_t f1 = 0; f1 < num_features_; f1++)
		{
			//update the penalty using one node
			/*uint32_t penalty_left_classification = std::min(penalty_computer_.PositivesZeroZero(f1, f1), penalty_computer_.NegativesZeroZero(f1, f1));
			uint32_t penalty_right_classification = std::min(penalty_computer_.PositivesOneOne(f1, f1), penalty_computer_.NegativesOneOne(f1, f1));
			uint32_t penalty_one_node = penalty_left_classification + penalty_right_classification;
			if (penalty_one_node < optimal_at_most_one_node.misclassifications)
			{
				optimal_at_most_one_node.feature = f1;
				optimal_at_most_one_node.misclassifications = penalty_one_node;
				optimal_at_most_one_node.left_node_count = 0;
				optimal_at_most_one_node.right_node_count = 0;
			}*/

			//for (size_t f2 = f1 + 1; f2 < num_features_; f2++)
			for (size_t f2 = 0; f2 < num_features_; f2++)
			{
				if (f1 == f2) { continue; }
				/*uint32_t penalty_branch_one_one = penalty_computer_.PenaltyBranchOneOne(f1, f2);
				uint32_t penalty_branch_one_zero = penalty_computer_.PenaltyBranchOneZero(f1, f2);
				uint32_t penalty_branch_zero_one = penalty_computer_.PenaltyBranchZeroOne(f1, f2);
				uint32_t penalty_branch_zero_zero = penalty_computer_.PenaltyBranchZeroZero(f1, f2);
				*/

				// 1 1
				uint32_t false_positives_one_one_label0 = 0;
				uint32_t false_negatives_one_one_label0 = penalty_computer_.PositivesOneOne(f1, f2);
				
				uint32_t false_positives_one_one_label1 = penalty_computer_.NegativesOneOne(f1, f2);
				uint32_t false_negatives_one_one_label1 = 0;
				
				// 0 1 
				uint32_t false_positives_zero_one_label0 = 0;
				uint32_t false_negatives_zero_one_label0 = penalty_computer_.PositivesZeroOne(f1, f2);
				
				uint32_t false_positives_zero_one_label1 = penalty_computer_.NegativesZeroOne(f1, f2);
				uint32_t false_negatives_zero_one_label1 = 0;
				
				// 1 0
				uint32_t false_positives_one_zero_label0 = 0;
				uint32_t false_negatives_one_zero_label0 = penalty_computer_.PositivesOneZero(f1, f2);
				
				uint32_t false_positives_one_zero_label1 = penalty_computer_.NegativesOneZero(f1, f2);
				uint32_t false_negatives_one_zero_label1 = 0;
				
				// 0 0
				uint32_t false_positives_zero_zero_label0 = 0;
				uint32_t false_negatives_zero_zero_label0 = penalty_computer_.PositivesZeroZero(f1, f2);
				
				uint32_t false_positives_zero_zero_label1 = penalty_computer_.NegativesZeroZero(f1, f2);
				uint32_t false_negatives_zero_zero_label1 = 0;
				
				//update using f1 as the root and f2 as the child
				
				//core for updating the best left child
				DecisionNodeAssignment temp(f2, 0, 0, 0, 0);

				//labels: 0 0				
				temp.num_false_positives = false_positives_zero_zero_label0 + false_positives_zero_one_label0;
				temp.num_false_negatives = false_negatives_zero_zero_label0 + false_negatives_zero_one_label0;
				//if (temp.Misclassifications() == 0) { assert(CheckPerfectLeftChild(f1, temp, data)); }
				UpdateBestLeftChild(f1, temp);

				//labels: 0 1				
				temp.num_false_positives = false_positives_zero_zero_label0 + false_positives_zero_one_label1;
				temp.num_false_negatives = false_negatives_zero_zero_label0 + false_negatives_zero_one_label1;
				//if (temp.Misclassifications() == 0) { assert(CheckPerfectLeftChild(f1, temp, data)); }
				UpdateBestLeftChild(f1, temp);

				//labels: 1 0				
				temp.num_false_positives = false_positives_zero_zero_label1 + false_positives_zero_one_label0;
				temp.num_false_negatives = false_negatives_zero_zero_label1 + false_negatives_zero_one_label0;
				//if (temp.Misclassifications() == 0) { assert(CheckPerfectLeftChild(f1, temp, data)); }
				UpdateBestLeftChild(f1, temp);

				//labels: 1 1				
				temp.num_false_positives = false_positives_zero_zero_label1 + false_positives_zero_one_label1;
				temp.num_false_negatives = false_negatives_zero_zero_label1 + false_negatives_zero_one_label1;
				//if (temp.Misclassifications() == 0) { assert(CheckPerfectLeftChild(f1, temp, data)); }
				UpdateBestLeftChild(f1, temp);


				//update the right child
				
				//labels: 0 0				
				temp.num_false_positives = false_positives_one_one_label0 + false_positives_one_zero_label0;
				temp.num_false_negatives = false_negatives_one_one_label0 + false_negatives_one_zero_label0;
				UpdateBestRightChild(f1, temp);

				//labels: 0 1				
				temp.num_false_positives = false_positives_one_one_label1 + false_positives_one_zero_label0;
				temp.num_false_negatives = false_negatives_one_one_label1 + false_negatives_one_zero_label0;
				UpdateBestRightChild(f1, temp);

				//labels: 1 0				
				temp.num_false_positives = false_positives_one_one_label0 + false_positives_one_zero_label1;
				temp.num_false_negatives = false_negatives_one_one_label0 + false_negatives_one_zero_label1;
				UpdateBestRightChild(f1, temp);

				//labels: 1 1				
				temp.num_false_positives = false_positives_one_one_label1 + false_positives_one_zero_label1;
				temp.num_false_negatives = false_negatives_one_one_label1 + false_negatives_one_zero_label1;
				UpdateBestRightChild(f1, temp);

				//---now do the same update but with f2 as the root
				//todo: for now I am just doing more loops, but it would be better to do as before
				//temp.feature = f1;
				
				//find the best option using only two nodes

				//use f1 as root
				//UpdateBestTwoNodeAssignment(f1, penalty_left_classification, penalty_right_classification);
				//use f2 as root
				//UpdateBestTwoNodeAssignment(f2, penalty_computer_.PenaltyBranchZeroZero(f2, f2), penalty_computer_.PenaltyBranchOneOne(f2, f2));
			}
			UpdateBestThreeNodeAssignment(f1); //it is important to call this after the computing the best left and right subtree (the previous two)
			UpdateBestTwoNodeAssignment(f1);
		}
		if (max_node_budget == 2) { return optimal_at_most_two_nodes; }
		else { assert(max_node_budget == 3); return optimal_at_most_three_nodes; }
		
		//now take care to adjust assignment so that increasing the node count decreases the misclassification score
		/*assert(optimal_at_most_one_node.misclassifications <= data.ComputeClassificationCost());
		assert(optimal_at_most_one_node.misclassifications >= optimal_at_most_two_nodes.misclassifications);
		if (optimal_at_most_one_node.misclassifications == optimal_at_most_two_nodes.misclassifications)
		{
			optimal_at_most_two_nodes = optimal_at_most_one_node;
		}

		assert(optimal_at_most_two_nodes.misclassifications >= optimal_at_most_three_nodes.misclassifications);
		if (optimal_at_most_two_nodes.misclassifications == optimal_at_most_three_nodes.misclassifications)
		{
			optimal_at_most_three_nodes = optimal_at_most_two_nodes;
		}

		if (max_node_budget == 2) { return optimal_at_most_two_nodes; }
		else { assert(max_node_budget == 3); return optimal_at_most_three_nodes; }*/
	}

	void SpecialisedOptimalBinaryClassificationTreeSolverDepth2::SolveFull(DecisionNode * node, State *state, DecisionTree * tree)
	{
		runtime_assert(1 == 2);

		/*DecisionNodeAssignment assignment = Solve(*(node->GetData()), node->GetNodeBudget());
		node->is_exhausted_ = false; //hax todo, when changing the assignment we need to unexhaust the node otherwise we cannot finalise/exhaust an already exhausted node
		node->Finalise(assignment);

		if (assignment.NumNodes() == 0) { return; }

		DecisionNode * left_child_copy, *right_child_copy;
		left_child_copy = tree->PopFreeNode();
		right_child_copy = tree->PopFreeNode();
		bool success = state->PrepareDataForChildren(*node->GetData(), assignment.feature, *left_child_copy->GetData(), *right_child_copy->GetData());
		if (!success) { std::cout << "...issue in reconstruction\n"; exit(1); }
		tree->SetChildren(node, left_child_copy, right_child_copy);
		left_child_copy->Activate(assignment.left_node_count);
		right_child_copy->Activate(assignment.right_node_count);

		if (assignment.left_node_count == 1)
		{
			int feature_left = best_children_info_[assignment.feature].left_child_feature;
			if (left_child_copy->GetData()->ComputeClassificationCost() == best_children_info_[assignment.feature].left_child_penalty)
			{
				feature_left = UINT32_MAX;
			}

			left_child_copy->AssignFeatureTemporary(feature_left);
			//misclassification is not properly set...
			left_child_copy->MarkExhausted();
			if (left_child_copy->GetData()->ComputeClassificationCost() > best_children_info_[assignment.feature].left_child_penalty)
			{
				auto a = tree->PopFreeNode();
				auto b = tree->PopFreeNode();
				state->PrepareDataForChildren(*left_child_copy->GetData(), left_child_copy->GetCurrentAssignment().feature, *a->GetData(), *b->GetData());
				tree->SetChildren(left_child_copy, a, b);
				a->Activate(0);
				b->Activate(0);
			}
		}

		if (assignment.right_node_count == 1)
		{
			int feature_right = best_children_info_[assignment.feature].left_child_feature;
			if (right_child_copy->GetData()->ComputeClassificationCost() == best_children_info_[assignment.feature].right_child_penalty)
			{
				feature_right = UINT32_MAX;
			}

			right_child_copy->AssignFeatureTemporary(best_children_info_[assignment.feature].right_child_feature);
			right_child_copy->MarkExhausted();

			if (right_child_copy->GetData()->ComputeClassificationCost() > best_children_info_[assignment.feature].right_child_penalty)
			{
				auto a = tree->PopFreeNode();
				auto b = tree->PopFreeNode();
				state->PrepareDataForChildren(*right_child_copy->GetData(), right_child_copy->GetCurrentAssignment().feature, *a->GetData(), *b->GetData());
				tree->SetChildren(right_child_copy, a, b);
				a->Activate(0);
				b->Activate(0);
			}
		}		*/
	}

	bool SpecialisedOptimalBinaryClassificationTreeSolverDepth2::IsTerminalNode(DecisionNode * node, State *state)
	{
		//a terminal node is a node that should be processed with a specialised algorithm
		if (node->GetNodeBudget() == 1)
		{
			return true;
		}
		if (node->GetNodeBudget() == 2)
		{
			return true;
		}
		else if (node->GetNodeBudget() == 3 && (state->decision_tree_.GetNodeDepth(node) + 1 == state->parameters_.max_depth))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	int SpecialisedOptimalBinaryClassificationTreeSolverDepth2::ProbeDifference(LearningData & data)
	{
		return penalty_computer_.difference_computer_.ProbeDifference(data);
	}

	bool SpecialisedOptimalBinaryClassificationTreeSolverDepth2::CheckPerfectLeftChild(int feature, DecisionNodeAssignment& assignment, LearningData& data)
	{
		int pos_zero_zero = 0;
		int neg_zero_zero = 0;
		int pos_zero_one = 0;
		int neg_zero_one = 0;

		for (const FeatureVectorBinary& fv : data[0])
		{
			if (fv[feature]) { continue; }

			if (fv[assignment.feature]) { neg_zero_one++; }
			else { neg_zero_zero++; }
		}

		for (const FeatureVectorBinary& fv : data[1])
		{
			if (fv[feature]) { continue; }

			if (fv[assignment.feature]) { pos_zero_one++; }
			else { pos_zero_zero++; }
		}

		int c1 = std::min(pos_zero_zero, neg_zero_zero);
		int c2 = std::min(pos_zero_one, neg_zero_one);

		if (!(c1 == 0 && c2 == 0))
		{
			for (const FeatureVectorBinary& fv : data[1])
			{
				if (fv[26] && !fv[27])
				{
					std::cout << "kekw: ";
					std::cout << fv << "\n";
				}
			}
		}

		assert(c1 == 0 && c2 == 0);
		return c1 == 0 && c2 == 0;
	}

	double SpecialisedOptimalBinaryClassificationTreeSolverDepth2::GetFeatureSimiliarity(uint32_t f1, uint32_t f2, LearningData &data)
	{
		if (f1 > f2) { std::swap(f1, f2); }

		uint32_t a1 = penalty_computer_.PositivesOneOne(f1, f2) + penalty_computer_.PositivesZeroZero(f1, f2);
		uint32_t b1 = penalty_computer_.PositivesOneZero(f1, f2) + penalty_computer_.PositivesZeroOne(f1, f2);
		uint32_t c1 = std::max(a1, b1);

		uint32_t a2 = penalty_computer_.NegativesOneOne(f1, f2) + penalty_computer_.NegativesZeroZero(f1, f2);
		uint32_t b2 = penalty_computer_.NegativesOneZero(f1, f2) + penalty_computer_.NegativesZeroOne(f1, f2);
		uint32_t c2 = std::max(a2, b2);

		return ((double(c1) / data.NumInstancesForLabel(1)) + (double(c2) / data.NumInstancesForLabel(0))) / 2;
	}

	bool SpecialisedOptimalBinaryClassificationTreeSolverDepth2::AreFeaturesEquivalent(uint32_t f1, uint32_t f2)
	{
		//two features are identical if they appear in the same positive and same negative features

		//let's check if they are really identical
		bool b1 = (penalty_computer_.PositivesOneOne(f1, f1) == penalty_computer_.PositivesOneOne(f2, f2));
		bool b2 = (penalty_computer_.NegativesOneOne(f1, f1) == penalty_computer_.NegativesOneOne(f2, f2));

		if (!b1 || !b2) { return false; }

		bool b3 = (penalty_computer_.PositivesOneOne(f1, f2) == penalty_computer_.PositivesOneOne(f1, f1));
		bool b4 = (penalty_computer_.NegativesOneOne(f1, f2) == penalty_computer_.NegativesOneOne(f1, f1));

		if (!b3 || !b4) { return false; }

		if (b1 && b2 && b3 && b4) { return true; }

		//let's check if the are the inverse of each other
		b1 = (penalty_computer_.PositivesOneOne(f1, f1) == (penalty_computer_.num_positives_ - penalty_computer_.PositivesOneOne(f2, f2)));
		b2 = (penalty_computer_.NegativesOneOne(f1, f1) == (penalty_computer_.num_negatives_ - penalty_computer_.NegativesOneOne(f2, f2)));

		if (!b1 || !b2) { return false; }

		b3 = (penalty_computer_.PositivesOneOne(f1, f2) == (penalty_computer_.num_positives_ - penalty_computer_.PositivesOneOne(f1, f1)));
		b4 = (penalty_computer_.NegativesOneOne(f1, f2) == (penalty_computer_.num_negatives_ - penalty_computer_.NegativesOneOne(f1, f1)));

		//or, if one appears in exactly those that the other doesn't appear

		return b1 && b2 && b3 && b4;
	}

	void SpecialisedOptimalBinaryClassificationTreeSolverDepth2::InitialiseDataStructures(LearningData & data)
	{
		InitialiseChildrenInfo();
		penalty_computer_.Initialise(data);
	}

	void SpecialisedOptimalBinaryClassificationTreeSolverDepth2::InitialiseChildrenInfo()
	{
		//memset(&children_info_[0], UINT32_MAX, sizeof(ChildrenInformation)*num_features_);
		for (uint32_t i = 0; i < num_features_; i++)
		{
			best_children_info_[i].left_child_assignments.Clear();
			best_children_info_[i].right_child_assignments.Clear();

			//best_children_info_[i].left_child_assignments.hax_filter_ = true;
			//best_children_info_[i].right_child_assignments.hax_filter_ = true;
		}
	}

	void SpecialisedOptimalBinaryClassificationTreeSolverDepth2::UpdateBestLeftChild(uint32_t root_feature, DecisionNodeAssignment& left_child_assignment)
	{
		best_children_info_[root_feature].left_child_assignments.AddAssignment(left_child_assignment);
	}

	void SpecialisedOptimalBinaryClassificationTreeSolverDepth2::UpdateBestRightChild(uint32_t root_feature, DecisionNodeAssignment& right_child_assignment)
	{
		best_children_info_[root_feature].right_child_assignments.AddAssignment(right_child_assignment);
	}

	void SpecialisedOptimalBinaryClassificationTreeSolverDepth2::UpdateBestTwoNodeAssignment(uint32_t root_feature)
	{
		runtime_assert(best_children_info_[root_feature].left_child_assignments.Size() > 0);
		runtime_assert(best_children_info_[root_feature].right_child_assignments.Size() > 0);
		
		uint32_t num_positive_instances_left = penalty_computer_.PositivesZeroZero(root_feature, root_feature);
		uint32_t num_negative_instances_left = penalty_computer_.NegativesZeroZero(root_feature, root_feature);
		uint32_t num_positive_instances_right = penalty_computer_.PositivesOneOne(root_feature, root_feature);
		uint32_t num_negative_instances_right = penalty_computer_.NegativesOneOne(root_feature, root_feature);

		//(feature, left_node_count, right_node_count, num_false_positives, num_false_negatives) :
		DecisionNodeAssignment right_leaf_label0(UINT32_MAX, 0, 0, 0, 0);
		right_leaf_label0.num_false_positives = 0;
		right_leaf_label0.num_false_negatives = num_positive_instances_right;

		DecisionNodeAssignment right_leaf_label1(UINT32_MAX, 0, 0, 0, 0);
		right_leaf_label1.num_false_positives = num_negative_instances_right;
		right_leaf_label1.num_false_negatives = 0;

		AssignmentContainer right_leaves;
		right_leaves.AddAssignment(right_leaf_label0);
		right_leaves.AddAssignment(right_leaf_label1);

		//------

		DecisionNodeAssignment left_leaf_label0(UINT32_MAX, 0, 0, 0, 0);
		left_leaf_label0.num_false_positives = 0;
		left_leaf_label0.num_false_negatives = num_positive_instances_left;

		DecisionNodeAssignment left_leaf_label1(UINT32_MAX, 0, 0, 0, 0);
		left_leaf_label1.num_false_positives = num_negative_instances_left;
		left_leaf_label1.num_false_negatives = 0;

		AssignmentContainer left_leaves;
		left_leaves.AddAssignment(left_leaf_label0);
		left_leaves.AddAssignment(left_leaf_label1);

		//-------

		AssignmentContainer temp = best_children_info_[root_feature].left_child_assignments;
		temp.CombineUsingAddition(right_leaves, root_feature, 1, 0);		
		optimal_at_most_two_nodes.AddAssignments(temp);
		
		temp = best_children_info_[root_feature].right_child_assignments;
		temp.CombineUsingAddition(left_leaves, root_feature, 0, 1);
		optimal_at_most_two_nodes.AddAssignments(temp);

		/*int penalty_two_nodes = best_children_info_[root_feature].left_child_penalty + right_misclass;
		if (optimal_at_most_two_nodes.misclassifications > penalty_two_nodes)
		{
			optimal_at_most_two_nodes.misclassifications = penalty_two_nodes;
			optimal_at_most_two_nodes.feature = root_feature;
			optimal_at_most_two_nodes.left_node_count = 1;
			optimal_at_most_two_nodes.right_node_count = 0;
		}

		penalty_two_nodes = best_children_info_[root_feature].right_child_penalty + left_misclass;
		if (optimal_at_most_two_nodes.misclassifications > penalty_two_nodes)
		{
			optimal_at_most_two_nodes.misclassifications = penalty_two_nodes;
			optimal_at_most_two_nodes.feature = root_feature;
			optimal_at_most_two_nodes.left_node_count = 0;
			optimal_at_most_two_nodes.right_node_count = 1;
		}*/
	}

	void SpecialisedOptimalBinaryClassificationTreeSolverDepth2::UpdateBestThreeNodeAssignment(uint32_t root_feature)
	{
		runtime_assert(best_children_info_[root_feature].left_child_assignments.Size() > 0);
		runtime_assert(best_children_info_[root_feature].right_child_assignments.Size() > 0);

		AssignmentContainer temp = best_children_info_[root_feature].left_child_assignments;
		temp.CombineUsingAddition(best_children_info_[root_feature].right_child_assignments, root_feature, 1, 1);
		optimal_at_most_three_nodes.AddAssignments(temp);
		
		//actually we cannot just combine like this
		/*uint32_t new_penalty = best_children_info_[root_feature].left_child_penalty + best_children_info_[root_feature].right_child_penalty;
		if (optimal_at_most_three_nodes.misclassifications > new_penalty)
		{
			optimal_at_most_three_nodes.misclassifications = new_penalty;
			optimal_at_most_three_nodes.feature = root_feature;
			optimal_at_most_three_nodes.left_node_count = 1;
			optimal_at_most_three_nodes.right_node_count = 1;
		}*/
	}
}