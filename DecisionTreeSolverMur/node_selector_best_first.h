#pragma once

#include "node_selector_abstract.h"

namespace DecisionTreeSolverMur
{
	class NodeSelectorBestFirst : public NodeSelectorAbstract
	{
	public:
		NodeSelectorBestFirst(State * state);
		DecisionNode * GetNextNode();

	private:
		bool IsNodeBetter(DecisionNode *new_node, DecisionNode *reference_node);
	};


	NodeSelectorBestFirst::NodeSelectorBestFirst(State * state) :
		NodeSelectorAbstract(state)
	{}

	DecisionNode * NodeSelectorBestFirst::GetNextNode()
	{
		static std::vector<DecisionNode*> stack;
		stack.clear();

		DecisionNode *best_node = NULL;
		stack.push_back(state_->GetRootNode());
		while (!stack.empty())
		{
			DecisionNode * node = stack.back();
			stack.pop_back();

			if (GetNodeType(node) != 3 && best_node == NULL)
			{
				best_node = node;
			}
			else if (GetNodeType(node) != 3 && IsNodeBetter(node, best_node))
			{
				best_node = node;
			}

			if (!state_->decision_tree_.HasChildren(node)) { continue; }
			
			auto left_child = state_->decision_tree_.GetLeftChild(node);
			if (!left_child->IsExhausted()) { stack.push_back(left_child); }
			
			auto right_child = state_->decision_tree_.GetRightChild(node);
			if (!right_child->IsExhausted()) { stack.push_back(right_child); }
		}
		assert(best_node != NULL);
		return best_node;
	}

	//returns if the first node is more favourable than the second node
	bool NodeSelectorBestFirst::IsNodeBetter(DecisionNode * new_node, DecisionNode * reference_node)
	{
		runtime_assert(1 == 2);
		return false;
		/*
		//tier1: nodes with no descendents that are not terminal
		//tier2: nodes with no descendents that are terminal

		//tie breaking is not clear

		uint32_t node1_type = GetNodeType(new_node);
		uint32_t node2_type = GetNodeType(reference_node);

		if (node1_type < node2_type) { return true; }

		if (node1_type > node2_type) { return false; }

		//at this point, both node types are equal
		if (new_node->HasFeasibleSolution() && reference_node->HasFeasibleSolution())
		{
			auto a1 = new_node->GetBestAssignments();
			auto a2 = reference_node->GetBestAssignments();
			return (double(a1.misclassifications) / a1.NumNodes()) > (double(a2.misclassifications) / a2.NumNodes());
		}

		if (!new_node->HasFeasibleSolution() && !reference_node->HasFeasibleSolution())
		{
			return (double(new_node->GetModifiedMisclassificationBudget()) / new_node->GetNodeBudget()) > (double(reference_node->GetModifiedMisclassificationBudget()) / reference_node->GetNodeBudget());
		}

		if (new_node->HasFeasibleSolution() && !reference_node->HasFeasibleSolution())
		{
			auto a1 = new_node->GetBestAssignments();
			return (double(a1.misclassifications) / a1.NumNodes()) > (double(reference_node->GetModifiedMisclassificationBudget()) / reference_node->GetNodeBudget());
		}

		if (!new_node->HasFeasibleSolution() && reference_node->HasFeasibleSolution())
		{
			auto a2 = reference_node->GetBestAssignments();
			return (double(new_node->GetModifiedMisclassificationBudget()) / new_node->GetNodeBudget()) > (double(a2.misclassifications) / a2.NumNodes());
		}
		*/
		assert(1 == 2); //should never reach this
	}
}