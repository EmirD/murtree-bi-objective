#include "node_budget_selector_abstract.h"
#include "decision_tree.h"

namespace DecisionTreeSolverMur
{

	NodeBudgetSelectorAbstract::NodeBudgetSelectorAbstract(DecisionTree * tree, DecisionNode * node) :
		allowed_node_budget_(UINT32_MAX),
		tree_(tree),
		node_(node)
	{}

	NodeBudgetSelectorAbstract::~NodeBudgetSelectorAbstract()
	{
	}

	uint32_t NodeBudgetSelectorAbstract::GetNodeBudget() const
	{
		return allowed_node_budget_;
	}

	uint32_t NodeBudgetSelectorAbstract::MaxNumNodesInChildSubTree()
	{
		assert(allowed_node_budget_ > 0); //the allowed budget can be zero, but then this method is never called

		uint32_t limit_based_on_height = (uint32_t(1) << tree_->GetSubtreeMaxFeatureHeight(node_)) - 1;
		return std::min(limit_based_on_height, allowed_node_budget_-1); //minus one to take into account this node
	}

}