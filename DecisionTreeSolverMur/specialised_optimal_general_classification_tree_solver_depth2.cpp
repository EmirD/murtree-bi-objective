#include "specialised_optimal_general_classification_tree_solver_depth2.h"
#include "state.h"
#include "decision_node.h"

namespace DecisionTreeSolverMur
{
	/*SpecialisedOptimalGeneralClassificationTreeSolverDepth2::SpecialisedOptimalGeneralClassificationTreeSolverDepth2(uint32_t num_labels, size_t num_features, uint32_t num_total_data_points) :
		num_features_(num_features),
		penalty_computer_(num_labels, num_features, num_total_data_points),
		best_children_info_(num_features)
	{
	}

	DecisionNodeAssignment SpecialisedOptimalGeneralClassificationTreeSolverDepth2::Solve(LearningData & data, uint32_t max_node_budget)
	{
		assert(max_node_budget == 1 || max_node_budget == 2 || max_node_budget == 3);

		//depth one are automatically delegated to the depth 1 solver and no internal structures are updated
		//this is done to avoid cases where a depth one tree is called, and then a depth 3 with the same data is called
		//in that case the solver would believe the previously computed values are relevant for the depth 3 tree but they are not
		if (max_node_budget == 1)
		{
			tree_solver_depth1_.ComputeOptimalDecisionTree(data);
			DecisionNodeAssignment assignment;
			assignment.feature = tree_solver_depth1_.GetOptimalFeature();
			assignment.misclassifications = tree_solver_depth1_.GetOptimalPenalty();
			assignment.left_node_count = 0;
			assignment.right_node_count = 0;
			return assignment;
		}

		InitialiseDataStructures(data);

		//see if you can just reuse a previous computation
		if (penalty_computer_.difference_computer_.seen_last_time_.size() > 0
			&& penalty_computer_.difference_computer_.NumDifferences() == 0)
		{
			if (max_node_budget == 2)
			{
				return optimal_at_most_two_nodes;
			}
			else
			{
				assert(max_node_budget == 3);
				return optimal_at_most_three_nodes;
			}
		}

		optimal_at_most_two_nodes.Nullify();
		optimal_at_most_three_nodes.Nullify();
		optimal_at_most_one_node.Nullify();

		optimal_at_most_one_node.misclassifications = data.ComputeClassificationCost();
		optimal_at_most_two_nodes.misclassifications = data.ComputeClassificationCost();
		optimal_at_most_three_nodes.misclassifications = data.ComputeClassificationCost();

		for (size_t f1 = 0; f1 < num_features_; f1++)
		{
			//update the penalty using one node
			uint32_t penalty_left_classification = penalty_computer_.PenaltyBranchZeroZero(f1, f1);
			uint32_t penalty_right_classification = penalty_computer_.PenaltyBranchOneOne(f1, f1);
			uint32_t penalty_one_node = penalty_left_classification + penalty_right_classification;
			if (penalty_one_node < optimal_at_most_one_node.misclassifications)
			{
				optimal_at_most_one_node.feature = f1;
				optimal_at_most_one_node.misclassifications = penalty_one_node;
				optimal_at_most_one_node.left_node_count = 0;
				optimal_at_most_one_node.right_node_count = 0;
			}

			for (size_t f2 = f1 + 1; f2 < num_features_; f2++)
			{
				uint32_t penalty_branch_one_one = penalty_computer_.PenaltyBranchOneOne(f1, f2);
				uint32_t penalty_branch_one_zero = penalty_computer_.PenaltyBranchOneZero(f1, f2);
				uint32_t penalty_branch_zero_one = penalty_computer_.PenaltyBranchZeroOne(f1, f2);
				uint32_t penalty_branch_zero_zero = penalty_computer_.PenaltyBranchZeroZero(f1, f2);

				UpdateBestLeftChild(f1, f2, penalty_branch_zero_one + penalty_branch_zero_zero);
				UpdateBestRightChild(f1, f2, penalty_branch_one_one + penalty_branch_one_zero);

				UpdateBestLeftChild(f2, f1, penalty_branch_one_zero + penalty_branch_zero_zero);
				UpdateBestRightChild(f2, f1, penalty_branch_one_one + penalty_branch_zero_one);

				//find the best option using only two nodes

				//use f1 as root
				UpdateBestTwoNodeAssignment(f1, penalty_left_classification, penalty_right_classification);
				//use f2 as root
				UpdateBestTwoNodeAssignment(f2, penalty_computer_.PenaltyBranchZeroZero(f2, f2), penalty_computer_.PenaltyBranchOneOne(f2, f2));
			}
			UpdateBestThreeNodeAssignment(f1); //it is important to call this after the computing the best left and right subtree (the previous two)
		}

		//now take care to adjust assignment so that increasing the node count decreases the misclassification score
		assert(optimal_at_most_one_node.misclassifications <= data.ComputeClassificationCost());
		assert(optimal_at_most_one_node.misclassifications >= optimal_at_most_two_nodes.misclassifications);
		if (optimal_at_most_one_node.misclassifications == optimal_at_most_two_nodes.misclassifications)
		{
			optimal_at_most_two_nodes = optimal_at_most_one_node;
		}

		assert(optimal_at_most_two_nodes.misclassifications >= optimal_at_most_three_nodes.misclassifications);
		if (optimal_at_most_two_nodes.misclassifications == optimal_at_most_three_nodes.misclassifications)
		{
			optimal_at_most_three_nodes = optimal_at_most_two_nodes;
		}

		if (max_node_budget == 2) { return optimal_at_most_two_nodes; }
		else { assert(max_node_budget == 3); return optimal_at_most_three_nodes; }
	}

	void SpecialisedOptimalGeneralClassificationTreeSolverDepth2::SolveFull(DecisionNode * node, State * state, DecisionTree * tree)
	{
		DecisionNodeAssignment assignment = Solve(*(node->GetData()), node->GetNodeBudget());
		node->is_exhausted_ = false; //hax todo, when changing the assignment we need to unexhaust the node otherwise we cannot finalise/exhaust an already exhausted node
		node->Finalise(assignment);

		if (assignment.NumNodes() == 0) { return; }

		DecisionNode * left_child_copy, *right_child_copy;
		left_child_copy = tree->PopFreeNode();
		right_child_copy = tree->PopFreeNode();
		bool success = state->PrepareDataForChildren(*node->GetData(), assignment.feature, *left_child_copy->GetData(), *right_child_copy->GetData());
		if (!success) { std::cout << "...issue in reconstruction\n"; exit(1); }
		tree->SetChildren(node, left_child_copy, right_child_copy);
		left_child_copy->Activate(assignment.left_node_count);
		right_child_copy->Activate(assignment.right_node_count);

		if (assignment.left_node_count == 1)
		{
			int feature_left = best_children_info_[assignment.feature].left_child_feature;
			if (left_child_copy->GetData()->ComputeClassificationCost() == best_children_info_[assignment.feature].left_child_penalty)
			{
				feature_left = UINT32_MAX;
			}

			left_child_copy->AssignFeatureTemporary(feature_left);
			//misclassification is not properly set...
			left_child_copy->MarkExhausted();
			if (left_child_copy->GetData()->ComputeClassificationCost() > best_children_info_[assignment.feature].left_child_penalty)
			{
				auto a = tree->PopFreeNode();
				auto b = tree->PopFreeNode();
				state->PrepareDataForChildren(*left_child_copy->GetData(), left_child_copy->GetCurrentAssignment().feature, *a->GetData(), *b->GetData());
				tree->SetChildren(left_child_copy, a, b);
				a->Activate(0);
				b->Activate(0);
			}
		}

		if (assignment.right_node_count == 1)
		{
			int feature_right = best_children_info_[assignment.feature].left_child_feature;
			if (right_child_copy->GetData()->ComputeClassificationCost() == best_children_info_[assignment.feature].right_child_penalty)
			{
				feature_right = UINT32_MAX;
			}

			right_child_copy->AssignFeatureTemporary(best_children_info_[assignment.feature].right_child_feature);
			right_child_copy->MarkExhausted();

			if (right_child_copy->GetData()->ComputeClassificationCost() > best_children_info_[assignment.feature].right_child_penalty)
			{
				auto a = tree->PopFreeNode();
				auto b = tree->PopFreeNode();
				state->PrepareDataForChildren(*right_child_copy->GetData(), right_child_copy->GetCurrentAssignment().feature, *a->GetData(), *b->GetData());
				tree->SetChildren(right_child_copy, a, b);
				a->Activate(0);
				b->Activate(0);
			}
		}
	}

	bool SpecialisedOptimalGeneralClassificationTreeSolverDepth2::IsTerminalNode(DecisionNode * node, State * state)
	{
		//a terminal node is a node that should be processed with a specialised algorithm
		if (node->GetNodeBudget() == 1)
		{
			return true;
		}
		if (node->GetNodeBudget() == 2)
		{
			return true;
		}
		else if (node->GetNodeBudget() == 3 && (state->decision_tree_.GetNodeDepth(node) + 1 == state->parameters_.max_depth))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	int SpecialisedOptimalGeneralClassificationTreeSolverDepth2::ProbeDifference(LearningData & data)
	{
		return penalty_computer_.difference_computer_.ProbeDifference(data);
	}

	void SpecialisedOptimalGeneralClassificationTreeSolverDepth2::InitialiseDataStructures(LearningData & data)
	{
		InitialiseChildrenInfo();
		penalty_computer_.Initialise(data);
	}

	void SpecialisedOptimalGeneralClassificationTreeSolverDepth2::InitialiseChildrenInfo()
	{
		//memset(&children_info_[0], UINT32_MAX, sizeof(ChildrenInformation)*num_features_);
		for (uint32_t i = 0; i < num_features_; i++)
		{
			best_children_info_[i].left_child_feature = UINT32_MAX;
			best_children_info_[i].left_child_penalty = UINT32_MAX;
			best_children_info_[i].right_child_feature = UINT32_MAX;
			best_children_info_[i].right_child_penalty = UINT32_MAX;
		}
	}

	void SpecialisedOptimalGeneralClassificationTreeSolverDepth2::UpdateBestLeftChild(uint32_t root_feature, uint32_t left_child_feature, uint32_t penalty)
	{
		if (best_children_info_[root_feature].left_child_penalty > penalty)
		{
			best_children_info_[root_feature].left_child_feature = left_child_feature;
			best_children_info_[root_feature].left_child_penalty = penalty;
		}
	}

	void SpecialisedOptimalGeneralClassificationTreeSolverDepth2::UpdateBestRightChild(uint32_t root_feature, uint32_t right_child_feature, uint32_t penalty)
	{
		if (best_children_info_[root_feature].right_child_penalty > penalty)
		{
			best_children_info_[root_feature].right_child_feature = right_child_feature;
			best_children_info_[root_feature].right_child_penalty = penalty;
		}
	}

	void SpecialisedOptimalGeneralClassificationTreeSolverDepth2::UpdateBestTwoNodeAssignment(uint32_t root_feature, uint32_t left_misclass, uint32_t right_misclass)
	{
		int penalty_two_nodes = best_children_info_[root_feature].left_child_penalty + right_misclass;
		if (optimal_at_most_two_nodes.misclassifications > penalty_two_nodes)
		{
			optimal_at_most_two_nodes.misclassifications = penalty_two_nodes;
			optimal_at_most_two_nodes.feature = root_feature;
			optimal_at_most_two_nodes.left_node_count = 1;
			optimal_at_most_two_nodes.right_node_count = 0;
		}

		penalty_two_nodes = best_children_info_[root_feature].right_child_penalty + left_misclass;
		if (optimal_at_most_two_nodes.misclassifications > penalty_two_nodes)
		{
			optimal_at_most_two_nodes.misclassifications = penalty_two_nodes;
			optimal_at_most_two_nodes.feature = root_feature;
			optimal_at_most_two_nodes.left_node_count = 0;
			optimal_at_most_two_nodes.right_node_count = 1;
		}
	}

	void SpecialisedOptimalGeneralClassificationTreeSolverDepth2::UpdateBestThreeNodeAssignment(uint32_t root_feature)
	{
		assert(best_children_info_[root_feature].left_child_penalty != UINT32_MAX);
		assert(best_children_info_[root_feature].right_child_penalty != UINT32_MAX);

		uint32_t new_penalty = best_children_info_[root_feature].left_child_penalty + best_children_info_[root_feature].right_child_penalty;
		if (optimal_at_most_three_nodes.misclassifications > new_penalty)
		{
			optimal_at_most_three_nodes.misclassifications = new_penalty;
			optimal_at_most_three_nodes.feature = root_feature;
			optimal_at_most_three_nodes.left_node_count = 1;
			optimal_at_most_three_nodes.right_node_count = 1;
		}
	}*/
}