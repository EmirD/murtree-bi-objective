#include "branch_info.h"

#include <algorithm>

namespace DecisionTreeSolverMur
{

	DecisionTreeSolverMur::BranchInfo::BranchInfo()
	{
	}

	void BranchInfo::AddFeatureBranch(uint32_t feature, bool present)
	{
		uint32_t code = GetCode(feature, present);
		branch_codes_.push_back(code);

		ConvertIntoCanonicalRepresentation();
	}

	uint32_t BranchInfo::Size() const
	{
		return uint32_t(branch_codes_.size());
	}

	uint32_t BranchInfo::operator[](uint32_t i) const
	{
		return branch_codes_[i];
	}

	void BranchInfo::ConvertIntoCanonicalRepresentation()
	{
		std::sort(branch_codes_.begin(), branch_codes_.end());
	}

	uint32_t BranchInfo::GetCode(uint32_t feature, bool present)
	{
		return 2 * feature + present;
	}

}