#include "assignment_container.h"

#include <algorithm>

namespace DecisionTreeSolverMur
{

	AssignmentContainer::AssignmentContainer()
	{
	}

	//todo improve the efficiency of this method
	void AssignmentContainer::AddAssignment(const DecisionNodeAssignment& input_assignment, bool maximise)
	{
		if (maximise == false)
		{
			//stored_assignments_.front.push_back(input_assignment);
			stored_assignments_.insert(input_assignment);
			return;
		}

		//todo this is hacky with the 'maximise' parameter, we need to do something there

		//check if this solution is already dominated by some other solution
		for (DecisionNodeAssignment& stored_assignment : stored_assignments_.front)
		{
			//once we discover that a strictly better solution is already present, we abort
			if (!maximise)
			{
				if (IsAssignmentDominated(input_assignment, stored_assignment)) { return; }
			}
			else
			{
				if (IsAssignmentReverseDominated(input_assignment, stored_assignment)) { return; }
			}			
		}

		//solution is not dominated, we can add it to the pool of solutions
		//	and possibly remove other dominated solutions

		std::vector<DecisionNodeAssignment> old_assignments(stored_assignments_.front);
		stored_assignments_.clear();
		stored_assignments_.front.push_back(input_assignment);
		//readd the old solutions only if they are not dominated by the input
		//	note that old solutions do not dominate each other
		for (DecisionNodeAssignment& old_assignment : old_assignments)
		{
			if (!maximise)
			{
				if (IsAssignmentDominated(old_assignment, input_assignment) == false)
				{
					stored_assignments_.front.push_back(old_assignment);
				}
			}
			else
			{
				if (IsAssignmentReverseDominated(old_assignment, input_assignment) == false)
				{
					stored_assignments_.front.push_back(old_assignment);
				}
			}			
		}
		
		std::sort(
			stored_assignments_.front.begin(), 
			stored_assignments_.front.end(), 
			[](const DecisionNodeAssignment& p1, const DecisionNodeAssignment& p2)->bool
			{
				return p1.num_false_positives < p2.num_false_positives;
			}
		);
	}

	void AssignmentContainer::AddAssignments(const AssignmentContainer& assignments, bool maximise)
	{
		for (auto& assignment : assignments.GetStoredAssignments()) 
		{ 
			AddAssignment(assignment, maximise); 
		}
	}

	void AssignmentContainer::Clear()
	{
		stored_assignments_.clear();
	}

	void AssignmentContainer::CombineUsingSubtraction(const AssignmentContainer& container, uint32_t feature, uint32_t node_budget_left, uint32_t node_budget_right)
	{
		std::vector<DecisionNodeAssignment> combined_assignments;
		for (auto &assignment1 : this->GetStoredAssignments())
		{
			for (auto &assignment2 : container.GetStoredAssignments())
			{
				DecisionNodeAssignment combined_assignment(feature, node_budget_left, node_budget_right, 0, 0);
				//false positives - boring unsigned ints require the comparison
				if (assignment1.num_false_positives >= assignment2.num_false_positives)
				{
					combined_assignment.num_false_positives = (assignment1.num_false_positives - assignment2.num_false_positives);
				}//else stays zero
				
				//false negatives
				if (assignment1.num_false_negatives >= assignment2.num_false_negatives)
				{
					combined_assignment.num_false_negatives = (assignment1.num_false_negatives - assignment2.num_false_negatives);
				}//else stays zero

				combined_assignments.push_back(combined_assignment);
			}
		}

		stored_assignments_.clear();
		for (auto& assignment : combined_assignments) AddAssignment(assignment);

		/*for (size_t i = 0; i < combined_assignments.size(); i++)
		{
			bool should_include_assignment = true;
			for (size_t j = i + 1; j < combined_assignments.size(); j++)
			{
				if (IsAssignmentDominated(combined_assignments[i], combined_assignments[j]))
				{
					should_include_assignment = false;
					break;
				}
			}
			if (should_include_assignment) { stored_assignments_.insert(combined_assignments[i]); }
		}*/
	}

	void AssignmentContainer::CombineUsingAddition(AssignmentContainer& container, uint32_t feature, uint32_t node_budget_left, uint32_t node_budget_right)
	{
		/*std::sort(
			stored_assignments_.front.begin(),
			stored_assignments_.front.end(),
			[](const DecisionNodeAssignment& p1, const DecisionNodeAssignment& p2)->bool
			{
				return p1.num_false_positives < p2.num_false_positives;
			}
		);

		std::sort(
			container.stored_assignments_.front.begin(),
			container.stored_assignments_.front.end(),
			[](const DecisionNodeAssignment& p1, const DecisionNodeAssignment& p2)->bool
			{
				return p1.num_false_positives < p2.num_false_positives;
			}
		);*/

		stored_assignments_.add_and_merge_complex(container.stored_assignments_, feature, node_budget_left, node_budget_right);
		//stored_assignments_.add_and_merge_sort(container.stored_assignments_, feature, node_budget_left, node_budget_right);
		
		//below is the old version
		/*std::vector<DecisionNodeAssignment> combined_assignments;
		for (auto assignment1 : this->GetStoredAssignments())
		{
			for (auto assignment2 : container.GetStoredAssignments())
			{
				runtime_assert(assignment1.num_false_positives != UINT32_MAX);
				runtime_assert(assignment2.num_false_positives != UINT32_MAX);
				runtime_assert(assignment1.num_false_negatives != UINT32_MAX);
				runtime_assert(assignment2.num_false_negatives != UINT32_MAX);

				DecisionNodeAssignment combined_assignment(feature, node_budget_left, node_budget_right, 0, 0);
				//false positives
				combined_assignment.num_false_positives += assignment1.num_false_positives;
				combined_assignment.num_false_positives += assignment2.num_false_positives;
				//false negatives
				combined_assignment.num_false_negatives += assignment1.num_false_negatives;
				combined_assignment.num_false_negatives += assignment2.num_false_negatives;

				combined_assignments.push_back(combined_assignment);
			}
		}
		
		stored_assignments_.clear();
		for (size_t i = 0; i < combined_assignments.size(); i++)
		{
			bool should_include_assignment = true;
			for (size_t j = i + 1; j < combined_assignments.size(); j++)
			{
				if (IsAssignmentDominated(combined_assignments[i], combined_assignments[j]))
				{
					should_include_assignment = false;
					break;
				}
			}
			if (should_include_assignment) { stored_assignments_.insert(combined_assignments[i]); }
		}*/
	}

	void AssignmentContainer::MergeMaximisation(const AssignmentContainer& container)
	{
		for (auto &assignment : container.stored_assignments_.front)
		{
			AddAssignment(assignment, true);
		}
	}

	void AssignmentContainer::MergeMinimisation(const AssignmentContainer& container)
	{
		for (auto &assignment : container.stored_assignments_.front)
		{
			AddAssignment(assignment, false);
		}
	}

	uint32_t AssignmentContainer::Size() const
	{
		return stored_assignments_.length();
	}

	bool AssignmentContainer::IsEmpty() const
	{
		return Size() == 0;
	}

	uint32_t AssignmentContainer::MinNumNodes() const
	{
		uint32_t min_num_nodes = UINT32_MAX;
		for (const DecisionNodeAssignment &assignment : stored_assignments_.front)
		{
			min_num_nodes = std::min(min_num_nodes, assignment.NumNodes());
		}
		return min_num_nodes;
	}

	const std::vector<DecisionNodeAssignment>& AssignmentContainer::GetStoredAssignments() const
	{
		return stored_assignments_.front;
	}

	bool AssignmentContainer::operator!=(const AssignmentContainer& rhs) const
	{
		if (this->Size() != rhs.Size()) { return true; }

		//check if all assignments from the input are contained here
		for (const DecisionNodeAssignment& assignment : rhs.GetStoredAssignments())
		{
			if (IsEquivalentAssignmentPresent(assignment) == false) { return true; } //one difference is enough
		}
		//do the same check but reverse the roles of the instances
		for (const DecisionNodeAssignment& assignment : GetStoredAssignments())
		{
			if (rhs.IsEquivalentAssignmentPresent(assignment) == false) { return true; } //one difference is enough
		}
		return false;
	}

	bool AssignmentContainer::operator==(const AssignmentContainer& rhs) const
	{
		return !((*this) != rhs);
	}

	bool AssignmentContainer::IsAssignmentDominated(const DecisionNodeAssignment& assignment1, const DecisionNodeAssignment& assignment2) const
	{
		return assignment1.num_false_positives >= assignment2.num_false_positives &&
			assignment1.num_false_negatives >= assignment2.num_false_negatives;

		return assignment2.Misclassifications() <= assignment1.Misclassifications();
		//for now a standard metric based on misclassifications
		//if (assignment2.Misclassifications() <= assignment1.Misclassifications()) { return true; }
		//if (assignment2.Misclassifications() > assignment1.Misclassifications()) { return false; }
		//return assignment2.NumNodes() <= assignment1.NumNodes();

		//return assignment1.num_false_positives >= assignment2.num_false_positives &&
		//	assignment1.num_false_negatives >= assignment2.num_false_negatives;
	}

	bool AssignmentContainer::IsAssignmentReverseDominated(const DecisionNodeAssignment& assignment1, const DecisionNodeAssignment& assignment2) const
	{
		return assignment1.num_false_positives <= assignment2.num_false_positives &&
			assignment1.num_false_negatives <= assignment2.num_false_negatives;

		return assignment2.Misclassifications() >= assignment1.Misclassifications();
		//for now a standard metric based on misclassifications
		//if (assignment2.Misclassifications() > assignment1.Misclassifications()) { return true; }
		//if (assignment2.Misclassifications() < assignment1.Misclassifications()) { return false; }
		//return assignment2.NumNodes() >= assignment1.NumNodes();

		//return assignment1.num_false_positives >= assignment2.num_false_positives &&
		//	assignment1.num_false_negatives >= assignment2.num_false_negatives;
	}

	bool AssignmentContainer::AreAssignmentEquivalent(const DecisionNodeAssignment& assign1, const DecisionNodeAssignment& assign2) const
	{
		return assign1.num_false_positives == assign2.num_false_positives &&
			assign1.num_false_negatives == assign2.num_false_negatives;

		//for now a standard metric based on misclassifications
		return assign1.Misclassifications() == assign2.Misclassifications();
	}

	std::ostream& operator<<(std::ostream& out, const AssignmentContainer& rhs)
	{
		auto kek = rhs.GetStoredAssignments();
		std::sort(kek.begin(), kek.end(), [](auto& p1, auto& p2) { return p1.num_false_positives < p2.num_false_positives; });
		
		for (const DecisionNodeAssignment& assignnment : kek)
		{
			out << "(" << assignnment.num_false_positives << ", " << assignnment.num_false_negatives << ") ";
		}
		return out;
	}
	bool AssignmentContainer::DoInputAssignmentsContainDominatingSolution(const AssignmentContainer& container) const
	{
		for (auto &assignment : container.GetStoredAssignments())
		{
			if (DoesInputAssignmentDominateAtLeastOneAssignment(assignment)) { return true; }
		}
		return false;
	}

	bool AssignmentContainer::DoInputAssignmentsContainStrictlyDominatingSolution(const AssignmentContainer& container) const
	{
		for (auto &assignment : container.GetStoredAssignments())
		{
			if (DoesInputAssignmentStrictlyDominateAtLeastOneAssignment(assignment)) { return true; }
		}
		return false;
	}

	bool AssignmentContainer::ContainsNonDominatedSolution(const AssignmentContainer& input) const
	{
		for (auto& assignment : GetStoredAssignments())
		{
			if (input.IsInputAssignmentDominated(assignment) == false
				|| input.IsEquivalentAssignmentPresent(assignment))
			{ return true; }
		}
		return false;
	}

	bool AssignmentContainer::IsInputAssignmentDominated(const DecisionNodeAssignment& input_assignment, bool maximise) const
	{
		for (const DecisionNodeAssignment &stored_assignment : GetStoredAssignments())
		{
			if (maximise)
			{
				if (IsAssignmentReverseDominated(input_assignment, stored_assignment))
				{
					return true;
				}
			}
			else
			{
				if (IsAssignmentDominated(input_assignment, stored_assignment))
				{
					return true;
				}
			}			
		}
		return false;
	}

	bool AssignmentContainer::IsInputAssignmentStrictlyDominatedMax(const DecisionNodeAssignment& input_assignment) const
	{
		for (const DecisionNodeAssignment& stored_assignment : GetStoredAssignments())
		{
			if (stored_assignment.num_false_positives > input_assignment.num_false_positives &&
				stored_assignment.num_false_negatives > input_assignment.num_false_negatives)
			{
				return true;
			}
		}
		return false;
	}

	bool AssignmentContainer::DoesInputAssignmentDominateAtLeastOneAssignment(const DecisionNodeAssignment& input_assignment) const
	{
		for (auto &stored_assignment : GetStoredAssignments())
		{
			if (IsAssignmentDominated(stored_assignment, input_assignment)) { return true; }
		}
		return false;
	}

	bool AssignmentContainer::DoesInputAssignmentStrictlyDominateAtLeastOneAssignment(const DecisionNodeAssignment& input_assignment) const
	{
		for (auto &stored_assignment : GetStoredAssignments())
		{
			if (IsAssignmentDominated(stored_assignment, input_assignment) && !IsAssignmentDominated(input_assignment, stored_assignment)) { return true; }
		}
		return false;
	}

	bool AssignmentContainer::IsEquivalentAssignmentPresent(const DecisionNodeAssignment& assignment) const
	{
		for (const DecisionNodeAssignment stored_assignment : GetStoredAssignments())
		{
			if (AreAssignmentEquivalent(stored_assignment, assignment))
			{
				return true;
			}
		}
		return false;
	}

	bool AssignmentContainer::AreAllAssignmentsPresent(const AssignmentContainer& container) const
	{
		for (const DecisionNodeAssignment& assignment : container.GetStoredAssignments())
		{
			if (IsEquivalentAssignmentPresent(assignment) == false) { return false; }
		}
		return true;
	}
	bool AssignmentContainer::AreAssignmentsReverseStrictlyDominatedByInput(const AssignmentContainer& container) const
	{
		for (const DecisionNodeAssignment& assignment : GetStoredAssignments())
		{
			if (container.IsInputAssignmentStrictlyDominatedMax(assignment) == false) { return false; }
		}
		return true;

		/*
		return assignment1.num_false_positives <= assignment2.num_false_positives &&
			assignment1.num_false_negatives <= assignment2.num_false_negatives;
		*/




		/*for (const DecisionNodeAssignment& assignment : container.GetStoredAssignments())
		{
			if (container.IsInputAssignmentDominated(assignment) == false) { return false; }
		}*/
		for (const DecisionNodeAssignment& assignment : GetStoredAssignments())
		{
			if (container.IsInputAssignmentDominated(assignment, true) == false) { return false; }
		}
		return true;
	}

	AssignmentContainer AssignmentContainer::RemoveSolutionsDominatedByInput(const AssignmentContainer& container)
	{
		AssignmentContainer removed_assignments;
		ParetoFront old_assignments = stored_assignments_;
		stored_assignments_.clear();
		for (auto assignment : old_assignments.front)
		{
			if (container.IsInputAssignmentDominated(assignment) == false)
			{
				stored_assignments_.insert(assignment);//.push_back(assignment);
			}
			else
			{
				removed_assignments.AddAssignment(assignment);
			}
		}
		return removed_assignments;
	}
}