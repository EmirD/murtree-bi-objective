#pragma once

#include "state.h"
#include "node_selector_abstract.h"
#include "parameters.h"
#include "statistics.h"
#include "decision_tree.h"
#include "decision_tree_learning.h"

#include <stdint.h>
#include <vector>
#include <string>
#include <time.h>

namespace DecisionTreeSolverMur
{

	struct ParetoTree
	{
		DecisionTree* decision_tree_;
		int num_false_positives;
		int num_false_negatives;
	};

	struct Result
	{
		Result() :decision_tree_(0){};

		uint32_t misclassifications;
		bool is_proven_optimal;
		DecisionTree *decision_tree_; //todo a bit hacky since I do not have deep copies for nodes; the data is also shallow
	
		AssignmentContainer best_assignments;
		std::vector<ParetoTree> pareto_trees;
		bool IsFeasible() const { return misclassifications != UINT32_MAX; }
	};

	struct ResultHyperParameterTuning
	{
		ResultHyperParameterTuning() 
		{
			best_f1_tree = 0;
			best_accuracy_tree = 0;
			best_weighted_accuracy_tree = 0;
		};

		double total_time;
		//Result best_result;
		DecisionTree* best_f1_tree, * best_accuracy_tree, * best_weighted_accuracy_tree;
		std::vector<std::vector<TrainTestResult> > stats;

		std::vector<std::vector<TrainTestResult> > stats_accuracy, stats_weighted_accuracy, stats_f1;
	};

	struct SolverParameters
	{
		SolverParameters() :
			max_depth(4),
			max_size(15),
			time_limit(UINT32_MAX),
			upper_bound_constraint(UINT32_MAX),
			verbose(true)
		{}

		uint32_t max_depth;
		uint32_t max_size;
		uint32_t time_limit;
		uint32_t upper_bound_constraint;
		bool verbose;
	};

	class Solver
	{
	public:
		Solver(LearningData &data, Parameters &params);
		~Solver();

		Result Solve(SolverParameters &parameters);
		Result SolveCARTobjective(SolverParameters &parameters, double alpha);

		static void GetBestTrees(LearningData& data, std::vector<ParetoTree>& pts, DecisionTree** best_accuracy_tree, int& split_misclas, DecisionTree** best_weighted_accuracy_tree, double& split_weighted_accuracy, DecisionTree** best_f1_tree, double& split_f1_score);

		static double ComputeWeightedAccuracy(LearningData &data, int false_positives, int false_negatives);
		static void SetWeights(LearningData& data, double& positive_weight, double& negative_weight);
		static double ComputeF1score(LearningData& data, int false_positives, int false_negatives);
		static ResultHyperParameterTuning SolveHyperParameterTuning(LearningData &data, std::vector<TrainTestSplit> &splits, Parameters &global_parameters, SolverParameters &solver_parameters);

		Statistics stats_;

	//private:

		void Initialise(SolverParameters &parameters);

		void ProcessNode(DecisionNode * node);
		void ProcessNonTerminalNode(DecisionNode * node);

		void PreprocessRootData();

		void UpdateBestAssignment(bool verbose);

		bool IsWithinTimeBudget(uint32_t time_limit);
		time_t GetRuntimeInSeconds();

		Result ConstructOutput(bool verbose);

		State state_;

		NodeSelectorAbstract *node_selector_;
		
		AssignmentContainer best_assignments_;
		clock_t clock_start_;
		Statistics statistics_;

		const std::string node_selection_strategy_;
	};

}