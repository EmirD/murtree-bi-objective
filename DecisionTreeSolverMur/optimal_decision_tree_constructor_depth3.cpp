#include "optimal_decision_tree_constructor_depth3.h"

namespace DecisionTreeSolverMur
{
	OptimalDecisionTreeConstructorDepth3::OptimalDecisionTreeConstructorDepth3(size_t num_features) :
		num_features_(num_features),
		optimal_penalty_(UINT32_MAX),
		penalty_computer_(num_features),
		subtree_info_(num_features, SubtreeInformation(num_features))
	{
	}

	void OptimalDecisionTreeConstructorDepth3::ComputeOptimalDecisionTree(LearningData & data)
	{
		InitialiseDataStructures(data);

		for (size_t f1 = 0; f1 < num_features_; f1++)
		{
			for (size_t f2 = f1 + 1; f2 < num_features_; f2++)
			{
				for (size_t f3 = f2 + 1; f3 < num_features_; f3++)
				{
					UpdateTrees(f1, f2, f3);
					UpdateTrees(f1, f3, f2);

					UpdateTrees(f2, f1, f3);
					UpdateTrees(f2, f3, f1);

					UpdateTrees(f3, f1, f2);
					UpdateTrees(f3, f2, f1);
				}
			}
		}
		UpdateBestPenalty();
	}

	uint32_t OptimalDecisionTreeConstructorDepth3::GetOptimalPenalty() const
	{
		return optimal_penalty_;
	}

	void OptimalDecisionTreeConstructorDepth3::InitialiseDataStructures(LearningData & data)
	{
		optimal_penalty_ = UINT32_MAX;
		InitialiseSubtreeInfo();
		penalty_computer_.Initialise(data);
	}

	void OptimalDecisionTreeConstructorDepth3::InitialiseSubtreeInfo()
	{
		for (uint32_t f1 = 0; f1 < num_features_; f1++)
		{
			for (uint32_t f2 = 0; f2 < num_features_; f2++)
			{
				subtree_info_[f1].left_subtree[f2].left_child_penalty = UINT32_MAX;
				subtree_info_[f1].left_subtree[f2].right_child_penalty = UINT32_MAX;
				subtree_info_[f1].right_subtree[f2].left_child_penalty = UINT32_MAX;
				subtree_info_[f1].right_subtree[f2].right_child_penalty = UINT32_MAX;
			}
		}
	}

	void OptimalDecisionTreeConstructorDepth3::UpdateTrees(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		uint32_t penalty_branch_one_one_one = penalty_computer_.PenaltyBranchOneOneOne(f1, f2, f3);
		uint32_t penalty_branch_one_one_zero = penalty_computer_.PenaltyBranchOneOneZero(f1, f2, f3);
		uint32_t penalty_branch_one_zero_one = penalty_computer_.PenaltyBranchOneZeroOne(f1, f2, f3);
		uint32_t penalty_branch_one_zero_zero = penalty_computer_.PenaltyBranchOneZeroZero(f1, f2, f3);

		uint32_t penalty_branch_zero_one_one = penalty_computer_.PenaltyBranchZeroOneOne(f1, f2, f3);
		uint32_t penalty_branch_zero_one_zero = penalty_computer_.PenaltyBranchZeroOneZero(f1, f2, f3);
		uint32_t penalty_branch_zero_zero_one = penalty_computer_.PenaltyBranchZeroZeroOne(f1, f2, f3);
		uint32_t penalty_branch_zero_zero_zero = penalty_computer_.PenaltyBranchZeroZeroZero(f1, f2, f3);

		UpdateBestLeftLeftChild(f1, f2, f3, penalty_branch_zero_zero_zero + penalty_branch_zero_zero_one);
		UpdateBestLeftRightChild(f1, f2, f3, penalty_branch_zero_one_zero + penalty_branch_zero_one_one);

		UpdateBestRightLeftChild(f1, f2, f3, penalty_branch_one_zero_zero + penalty_branch_one_zero_one);
		UpdateBestRightRightChild(f1, f2, f3, penalty_branch_one_one_zero + penalty_branch_one_one_one);
	}

	void OptimalDecisionTreeConstructorDepth3::UpdateBestLeftLeftChild(uint32_t f1, uint32_t f2, uint32_t f3, uint32_t penalty)
	{
		if (subtree_info_[f1].left_subtree[f2].left_child_penalty > penalty)
		{
			subtree_info_[f1].left_subtree[f2].left_child_feature = f3;
			subtree_info_[f1].left_subtree[f2].left_child_penalty = penalty;
		}
	}

	void OptimalDecisionTreeConstructorDepth3::UpdateBestLeftRightChild(uint32_t f1, uint32_t f2, uint32_t f3, uint32_t penalty)
	{
		if (subtree_info_[f1].left_subtree[f2].right_child_penalty > penalty)
		{
			subtree_info_[f1].left_subtree[f2].right_child_feature = f3;
			subtree_info_[f1].left_subtree[f2].right_child_penalty = penalty;
		}
	}

	void OptimalDecisionTreeConstructorDepth3::UpdateBestRightLeftChild(uint32_t f1, uint32_t f2, uint32_t f3, uint32_t penalty)
	{
		if (subtree_info_[f1].right_subtree[f2].left_child_penalty > penalty)
		{
			subtree_info_[f1].right_subtree[f2].left_child_feature = f3;
			subtree_info_[f1].right_subtree[f2].left_child_penalty = penalty;
		}
	}

	void OptimalDecisionTreeConstructorDepth3::UpdateBestRightRightChild(uint32_t f1, uint32_t f2, uint32_t f3, uint32_t penalty)
	{
		if (subtree_info_[f1].right_subtree[f2].right_child_penalty > penalty)
		{
			subtree_info_[f1].right_subtree[f2].right_child_feature = f3;
			subtree_info_[f1].right_subtree[f2].right_child_penalty = penalty;
		}
	}

	void OptimalDecisionTreeConstructorDepth3::UpdateBestPenalty()
	{
		for (uint32_t f1 = 0; f1 < num_features_; f1++)
		{
			uint32_t local_penalty_left = UINT32_MAX;
			uint32_t local_penalty_right = UINT32_MAX;
			for (uint32_t f2 = 0; f2 < num_features_; f2++)
			{
				if (f1 == f2) { continue; }

				local_penalty_left =
					std::min(local_penalty_left,
						subtree_info_[f1].left_subtree[f2].left_child_penalty + subtree_info_[f1].left_subtree[f2].right_child_penalty
					);

				local_penalty_right =
					std::min(local_penalty_right,
						subtree_info_[f1].right_subtree[f2].left_child_penalty + subtree_info_[f1].right_subtree[f2].right_child_penalty
					);
			}
			optimal_penalty_ = std::min(optimal_penalty_, local_penalty_left + local_penalty_right);
		}
	}
}
