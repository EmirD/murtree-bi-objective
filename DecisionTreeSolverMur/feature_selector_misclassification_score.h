#include "feature_selector_abstract.h"
#include "key_value_heap.h"
#include "..\optimal_decision_tree_constructor_depth1.h"

namespace DecisionTreeSolverMur
{
	class FeatureSelectorMisclassificationScore : public FeatureSelectorAbstract
	{
	public:
		FeatureSelectorMisclassificationScore(DecisionTree * tree, DecisionNode * node, uint32_t num_features, bool ascending) :
			FeatureSelectorAbstract(tree, node, num_features),
			feature_heap_(num_features),
			ascending_(ascending)
		{}

	protected:

		uint32_t PopNextFeatureInternal();
		void ResetInternal(LearningData &data);

		void BuildHeap(LearningData &data);

		bool ascending_;
		OptimalDecisionTreeConstructorDepth1 tree_builder_;
		KeyValueHeap feature_heap_;
	};

	uint32_t FeatureSelectorMisclassificationScore::PopNextFeatureInternal()
	{
		return feature_heap_.PopMax();
	}

	void FeatureSelectorMisclassificationScore::ResetInternal(LearningData &data)
	{
		BuildHeap(data);
	}

	void FeatureSelectorMisclassificationScore::BuildHeap(LearningData &data)
	{
		while (feature_heap_.Size() != 0) { feature_heap_.PopMax(); } //todo a beter way to do this...
		tree_builder_.ComputeOptimalDecisionTree(data);
		for(uint32_t feature = 0; feature < data.NumFeatures(); feature++) //could construct the heap more efficiently but this is not the bottle neck
		{
			feature_heap_.Readd(feature);
			uint32_t score = tree_builder_.GetTreeInfoForRootFeature(feature).Misclassifications();
			if (ascending_) { assert(score <= data.Size()); score = data.Size() - score; };
			feature_heap_.Increment(feature, score);
		}
		assert(feature_heap_.Size() == num_features_);
	}
}