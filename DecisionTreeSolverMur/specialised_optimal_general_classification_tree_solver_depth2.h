#pragma once

#include "specialised_optimal_decision_tree_solver_abstract.h"
#include "..//penalty_branch_computer_depth_2.h"
#include "..//optimal_decision_tree_constructor_depth1.h"

namespace DecisionTreeSolverMur
{
	class SpecialisedOptimalGeneralClassificationTreeSolverDepth2 : public SpecialisedOptimalDecisionTreeSolverAbstract
	{
	public:
		SpecialisedOptimalGeneralClassificationTreeSolverDepth2(uint32_t num_labels, size_t num_features, uint32_t num_total_data_points = 0); //todo make it mandatory not an optional parameter?

		DecisionNodeAssignment Solve(LearningData &data, uint32_t max_node_budget);
		void SolveFull(DecisionNode *node, State *state, DecisionTree *tree);

		bool IsTerminalNode(DecisionNode *node, State *state);
		int ProbeDifference(LearningData &data);

	private:

		struct ChildrenInformation
		{
			uint32_t left_child_feature, right_child_feature;
			uint32_t left_child_penalty, right_child_penalty;
		};

		struct BestPenalties
		{
			BestPenalties() :zero_nodes(UINT32_MAX), one_node(UINT32_MAX), two_nodes(UINT32_MAX), three_nodes(UINT32_MAX) {}
			uint32_t zero_nodes;
			uint32_t one_node;
			uint32_t two_nodes;
			uint32_t three_nodes;
		};

		void InitialiseDataStructures(LearningData &data);
		void InitialiseChildrenInfo();

		void UpdateBestLeftChild(uint32_t root_feature, uint32_t left_child_feature, uint32_t penalty);
		void UpdateBestRightChild(uint32_t root_feature, uint32_t right_child_feature, uint32_t penalty);
		void UpdateBestTwoNodeAssignment(uint32_t root_feature, uint32_t left_misclass, uint32_t right_misclass);
		void UpdateBestThreeNodeAssignment(uint32_t root_feature);

		uint32_t num_features_;
		PenaltyBranchComputerDepth2 penalty_computer_;
		std::vector<ChildrenInformation> best_children_info_;

		DecisionNodeAssignment optimal_at_most_one_node, optimal_at_most_two_nodes, optimal_at_most_three_nodes;
		OptimalDecisionTreeConstructorDepth1 tree_solver_depth1_;

		//		friend OptimalDecisionTreeConstructorDepth2Tester;
	};
}