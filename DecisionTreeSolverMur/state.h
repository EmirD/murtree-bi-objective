#pragma once

#include "parameters.h"
#include "decision_tree.h"
#include "statistics.h"
#include "cache_branch_bucketing_hash.h"
#include "../learning_data.h"
#include "decision_tree.h"
#include "specialised_optimal_decision_tree_solver_abstract.h"
#include "specialised_optimal_binary_classification_tree_solver_depth2.h"
#include "lower_bound_computer.h"

namespace DecisionTreeSolverMur
{
	struct PairFeatureNodeBudgets
	{
		PairFeatureNodeBudgets(const uint32_t feature, const PairNodeBudget budgets) :feature(feature), budgets(budgets) {}
		uint32_t feature;
		PairNodeBudget budgets;
	};

	class State
	{
	public:
		State(LearningData &data, Parameters &parameters, Statistics &statistics);
		~State();

		void Initialise(uint32_t max_depth, uint32_t max_size, uint32_t upper_bound_constraint);		

		PairFeatureNodeBudgets PopNextNodeAssignment(DecisionNode *node);
		uint32_t PopNextFeatureForNode(DecisionNode * node);
		PairNodeBudget PopNextNodeBudgetsForNode(DecisionNode *node);

		bool PerformAssignmentToNode(DecisionNode * node, uint32_t feature, PairNodeBudget budgets);
		bool PrepareDataForChildren(LearningData &data, uint32_t feature, LearningData &data_without_feature, LearningData &data_with_feature); //sets the data for the children with respect to the assigned feature of the parent node. If one of the two children would result in a empty data, the method sets both children data to empty and returns false. Otherwise the data of the nodes will be set and true will be returned.
		
		void PropagateNode(DecisionNode *node);
		void PropagateUpwards(DecisionNode *node);
		void PropagateDownwards();
		bool PropagateDownwardsFromNodeUntilFirstPruning(DecisionNode *node); //performs at most one node pruning. Returns true if pruning took places, and false otherwise.

		void PruneNode(DecisionNode *node); //prunes node and propagates the pruning upwards. Note that the other child of node's parent will also get pruned.
		void PruneChildren(DecisionNode *node); //prune children might result in having 'node' pruned as well

		AssignmentContainer GetBestAssignments();
		DecisionNode * GetRootNode();
		uint32_t DepthBelowNode(DecisionNode * node);	

		bool IsTerminalNode(DecisionNode * node);
		bool IsRootNode(DecisionNode * node);
		bool IsRootNodeExhausted();

		DecisionTree * ReconstructBestSolution();
		DecisionTree* ReconstructBestParetoTree(DecisionNodeAssignment root_assignment);
		bool ReconstructBestSolution_node(DecisionNode *node_copy, DecisionTree &tree, DecisionNodeAssignment target_assignment);
		
		bool IsStateCorrect(); //used for debugging

	//private:

		void InitialiseNode(DecisionNode *node, uint32_t node_budget); //assumes the data for node has been set
		void CacheBranch(DecisionNode *node);
		bool RestoreBranchFromCache(DecisionNode *node, uint32_t node_budget);
		AssignmentContainer GetLowerBound(DecisionNode *node);

		uint32_t MaxNumNodesForDepth(uint32_t depth);

		void SolveTerminalNode(DecisionNode *node);

		AssignmentContainer GetSolutionsForNode(DecisionNode *node, DecisionTree& tree);
		SpecialisedOptimalBinaryClassificationTreeSolverDepth2 *tree_solver_left_, *tree_solver_right_;
		LowerBoundComputer similiarity_lower_bound_computer_;

		LearningData &data_;
		DecisionTree decision_tree_;
		CacheBranchBucketingHash *cache_;
		CacheBranchBucketingHash* cache_backup_;
		Statistics &statistics_;

		struct StateParameters
		{
			StateParameters(bool features_first, bool use_ub_external, bool use_pruning, uint32_t unit_tree) :
				assignments_feature_first(features_first),
				use_upper_bound_external_budget(use_ub_external),
				use_pruning(use_pruning),
				unit_tree_depth(unit_tree),
				use_upper_bounding(true),
				use_infeasibility_lower_bounding(true)
			{}

			uint32_t max_depth;
			uint32_t max_size;			
			uint32_t upper_bound_constraint;
						
			const bool assignments_feature_first;
			const bool use_upper_bound_external_budget;
			bool use_pruning;
			bool use_upper_bounding;
			bool use_infeasibility_lower_bounding;
			
			const uint32_t unit_tree_depth;
		} parameters_;
	};

}