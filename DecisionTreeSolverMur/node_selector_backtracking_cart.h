#pragma once

#include "node_selector_abstract.h"

namespace DecisionTreeSolverMur
	{	
	class NodeSelectorBacktrackingCART : public NodeSelectorAbstract
	{
	public:
		NodeSelectorBacktrackingCART(State * state, bool larger_misclassification_side_first);
		DecisionNode * GetNextNode();

	private:
		DecisionNode * NextNodeInPostOrderSearch(DecisionNode * node);
		DecisionNode * FindUnexpandedNode(DecisionNode *node);

		bool IsFirstNodeBetterThanTheSecondOptimalitySearch(DecisionNode *node1, DecisionNode *node2);

		bool larger_misclassification_side_first_;
	};

	NodeSelectorBacktrackingCART::NodeSelectorBacktrackingCART(State * state, bool larger_misclassification_side_first) :
		NodeSelectorAbstract(state),
		larger_misclassification_side_first_(larger_misclassification_side_first)
	{}

	DecisionNode * NodeSelectorBacktrackingCART::GetNextNode()
	{
		DecisionNode *next_node = FindUnexpandedNode(state_->GetRootNode());

		if (next_node != NULL) { return next_node; }

		next_node = NextNodeInPostOrderSearch(state_->GetRootNode());
		assert(next_node != NULL);
		return next_node;
	}

	DecisionNode * NodeSelectorBacktrackingCART::NextNodeInPostOrderSearch(DecisionNode * node)
	{
		assert(node != NULL && node->IsActivated());

		if (node->IsExhausted()) { return NULL; }

		if (state_->IsTerminalNode(node) || state_->decision_tree_.HasChildren(node) == false) { return node; }

		DecisionNode * left_child = state_->decision_tree_.GetLeftChild(node);
		DecisionNode * right_child = state_->decision_tree_.GetRightChild(node);


		if (IsFirstNodeBetterThanTheSecondOptimalitySearch(left_child, right_child) == larger_misclassification_side_first_)
		{
			DecisionNode * left_result = NextNodeInPostOrderSearch(left_child);
			if (left_result != NULL) { return left_result; }

			DecisionNode * right_result = NextNodeInPostOrderSearch(right_child);
			if (right_result != NULL) { return right_result; }
		}
		else
		{
			DecisionNode * right_result = NextNodeInPostOrderSearch(right_child);
			if (right_result != NULL) { return right_result; }

			DecisionNode * left_result = NextNodeInPostOrderSearch(left_child);
			if (left_result != NULL) { return left_result; }
		}

		return node;
	}

	DecisionNode * NodeSelectorBacktrackingCART::FindUnexpandedNode(DecisionNode * node)
	{
		if (state_->IsTerminalNode(node)) { return NULL; }

		if (node->IsExhausted()) { return NULL; }

		if (state_->decision_tree_.HasChildren(node) == false) { return node; }

		DecisionNode *left_child = state_->decision_tree_.GetLeftChild(node);
		DecisionNode *right_child = state_->decision_tree_.GetRightChild(node);

		if (left_child->IsExhausted() && right_child->IsExhausted()) { return node; }

		DecisionNode *left_result = FindUnexpandedNode(left_child);
		if (left_result != NULL) { return left_result; }
		
		return FindUnexpandedNode(right_child);
	}

	bool NodeSelectorBacktrackingCART::IsFirstNodeBetterThanTheSecondOptimalitySearch(DecisionNode *node1, DecisionNode *node2)
	{
		runtime_assert(1 == 2);
		return false;
		/*
		uint32_t lb1 = state_->GetLowerBound(node1);
		uint32_t lb2 = state_->GetLowerBound(node2);

		//maybe try the modified budget instead but it might result in switching branches...
		if (node1->GetExternalMisclassificationBudget() - lb1 > node2->GetExternalMisclassificationBudget() - lb2) { return true; }
		return node1->GetData()->ComputeClassificationCost() > node2->GetData()->ComputeClassificationCost();

		return node1->GetData()->ComputeClassificationCost() - lb1 > node2->GetData()->ComputeClassificationCost() - lb2;*/
	}
}