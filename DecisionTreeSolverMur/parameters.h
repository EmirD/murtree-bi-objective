#pragma once

#include <string>
#include <iostream>
#include <assert.h>

namespace DecisionTreeSolverMur
{

	struct Parameters
	{
		Parameters()
		{
			max_depth = 4;
			max_size = UINT32_MAX;
			unit_tree_depth = 2;
			upper_bound_constraint = UINT32_MAX;

			node_selection_strategy = "best_first_variant1";
			feature_ordering_strategy = "in_order";
			node_budget_strategy = "simple";
			cache_technique = "bucketing_hash";
			branch_length_hash_bucketing = 50;

			time_limit = UINT32_MAX;

			preprocess_data = true;
			verbose = true;
			
			use_upper_bounding = true;
			use_upper_bound_caching = false;
			use_lower_bound_caching = false;
			use_pruning = true;			
			assignments_feature_first = true;
			use_upper_bound_external_budget = false;
			use_infeasibility_lower_bounding = true;

			use_similiarity_lower_bound = true;
			use_incremental_frequency_reconstruction = true;

			use_CART_objective = false;
			CART_alpha = 0.1;

			hyper_tuning_mode = false;
		}

		void CheckCorrectness()
		{
			if (cache_technique == "bucketing_hash" && branch_length_hash_bucketing == 0)
			{
				std::cout << "parameter error\n";
				exit(1);
			}
		}

		uint32_t max_depth;
		uint32_t max_size;
		uint32_t unit_tree_depth;
		uint32_t upper_bound_constraint;

		std::string node_selection_strategy;
		std::string feature_ordering_strategy;
		std::string node_budget_strategy;
		std::string cache_technique;
		uint32_t branch_length_hash_bucketing;

		std::string dump_file;

		uint32_t time_limit;

		bool preprocess_data;
		bool verbose;
		bool use_upper_bounding;
		bool use_infeasibility_lower_bounding;

		bool use_upper_bound_caching;
		bool use_lower_bound_caching;
		bool use_pruning;
		bool assignments_feature_first;
		bool use_upper_bound_external_budget;

		bool use_similiarity_lower_bound;
		bool use_incremental_frequency_reconstruction;

		bool use_CART_objective;
		double CART_alpha;

		bool hyper_tuning_mode;
	};

}