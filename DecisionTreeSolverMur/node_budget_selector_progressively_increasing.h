#pragma once

#include "node_budget_selector_abstract.h"

#include <vector>

namespace DecisionTreeSolverMur
{
	class NodeBudgetSelectorProgressivelyIncreasing : public NodeBudgetSelectorAbstract
	{
	public:
		NodeBudgetSelectorProgressivelyIncreasing(DecisionTree * tree, DecisionNode * node);

		PairNodeBudget PopNextBudgetSplit();
		void Reset(uint32_t new_node_budget);
		bool AreThereAnyOptionsLeft() const;

	private:
		void InitialiseOptions();

		std::vector<PairNodeBudget> possible_budget_splits_;
	};
}