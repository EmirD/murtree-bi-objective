#include "pareto_front.h"

namespace DecisionTreeSolverMur
{
    std::ostream& operator<<(std::ostream& out, ParetoFront& pf)
    {
        for (int i = 0; i < pf.length(); i++)
            out << "(" << pf[i].num_false_positives << ", " << pf[i].num_false_negatives << ") ";
        return out;
    }
}