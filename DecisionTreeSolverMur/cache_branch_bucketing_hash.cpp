#include "cache_branch_bucketing_hash.h"
#include "runtime_assert.h"

namespace DecisionTreeSolverMur
{
	CacheBranchBucketingHash::CacheBranchBucketingHash(uint32_t max_branch_length) :
		cache_(max_branch_length + 1),
		use_lower_bound_caching_(true),
		use_upper_bound_caching_(true)
	{}

	void CacheBranchBucketingHash::StoreOptimalBranchAssignments(const BranchInfo & branch, const AssignmentContainer &optimal_assignments, uint32_t total_node_budget)
	{
		runtime_assert(optimal_assignments.MinNumNodes() <= total_node_budget);
		runtime_assert(total_node_budget > 0); //note that even though we never store trivial cases, they still might appear in the cache when using more than zero nodes does not lead to a better solution

		if (!use_upper_bound_caching_ || branch.Size() >= cache_.size()) { return; } //ignoring branches that are too large
		
		auto &hashmap = cache_[branch.Size()];
		auto iter_vector_entry = hashmap.find(branch);
		if (iter_vector_entry == hashmap.end()) //if the branch has never been seen before, create a new entry for it
		{
			//todo: not sure we can do the tricks we did for single-objective optimisation
			//even if one of the solutions has less than the prescribed limited of nodes
			//doesn't mean that the Pareto front for less nodes only has that solution! Other trade-offs are possible

			std::vector<CacheEntry> vector_entry;
			//for (uint32_t budget = optimal_assignments.MinNumNodes(); budget <= total_node_budget; budget++)
			for (uint32_t budget = total_node_budget; budget <= total_node_budget; budget++)
			{				
				CacheEntry entry(budget);
				entry.SetOptimalAssignments(optimal_assignments);
				vector_entry.push_back(entry);
			}
			cache_[branch.Size()].insert(std::pair<BranchInfo, std::vector<CacheEntry> >(branch, vector_entry));
		}
		else
		{
			//now we need to see if other node budget have been seen before. If it was seen, update it;
			std::vector<bool> node_budget_seen(total_node_budget+1, false);
			for (CacheEntry &entry : iter_vector_entry->second)
			{
				//assert(optimal_assignments.Misclassifications() >= entry.GetLowerBound() 
				
				//optimal assignments cannot contain a solution that dominates anything from the lower bound
				//TODO FIX this assert
				//assert(entry.GetLowerBound().DoInputAssignmentsContainStrictlyDominatingSolution(optimal_assignments) == false
					//|| optimal_assignments.MinNumNodes() < entry.GetNodeBudget());
				
				//if (optimal_assignments.MinNumNodes() <= entry.GetNodeBudget()
				//	&& entry.GetNodeBudget() <= total_node_budget)
				if (total_node_budget == entry.GetNodeBudget())
				{
					assert(!entry.IsOptimal() 
						|| 
						(entry.GetOptimalSolutions().DoInputAssignmentsContainStrictlyDominatingSolution(optimal_assignments) == false
							&& optimal_assignments.DoInputAssignmentsContainStrictlyDominatingSolution(entry.GetOptimalSolutions()) == false)
					);
					
					node_budget_seen[entry.GetNodeBudget()] = true;
					if (entry.IsOptimal() == false)
					{
						entry.SetOptimalAssignments(optimal_assignments);
					}					
				}
			}
			//create entries for those which were not seen
			//for (uint32_t budget = optimal_assignments.MinNumNodes(); budget <= total_node_budget; budget++)
			for (uint32_t budget = total_node_budget; budget <= total_node_budget; budget++)
			{
				if (node_budget_seen[budget] == false)
				{
					CacheEntry entry(budget);
					entry.SetOptimalAssignments(optimal_assignments);
					iter_vector_entry->second.push_back(entry);
				}
			}		
		}
		//runtime_assert(RetrieveOptimalAssignment(branch, total_node_budget).DoInputAssignmentsContainDominatingSolution(optimal_assignments)
		//		&& optimal_assignments.DoInputAssignmentsContainDominatingSolution(RetrieveOptimalAssignment(branch, total_node_budget)));
	}

	bool CacheBranchBucketingHash::IsOptimalAssignmentCached(const BranchInfo & branch, uint32_t node_budget)
	{
		if (!use_upper_bound_caching_ || branch.Size() >= cache_.size()) { return false; } //ignoring branches that are too large
	
		auto &hashmap = cache_[branch.Size()];
		auto iter = hashmap.find(branch);
		
		if (iter == hashmap.end()) { return false; }

		for (CacheEntry &entry : iter->second)
		{
			if (entry.GetNodeBudget() == node_budget)
			{
				return entry.IsOptimal(); //stops and returns
			}			
		}
		return false;
	}

	const AssignmentContainer& CacheBranchBucketingHash::RetrieveOptimalAssignment(const BranchInfo & branch, uint32_t node_budget)
	{
		runtime_assert(IsOptimalAssignmentCached(branch, node_budget));

		auto &hashmap = cache_[branch.Size()];
		auto iter = hashmap.find(branch);
		for (CacheEntry &entry : iter->second)
		{
			if (entry.GetNodeBudget() == node_budget)
			{
				return entry.GetOptimalSolutions(); //stops and returns
			}
		}
		runtime_assert(1 == 2); //should never reach this
	}

	void CacheBranchBucketingHash::StoreBranchLowerBound(const BranchInfo & branch, AssignmentContainer& lower_bound, uint32_t node_budget)
	{
		//assert(lower_bound > 0); todo 
		runtime_assert(lower_bound.Size() > 0);

		if (use_lower_bound_caching_ == false || branch.Size() >= cache_.size()) { return; } //ignoring branches that are too large

		auto &hashmap = cache_[branch.Size()];
		auto iter_vector_entry = hashmap.find(branch);

		if (iter_vector_entry == hashmap.end()) //if the branch has never been seen before, create a new entry for it
		{
			std::vector<CacheEntry> vector_entry(1, CacheEntry(node_budget));
			vector_entry[0].UpdateLowerBound(lower_bound);
			cache_[branch.Size()].insert(std::pair<BranchInfo, std::vector<CacheEntry> >(branch, vector_entry));
		}
		else
		{
			//now we need to see if this node budget has been seen before. If it was seen, update it; otherwise create a new entry
			bool node_budget_seen = false;
			for (CacheEntry &entry : iter_vector_entry->second)
			{
				if (entry.GetNodeBudget() == node_budget)
				{
					entry.UpdateLowerBound(lower_bound);
					node_budget_seen = true;
					break;
				}
			}

			if (node_budget_seen == false)
			{
				CacheEntry entry(node_budget);
				entry.UpdateLowerBound(lower_bound);
				iter_vector_entry->second.push_back(entry);
			}
		}
	}

	AssignmentContainer CacheBranchBucketingHash::RetrieveLowerBound(const BranchInfo & branch, uint32_t node_budget)
	{
		DecisionNodeAssignment dummy;
		dummy.num_false_positives = 0;
		dummy.num_false_negatives = 0;
		AssignmentContainer best_lower_bound;
		best_lower_bound.AddAssignment(dummy);

		if (use_lower_bound_caching_ == false) { return best_lower_bound; }

		if (branch.Size() >= cache_.size()) { return best_lower_bound; } //ignoring branches that are too large
		
		auto &hashmap = cache_[branch.Size()];
		auto iter = hashmap.find(branch);

		if (iter == hashmap.end()) { return best_lower_bound; }
		
		for (CacheEntry &entry : iter->second)
		{
			if (node_budget <= entry.GetNodeBudget())
			{
				best_lower_bound.MergeMaximisation(entry.GetLowerBound());
			}
		}
		return best_lower_bound;
	}

	uint32_t CacheBranchBucketingHash::RetrieveUpperBound(const BranchInfo & branch, uint32_t node_budget)
	{
		runtime_assert(1 == 2); //I think this method can be deleted?
		/*if (use_upper_bound_caching_ == false) { return UINT32_MAX; }

		if (branch.Size() >= cache_.size()) { return UINT32_MAX; } //ignoring branches that are too large

		auto &hashmap = cache_[branch.Size()];
		auto iter = hashmap.find(branch);

		if (iter == hashmap.end()) { return UINT32_MAX; }

		uint32_t best_upper_bound = UINT32_MAX;
		for (CacheEntry &entry : iter->second)
		{
			if (entry.IsOptimal() && node_budget >= entry.GetNodeBudget())
			{
				best_upper_bound = std::min(best_upper_bound, entry.GetOptimalValue());
			}
		}
		return best_upper_bound;*/
	}

	uint32_t CacheBranchBucketingHash::NumEntries() const
	{
		size_t count = 0;
		for (auto &c : cache_)
		{
			count += c.size();
		}
		return count;
	}

	void CacheBranchBucketingHash::ClearLargeBranches(uint32_t num_nodes)
	{
		/*
		the idea is to remove all entries that contain num_nodes nodes or more nodes
		the vectors of branches will have corresponding entries removed
		and vectors that become empty should be removed too (although maybe I could avoid removing and just keeping empty vectors)
		not sure how the unordered hashmap will react to removing elements on the fly (e.g. how does branch_entry++ work after previously at branch_entry was removed?)
		but it is known that iterators should remain valid after removing,
		so we collect iterators in 'to_remove' and remove all together later
		*/
		std::vector<std::unordered_map<BranchInfo, std::vector<CacheEntry>, BranchInfoHashFunction, BranchInfoEquality >::iterator> to_remove;
		std::vector<CacheEntry> to_keep;
		for (size_t branch_size = 0; branch_size < cache_.size(); branch_size++)
		{
			for (auto branch_entry = cache_[branch_size].begin(); branch_entry != cache_[branch_size].end(); ++branch_entry)
			{
				assert(branch_entry->first.Size() == branch_size);
				for (CacheEntry &cache_entry : branch_entry->second)
				{
					if (branch_size + cache_entry.GetNodeBudget() < num_nodes)
					{
						to_keep.push_back(cache_entry);
					}
				}

				if (to_keep.size() == 0)
				{
					to_remove.push_back(branch_entry);
				}
				else
				{
					branch_entry->second = to_keep;
					to_keep.clear();
				}
			}
			for (auto iter : to_remove) { cache_[branch_size].erase(iter); }
			to_remove.clear();
		}
	}

	void CacheBranchBucketingHash::DisableUpperBounding()
	{
		use_upper_bound_caching_ = false;
	}

	void CacheBranchBucketingHash::DisableLowerBounding()
	{
		use_lower_bound_caching_ = false;
	}
}