#include "penalty_branch_computer_binary_depth2.h"

namespace DecisionTreeSolverMur
{
	PenaltyBranchComputerBinaryDepth2::PenaltyBranchComputerBinaryDepth2(uint32_t num_features, uint32_t num_total_data_points) :
		difference_computer_(2, num_features, num_total_data_points),
		counts_(num_features)
	{
	}

	void PenaltyBranchComputerBinaryDepth2::Initialise(LearningData & data)
	{
		bool using_incremental_updates = difference_computer_.seen_last_time_.size() > 0;

		//using_incremental_updates = false;

		num_positives_ = data.NumInstancesForLabel(1);
		num_negatives_ = data.NumInstancesForLabel(0);

		double magic_number = 1;
		if (using_incremental_updates) difference_computer_.Update(data);
		
		if (using_incremental_updates && magic_number * difference_computer_.NumDifferences() < data.Size())
		{
			//remove incrementally
			UpdateCounts(difference_computer_.data_to_remove_, -1);
			UpdateCounts(difference_computer_.data_to_add_, +1);
		}		
		else
		{
			//compute from scratch
			counts_.ResetToZeros();
			UpdateCounts(data, +1);
		}
	}

	bool PenaltyBranchComputerBinaryDepth2::IsNonDiscriminatoryFeature(uint32_t f)
	{
		return ((counts_.Positives(f, f) == 0 && counts_.Negatives(f, f) == 0)
			|| (counts_.Positives(f, f) == num_positives_ && counts_.Negatives(f, f) == num_negatives_));
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::PenaltyBranchOneOne(uint32_t feature1, uint32_t feature2)
	{
		return std::min(PositivesOneOne(feature1, feature2), NegativesOneOne(feature1, feature2));
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::PenaltyBranchOneZero(uint32_t feature1, uint32_t feature2)
	{
		return std::min(PositivesOneZero(feature1, feature2), NegativesOneZero(feature1, feature2));
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::PenaltyBranchZeroOne(uint32_t feature1, uint32_t feature2)
	{
		return std::min(PositivesZeroOne(feature1, feature2), NegativesZeroOne(feature1, feature2));
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::PenaltyBranchZeroZero(uint32_t feature1, uint32_t feature2)
	{
		return std::min(PositivesZeroZero(feature1, feature2), NegativesZeroZero(feature1, feature2));
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::PositivesOneOne(size_t feature1, size_t feature2)
	{
		if (feature1 > feature2) { std::swap(feature1, feature2); }
		return counts_.Positives(feature1, feature2);
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::PositivesOneZero(size_t feature1, size_t feature2)
	{
		if (feature1 > feature2) { return PositivesZeroOne(feature2, feature1); }
		return counts_.Positives(feature1, feature1) - counts_.Positives(feature1, feature2);
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::PositivesZeroOne(size_t feature1, size_t feature2)
	{
		if (feature1 > feature2) { return PositivesOneZero(feature2, feature1); }
		return counts_.Positives(feature2, feature2) - counts_.Positives(feature1, feature2);
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::PositivesZeroZero(size_t feature1, size_t feature2)
	{
		if (feature1 > feature2) { std::swap(feature1, feature2); }
		return num_positives_ - (PositivesOneOne(feature1, feature1) + PositivesOneOne(feature2, feature2) - PositivesOneOne(feature1, feature2));
		//return num_positives_ - (PositivesOneOne(feature1, feature2) + PositivesOneZero(feature1, feature2) + PositivesZeroOne(feature1, feature2));
		//return num_positives_ - PositivesZeroZero(feature1, feature2) - PositivesOneZero(feature1, feature2) - PositivesZeroOne(feature1, feature2);
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::NegativesOneOne(size_t feature1, size_t feature2)
	{
		if (feature1 > feature2) { std::swap(feature1, feature2); }
		return counts_.Negatives(feature1, feature2);
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::NegativesOneZero(size_t feature1, size_t feature2)
	{
		if (feature1 > feature2) { return NegativesZeroOne(feature2, feature1); }
		return counts_.Negatives(feature1, feature1) - counts_.Negatives(feature1, feature2);
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::NegativesZeroOne(size_t feature1, size_t feature2)
	{
		if (feature1 > feature2) { return NegativesOneZero(feature2, feature1); }
		return counts_.Negatives(feature2, feature2) - counts_.Negatives(feature1, feature2);
	}

	uint32_t PenaltyBranchComputerBinaryDepth2::NegativesZeroZero(size_t feature1, size_t feature2)
	{
		if (feature1 > feature2) { std::swap(feature1, feature2); }
		return num_negatives_ - (counts_.Negatives(feature1, feature1) + counts_.Negatives(feature2, feature2) - counts_.Negatives(feature1, feature2));
		//return num_negatives_ - (NegativesOneOne(feature1, feature2) + NegativesOneZero(feature1, feature2) + NegativesZeroOne(feature1, feature2));
	}

	void PenaltyBranchComputerBinaryDepth2::UpdateCounts(LearningData & data, int value)
	{
		for (FeatureVectorBinary positive_data_point : data[1])
		{
			size_t num_present_features = positive_data_point.NumPresentFeatures();
			for (size_t i = 0; i < num_present_features; i++)
			{
				uint32_t feature1 = positive_data_point.GetJthPresentFeature(i);
				for (size_t j = i; j < num_present_features; j++)
				{
					uint32_t feature2 = positive_data_point.GetJthPresentFeature(j);
					assert(feature1 <= feature2);
					//assert(counts_.Positives(feature1.Index(), feature2.Index()) >= 0);
					//counts_.Positives(feature1.Index(), feature2.Index()) += value;
					counts_.CountLabel(1, feature1, feature2) += value;
				}
			}
		}

		for (FeatureVectorBinary negative_data_point : data[0])
		{
			size_t num_present_features = negative_data_point.NumPresentFeatures();
			for (size_t i = 0; i < num_present_features; i++)
			{
				uint32_t feature1 = negative_data_point.GetJthPresentFeature(i);
				for (size_t j = i; j < num_present_features; j++)
				{
					uint32_t feature2 = negative_data_point.GetJthPresentFeature(j);
					assert(feature1 <= feature2);
					//assert(counts_.Negatives(feature1.Index(), feature2.Index()) >= 0);
					//counts_.Negatives(feature1.Index(), feature2.Index()) += value;
					counts_.CountLabel(0, feature1, feature2) += value;
				}
			}
		}
	}

	void PenaltyBranchComputerBinaryDepth2::UpdateCountsLabel(uint32_t label, LearningData & data, int value)
	{
		for (FeatureVectorBinary negative_data_point : data[label])
		{
			size_t num_present_features = negative_data_point.NumPresentFeatures();
			for (size_t i = 0; i < num_present_features; i++)
			{
				uint32_t feature1 = negative_data_point.GetJthPresentFeature(i);
				for (size_t j = i; j < num_present_features; j++)
				{
					uint32_t feature2 = negative_data_point.GetJthPresentFeature(j);
					assert(feature1 <= feature2);
					assert(counts_.Negatives(feature1, feature2) > 0);
					//counts_.Negatives(feature1.Index(), feature2.Index()) += value;
					counts_.CountLabel(label, feature1, feature2) += value;
				}
			}
		}
	}
	
}


