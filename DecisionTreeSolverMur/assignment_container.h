#pragma once

#include "decision_node_assignment.h"
#include "pareto_front.h"

#include <vector>
#include <fstream>

//todo: the performance could be improved by keeping the assignments sorted. Check to see if it matters.

namespace DecisionTreeSolverMur
{
	class AssignmentContainer
	{
	public:
		AssignmentContainer();
		void AddAssignment(const DecisionNodeAssignment& assignment, bool maximise = false); //if the input solution is not dominated by one of the stored solutions, adds it to the contained. Note that this may remove some of the stored solutions if they are dominated by the input solution
		void AddAssignments(const AssignmentContainer& assignments, bool maximise = false); //if the input solution is not dominated by one of the stored solutions, adds it to the contained. Note that this may remove some of the stored solutions if they are dominated by the input solution

		void Clear();

		void CombineUsingSubtraction(const AssignmentContainer& container, uint32_t feature, uint32_t node_budget_left, uint32_t node_budget_right);
		void CombineUsingAddition(AssignmentContainer& container, uint32_t feature, uint32_t node_budget_left, uint32_t node_budget_right);//combines the solutions and filters according to min. This does a crossproduct addition 
		void MergeMaximisation(const AssignmentContainer& container); //combines the solutions of the two containers, and then filters out solutions that are dominated according to a maximisation criteria
		void MergeMinimisation(const AssignmentContainer& container);
		bool DoInputAssignmentsContainDominatingSolution(const AssignmentContainer& container) const;
		bool DoInputAssignmentsContainStrictlyDominatingSolution(const AssignmentContainer& container) const;
		bool ContainsNonDominatedSolution(const AssignmentContainer& input) const; //checks if the current instance has at least one solution that is not dominated by the input
		bool IsInputAssignmentDominated(const DecisionNodeAssignment& input_assignment, bool maximise = false) const;
		bool IsInputAssignmentStrictlyDominatedMax(const DecisionNodeAssignment& input_assignment) const;
		bool DoesInputAssignmentDominateAtLeastOneAssignment(const DecisionNodeAssignment& input_assignment) const;
		bool DoesInputAssignmentStrictlyDominateAtLeastOneAssignment(const DecisionNodeAssignment& input_assignment) const;
		bool IsEquivalentAssignmentPresent(const DecisionNodeAssignment& assignment) const;
		bool AreAllAssignmentsPresent(const AssignmentContainer& container) const;
		bool AreAssignmentsReverseStrictlyDominatedByInput(const AssignmentContainer& container) const; //this checks whether these assignments are dominated by the input
		AssignmentContainer RemoveSolutionsDominatedByInput(const AssignmentContainer& container); //returns the assignments that were removed

		uint32_t Size() const;
		bool IsEmpty() const;
		uint32_t MinNumNodes() const;
		const std::vector<DecisionNodeAssignment>& GetStoredAssignments() const;

		bool operator!=(const AssignmentContainer& rhs) const;
		bool operator==(const AssignmentContainer& rhs) const;
		friend std::ostream& operator<<(std::ostream& out, const AssignmentContainer& rhs);

	//private:
		bool IsAssignmentDominated(const DecisionNodeAssignment& assign1, const DecisionNodeAssignment& assign2) const; //returns true if sol1 is dominated by sol2
		bool IsAssignmentReverseDominated(const DecisionNodeAssignment& assign1, const DecisionNodeAssignment& assign2) const;
		bool AreAssignmentEquivalent(const DecisionNodeAssignment& assign1, const DecisionNodeAssignment& assign2) const;
		
		//std::vector<DecisionNodeAssignment> stored_assignments_;
		ParetoFront stored_assignments_;
	};

}