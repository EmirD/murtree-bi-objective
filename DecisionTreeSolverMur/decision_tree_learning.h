#pragma once

#include "..//learning_data.h"
#include "decision_tree.h"

#include <vector>
#include <iostream>

namespace DecisionTreeSolverMur
{
	struct TrainTestResult
	{
		TrainTestResult() :training_accuracy(0), testing_accuracy(0), tree_size(0), tree_depth(0), training_time(0), f1_train(0), f1_test(0) {}
		double training_accuracy, testing_accuracy, tree_size, tree_depth, training_time, f1_train, f1_test;

		void Print()
		{
			std::cout << "Training accuracy: " << training_accuracy << "\n";
			std::cout << "Testing accuracy: " << testing_accuracy << "\n";
			std::cout << "Tree size: " << tree_size << "\n";
			std::cout << "Tree depth: " << tree_depth << "\n";
			std::cout << "Training time: " << training_time << "\n";
		}
	};
	struct TrainTestSplit 
	{
		TrainTestSplit(uint32_t num_labels, uint32_t num_features):
			training_set(num_labels, num_features), 
			testing_set(num_labels, num_features) {}
		LearningData training_set, testing_set; 
	};

	class DecisionTreeLearning
	{
	public:
		
		static TrainTestResult KFoldCrossValidation(LearningData &data, uint32_t max_depth, uint32_t max_size, uint32_t k);
		
		static DecisionTree * LearnDecisionTree(LearningData &data, uint32_t max_depth, uint32_t max_size);
		static DecisionTree * LearnDecisionTree_noValidation(LearningData &data, uint32_t max_depth, uint32_t max_size);
		static TrainTestResult ComputeLearningAccuracy(TrainTestSplit &split, uint32_t max_depth, uint32_t max_size);
		
	private:
		static std::vector<TrainTestSplit> GetKSplits(LearningData &data, uint32_t k);
		static TrainTestResult ComputeAverageResult(std::vector<TrainTestResult> &results);
	};
}