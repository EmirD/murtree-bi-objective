#pragma once

#include "node_selector_abstract.h"

namespace DecisionTreeSolverMur
{
	class NodeSelectorPostOrder : public NodeSelectorAbstract
	{
	public:
		NodeSelectorPostOrder(State * state);
		DecisionNode * GetNextNode();

	private:
		DecisionNode * NextNodeInPostOrderSearch(DecisionNode * node);
	};

	NodeSelectorPostOrder::NodeSelectorPostOrder(State * state) :
		NodeSelectorAbstract(state)
	{}

	DecisionNode * NodeSelectorPostOrder::GetNextNode()
	{
		DecisionNode *next_node = NextNodeInPostOrderSearch(state_->GetRootNode());
		assert(next_node != NULL);
		return next_node;
	}

	DecisionNode * NodeSelectorPostOrder::NextNodeInPostOrderSearch(DecisionNode * node)
	{
		assert(node != NULL && node->IsActivated());

		if (node->IsExhausted()) { return NULL; }

		if (state_->IsTerminalNode(node) || state_->decision_tree_.HasChildren(node) == false) { return node; }

		DecisionNode * left_child = state_->decision_tree_.GetLeftChild(node);
		DecisionNode * left_result = NextNodeInPostOrderSearch(left_child);
		if (left_result != NULL) { return left_result; }

		DecisionNode * right_child = state_->decision_tree_.GetRightChild(node);
		DecisionNode * right_result = NextNodeInPostOrderSearch(right_child);
		if (right_result != NULL) { return right_result; }

		return node;
	}
}