#pragma once

#include "node_budget_selector_abstract.h"

#include <vector>

namespace DecisionTreeSolverMur
{
	class NodeBudgetSelectorSimple : public NodeBudgetSelectorAbstract
	{
	public:
		NodeBudgetSelectorSimple(DecisionTree * tree, DecisionNode * node);
		~NodeBudgetSelectorSimple();

		PairNodeBudget PopNextBudgetSplit();
		void Reset(uint32_t new_node_budget);
		bool AreThereAnyOptionsLeft() const;

	private:
		void InitialiseOptions();

		std::vector<PairNodeBudget> possible_budget_splits_;
	};
}