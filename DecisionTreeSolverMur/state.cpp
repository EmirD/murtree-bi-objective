#include "state.h"
#include "cache_branch_bucketing_hash.h"
#include "specialised_optimal_binary_classification_tree_solver_depth2.h"
#include "specialised_optimal_general_classification_tree_solver_depth2.h"

namespace DecisionTreeSolverMur
{
	State::State(LearningData &data, Parameters & p, Statistics & statistics):
		data_(data),
		decision_tree_(data.NumLabels(), data.NumFeatures(), p.max_depth, p.feature_ordering_strategy, p.node_budget_strategy),
		parameters_(p.assignments_feature_first, p.use_upper_bound_external_budget, p.use_pruning, p.unit_tree_depth),
		statistics_(statistics),
		cache_(0),
		cache_backup_(0),
		similiarity_lower_bound_computer_(20, 100, data.MaxFeatureVectorID()) //HAX todo
	{
		assert(p.max_depth <= 20);//because of the lower bound computer
		parameters_.use_upper_bounding = p.upper_bound_constraint;
		parameters_.use_infeasibility_lower_bounding = p.use_infeasibility_lower_bounding;

		//important to first initialise cache before other things
		if (p.cache_technique == "bucketing_hash")
		{
			cache_ = new CacheBranchBucketingHash(p.branch_length_hash_bucketing);
			cache_backup_ = new CacheBranchBucketingHash(p.branch_length_hash_bucketing);
		}
		else
		{
			std::cout << "Unknown cache technique: " << p.cache_technique << "\n";
			exit(1);
		}
		if (p.use_lower_bound_caching == false) { cache_->DisableLowerBounding(); };
		if (p.use_upper_bound_caching == false) { cache_->DisableUpperBounding(); };
		if (p.use_similiarity_lower_bound == false) { similiarity_lower_bound_computer_.Disable(); }

		int num_points_for_incremental_frequencies = 0; //a value of zero means incrementality is not used
		if (p.use_incremental_frequency_reconstruction) { num_points_for_incremental_frequencies = data.MaxFeatureVectorID(); }

		if (data.NumLabels() == 2)
		{
			tree_solver_left_ = new SpecialisedOptimalBinaryClassificationTreeSolverDepth2(data.NumFeatures(), num_points_for_incremental_frequencies);
			tree_solver_right_ = new SpecialisedOptimalBinaryClassificationTreeSolverDepth2(data.NumFeatures(), num_points_for_incremental_frequencies);
		}
		else
		{
			runtime_assert(1 == 2);
			//tree_solver_left_ = new SpecialisedOptimalGeneralClassificationTreeSolverDepth2(data.NumLabels(), data.NumFeatures(), num_points_for_incremental_frequencies);
			//tree_solver_right_ = new SpecialisedOptimalGeneralClassificationTreeSolverDepth2(data.NumLabels(), data.NumFeatures(), num_points_for_incremental_frequencies);
		}
		parameters_.max_depth = p.max_depth;
		Initialise(p.max_depth, p.max_size, p.upper_bound_constraint);
	}

	State::~State()
	{
		delete cache_;
		delete cache_backup_;
	}

	void State::Initialise(uint32_t max_depth, uint32_t max_size, uint32_t upper_bound_constraint)
	{
		if (max_depth != parameters_.max_depth)
		{
			cache_->ClearLargeBranches(max_depth);
			cache_backup_->ClearLargeBranches(max_depth);
		}

		parameters_.max_depth = max_depth;
		parameters_.max_size = max_size;
		parameters_.upper_bound_constraint = upper_bound_constraint;

		similiarity_lower_bound_computer_.Initialise(max_depth, max_size);
		decision_tree_.Initialise(max_depth);
		DecisionNode *root = decision_tree_.GetRootNode();
		(*root->GetData()) = data_;
		InitialiseNode(root, max_size);
		//uint32_t external_budget = std::min(data_.ComputeClassificationCost(), parameters_.upper_bound_constraint);
		AssignmentContainer temp; //todo take into account upper bound constraint?
		DecisionNodeAssignment temp2(UINT32_MAX, 0, 0, UINT32_MAX, UINT32_MAX);
		temp.AddAssignment(temp2); //todo fix
		root->UpdateExternalBudget(temp);
	}

	PairFeatureNodeBudgets State::PopNextNodeAssignment(DecisionNode * node)
	{
		//we can either assign a feature, and then investigate all size assignments of the left and right child
		//or we can assign a size assignment for the left and right child, and then investigate all possible feature assignments

		if (parameters_.assignments_feature_first)
		{
			if (!node->IsAssignedFeatureNull() && node->AreThereAnyNodeBudgetOptionsLeft())
			{
				return PairFeatureNodeBudgets(node->GetAssignedFeature(), PopNextNodeBudgetsForNode(node));
			}
			else
			{
				assert(node->AreThereAnyFeaturesLeft()); //otherwise the node would be remove during upwards propagation
				if (!node->AreThereAnyNodeBudgetOptionsLeft()) { node->GetNodeBudgetSelector()->Reset(node->GetNodeBudget()); }
				return PairFeatureNodeBudgets(PopNextFeatureForNode(node), PopNextNodeBudgetsForNode(node));
			}
		}
		else
		{
			if (node->AreThereAnyFeaturesLeft() && node->hax_flag)//this hax is used as an indicator that the node has never been assigned; hax last budget keeps the last assignment. In principle, these hax should be kept by the respective classes (node budget and feature assignment classes) TODO
			{
				return PairFeatureNodeBudgets(PopNextFeatureForNode(node), node->hax_last_node_budget);
			}
			else
			{
				assert(node->AreThereAnyNodeBudgetOptionsLeft()); //otherwise the node would be remove during upwards propagation
				if (!node->AreThereAnyFeaturesLeft()) { node->GetFeatureSelector()->Reset(*node->GetData()); }

				auto wat = PopNextNodeBudgetsForNode(node);

				node->hax_flag = true;
				node->hax_last_node_budget = wat;

				return PairFeatureNodeBudgets(PopNextFeatureForNode(node), wat);
			}
		}
	}

	uint32_t State::PopNextFeatureForNode(DecisionNode * node)
	{
		FeatureSelectorAbstract * feature_selector = node->GetFeatureSelector();
		assert(feature_selector->AreThereAnyFeaturesLeft());
		uint32_t next_feature = feature_selector->PopNextFeature();
		assert(next_feature != UINT32_MAX);
		
		//if (node->Index() == 0) { std::cout << "c root feature " << next_feature << "\n"; } //this was used as a progress indicator
		return next_feature;
	}
	
	PairNodeBudget State::PopNextNodeBudgetsForNode(DecisionNode * node)
	{
		NodeBudgetSelectorAbstract * node_budget_selector = node->GetNodeBudgetSelector();
		assert(node_budget_selector->AreThereAnyOptionsLeft());
		return node->GetNodeBudgetSelector()->PopNextBudgetSplit();
	}

	bool State::PerformAssignmentToNode(DecisionNode * node, uint32_t feature, PairNodeBudget budgets)
	{
		assert(IsTerminalNode(node) == false);
		
		DecisionNode * left_child_new = decision_tree_.PopFreeNode();
		DecisionNode * right_child_new = decision_tree_.PopFreeNode();
		bool split_successful = PrepareDataForChildren(*node->GetData(), feature, *left_child_new->GetData(), *right_child_new->GetData());
		
		if (split_successful)
		{			
			decision_tree_.DeactivateDescendents(node);

			node->AssignFeatureTemporary(feature); //this is needed since intialising children will look into the cache, and there needs to be a feature for every node on the path to the root. The assignment of the node is not properly set at this point, this will be done immediately below.
			//initialise new child nodes
			decision_tree_.SetChildren(node, left_child_new, right_child_new);
			InitialiseNode(left_child_new, budgets.left_node_budget);
			InitialiseNode(right_child_new, budgets.right_node_budget);

			//assign current solution to the node
			DecisionNodeAssignment assignment(
				feature, 
				left_child_new->GetCurrentAssignment().NumNodes(), 
				right_child_new->GetCurrentAssignment().NumNodes(),
				left_child_new->GetNumFalsePositives() + right_child_new->GetNumFalsePositives(),
				left_child_new->GetNumFalseNegatives() + right_child_new->GetNumFalseNegatives()
			);
			node->UpdateCurrentAssignment(assignment); //properly set the state of the node with a full assignment, not just a feature as above.

			AssignmentContainer candidate_best = left_child_new->GetBestAssignments();
			candidate_best.CombineUsingAddition(
				right_child_new->assignments_best_,
				feature,
				left_child_new->GetNodeBudget(),
				right_child_new->GetNodeBudget()
			);
			node->UpdateBestAssignments(candidate_best);	


			//see if the node should get exhausted
			if (GetLowerBound(node) == node->assignments_best_
				|| left_child_new->IsExhausted() && right_child_new->IsExhausted() && !node->AreThereAnyAssignmentsLeft() ) 
			{ 
				node->MarkExhausted(); 
			}
			/*else if (parameters_.use_upper_bound_external_budget) //seems useless
			{
				BranchInfo left_branch = decision_tree_.GetBranchInfo(left_child_new);
				BranchInfo right_branch = decision_tree_.GetBranchInfo(right_child_new);

				uint32_t ub_left = cache_->RetrieveUpperBound(left_branch, left_child_new->GetNodeBudget());
				uint32_t ub_right = cache_->RetrieveUpperBound(left_branch, right_child_new->GetNodeBudget());

				if (left_child_new->GetExternalMisclassificationBudget() > ub_left)
				{
					left_child_new->UpdateExternalBudget(ub_left);
				}

				if (right_child_new->GetExternalMisclassificationBudget() > ub_right)
				{
					right_child_new->UpdateExternalBudget(ub_right);
				}
			}*/
			return true;
		}
		else
		{
			bool should_propagate = false;
			if (!node->AreThereAnyAssignmentsLeft()) 
			{ 
				node->MarkExhausted();
				should_propagate = true; 
			}
			//remember to free the nodes previously allocated
			decision_tree_.PushFreeNode(left_child_new);
			decision_tree_.PushFreeNode(right_child_new);
			return true;//not sure, but propagating does not hurt? Need to think about this since if we do not propagate the tree will not correctly capture the current state, which is not important for the current algorithm but if we make changes it might be
			return should_propagate;
		}
	}

	bool State::PrepareDataForChildren(LearningData &data, uint32_t feature, LearningData &data_without_feature, LearningData &data_with_feature)
	{
		data.SplitData(feature, data_without_feature, data_with_feature);

		if (data_without_feature.IsEmpty() || data_with_feature.IsEmpty())
		{
			data_without_feature.Clear();
			data_with_feature.Clear();
			return false;
		}
		return true;
	}

	void State::PropagateNode(DecisionNode * node)
	{
		PropagateUpwards(node);
		PropagateDownwards();		
	}

	void State::PropagateUpwards(DecisionNode * node)
	{
		if (node->IsExhausted()) { CacheBranch(node); }

		while (IsRootNode(node) == false)
		{
			DecisionNode * parent = decision_tree_.GetParent(node);
			DecisionNode * parent_left_child = decision_tree_.GetLeftChild(parent);
			DecisionNode * parent_right_child = decision_tree_.GetRightChild(parent);

			DecisionNodeAssignment new_assignment(
				parent->GetAssignedFeature(),
				parent_left_child->GetCurrentAssignment().NumNodes(),
				parent_right_child->GetCurrentAssignment().NumNodes(),
				parent_left_child->GetNumFalsePositives() + parent_right_child->GetNumFalsePositives(),
				parent_left_child->GetNumFalseNegatives() + parent_right_child->GetNumFalseNegatives()
			);
					
			parent->UpdateCurrentAssignment(new_assignment);
			//if both children have a feasible solution, then maybe a new best solution has been found.
			//	todo check: if this condition holds, are we guaranteed to have a new best solution?
			if (parent_left_child->HasFeasibleSolution() && parent_right_child->HasFeasibleSolution())
			{
				/*DecisionNodeAssignment possibly_new_best_assignment(
					parent->GetAssignedFeature(),
					parent_left_child->GetBestMisclassification() + parent_right_child->GetBestMisclassification(),
					parent_left_child->GetBestAssignments().NumNodes(),
					parent_right_child->GetBestAssignments().NumNodes()
				);*/
				AssignmentContainer candidate_best = parent_left_child->assignments_best_;
				candidate_best.CombineUsingAddition(
					parent_right_child->assignments_best_, 
					parent->GetAssignedFeature(),
					parent_left_child->GetNodeBudget(),
					parent_right_child->GetNodeBudget()
				);
				parent->UpdateBestAssignments(candidate_best);
			}

			bool parent_exhaustively_explored = parent_left_child->IsExhausted() && parent_right_child->IsExhausted() && !parent->AreThereAnyAssignmentsLeft();
			bool optimal_solution_reached = parent->HasFeasibleSolution()
				//&& (parent->GetBestMisclassification() == GetLowerBound(parent));
				&& GetLowerBound(parent) == parent->assignments_best_;
			
			
			bool debug = GetLowerBound(parent) == (parent->assignments_best_);
			debug &= !parent->HasFeasibleSolution();
			runtime_assert(!debug);

			if (parent_exhaustively_explored || optimal_solution_reached)
			{
				parent->MarkExhausted();
				CacheBranch(parent);
			}

			node = parent;
		}
	}

	void State::PropagateDownwards()
	{
		if (parameters_.use_pruning == false) { return; }

		//perform pruning, but only until one node is pruned. The process is restarted each time a node is pruned, and we iterate until a fixed point.
		bool pruning_happened = true;
		while (pruning_happened && GetRootNode()->IsActivated() && !IsRootNodeExhausted())
		{
			pruning_happened = PropagateDownwardsFromNodeUntilFirstPruning(GetRootNode());
			/*if (pruning_happened) {
				std::cout << "Pruned: " << std::endl;// system("pause");
			}*/
		}		
	}

	//assumes the budgets are appropriately set for the node and its descendents, i.e. the budgets should not be lower than they should be if they would be recomputed from scratch
	//this is always the case since we stop after the first pruning
	bool State::PropagateDownwardsFromNodeUntilFirstPruning(DecisionNode * node)
	{
		assert(node->IsActivated());

		AssignmentContainer lb_node = GetLowerBound(node);

		/*bool ignore_pruning = node->HasFeasibleSolution() && (node->GetBestMisclassification() == lb_node); //pruning is ignored for nodes that are optimal, since modified budget will ask for a better solution and then prune it. Seems like a hack, should look into this.
		assert(!ignore_pruning || node->IsExhausted());
		if (!ignore_pruning && lb_node > node->GetModifiedMisclassificationBudget()) 
		{
			PruneNode(node);
			return true;
		}*/		

		if (node->GetExternalMisclassificationBudget().AreAssignmentsReverseStrictlyDominatedByInput(lb_node))
		{
			PruneNode(node);
			return true;
		}
				
		if (node->IsExhausted() || IsTerminalNode(node)) { return false; }
				
		if (!decision_tree_.HasChildren(node)) { return false; }

		DecisionNode * left_child = decision_tree_.GetLeftChild(node);
		DecisionNode * right_child = decision_tree_.GetRightChild(node);

		AssignmentContainer modified_budget = node->GetModifiedMisclassificationBudget();
		AssignmentContainer lb_left = GetLowerBound(left_child);
		AssignmentContainer lb_right = GetLowerBound(right_child);
		AssignmentContainer lb_combined = lb_left;
		lb_combined.CombineUsingAddition(lb_right, UINT32_MAX, 0, 0);
		runtime_assert(lb_left.Size() > 0 && lb_right.Size() > 0 && lb_combined.Size() > 0);

		//if (lb_left + lb_right > modified_budget)
		if (modified_budget.AreAssignmentsReverseStrictlyDominatedByInput(lb_combined))
		{
			PruneChildren(node);
			return true;
		}

		assert(left_child->IsActivated() && right_child->IsActivated());
		//assert(lb_left <= modified_budget && lb_right <= modified_budget);
		
		//todo enable this assert? I think it's strict domination not regular domination
		//assert(lb_left.DoInputAssignmentsContainDominatingSolution(modified_budget) == false && lb_right.DoInputAssignmentsContainDominatingSolution(modified_budget) == false);
		
		if (parameters_.use_upper_bounding == false)
		{
			AssignmentContainer temp = modified_budget;
			right_child->UpdateExternalBudget(temp);//(modified_budget - lb_right);
			left_child->UpdateExternalBudget(temp);
		}
		else if (1 == 1)
		{
			AssignmentContainer temp = modified_budget;
			temp.CombineUsingSubtraction(lb_left, UINT32_MAX, 0, 0);
			right_child->UpdateExternalBudget(temp);//(modified_budget - lb_right);

			temp = modified_budget;
			temp.CombineUsingSubtraction(lb_right, UINT32_MAX, 0, 0);
			left_child->UpdateExternalBudget(temp);
		}
		else
		{
			if (right_child->IsExhausted())
			{
				AssignmentContainer temp = modified_budget;
				temp.CombineUsingSubtraction(lb_left, UINT32_MAX, 0, 0);
				left_child->UpdateExternalBudget(temp);//(modified_budget - lb_right);
			}
			else if (left_child->IsExhausted())
			{

			}

			AssignmentContainer temp = modified_budget;
			temp.CombineUsingSubtraction(lb_left, UINT32_MAX, 0, 0);
			left_child->UpdateExternalBudget(temp);//(modified_budget - lb_right);

			temp = modified_budget;
			temp.CombineUsingSubtraction(lb_right, UINT32_MAX, 0, 0);
			right_child->UpdateExternalBudget(temp);
		}
		
		bool pruned_node = PropagateDownwardsFromNodeUntilFirstPruning(left_child);
		if (pruned_node) { return true; }

		assert(right_child->IsActivated());
		return PropagateDownwardsFromNodeUntilFirstPruning(right_child);
	}

	void State::PruneNode(DecisionNode * node)
	{
		assert(node->IsActivated());

		//pruning a node will prune the given node, but also the other child of the node's parent
		//if there are no options left for the parent, then the parent will be marked exhausted and propagated upwards
		if (IsRootNode(node)) { node->Deactivate(); }
				
		DecisionNode * parent = decision_tree_.GetParent(node);
		assert(decision_tree_.GetLeftChild(parent)->IsActivated() && decision_tree_.GetRightChild(parent)->IsActivated());

		decision_tree_.DeactivateDescendents(parent);

		if (!parent->AreThereAnyAssignmentsLeft())
		{			
			parent->MarkExhausted();
			PropagateUpwards(parent);
		}
	}

	void State::PruneChildren(DecisionNode * node)
	{
		assert(!IsTerminalNode(node));
		assert(decision_tree_.GetLeftChild(node)->IsActivated() && decision_tree_.GetRightChild(node)->IsActivated());

		PruneNode(decision_tree_.GetLeftChild(node)); //note that this will also prune the right child, and maybe this node if it has no more features left
	}

	uint32_t State::MaxNumNodesForDepth(uint32_t depth)
	{
		return (1 << depth) - 1;
	}

	void State::SolveTerminalNode(DecisionNode * node)
	{
		runtime_assert(node->GetData()->ComputeClassificationCost() > 0);
		runtime_assert(tree_solver_left_->IsTerminalNode(node, this));

		statistics_.num_nodes_processed_terminal++;

		LearningData * data = node->GetData();
		AssignmentContainer optimal_assignments;

		uint32_t left_score = tree_solver_left_->ProbeDifference(*data);
		uint32_t right_score = tree_solver_right_->ProbeDifference(*data);
		SpecialisedOptimalBinaryClassificationTreeSolverDepth2* last_solver_used = NULL;
		if (left_score < right_score)
		{
			optimal_assignments = tree_solver_left_->Solve(*data, node->GetNodeBudget());
			last_solver_used = tree_solver_left_;
		}
		else
		{
			optimal_assignments = tree_solver_right_->Solve(*data, node->GetNodeBudget());
			last_solver_used = tree_solver_right_;
		}
		
		//todo maybe remove the zero one and incoporate it in the big tree computer, it just skips the second loop if there's not enough node budgets?
		if (node->GetNodeBudget() == 1 || parameters_.unit_tree_depth == 1)
		{
		}
		else if (parameters_.unit_tree_depth == 2)
		{
		}
		else if (parameters_.unit_tree_depth == 0)
		{
			std::cout << "NEED FIX\n";
			runtime_assert(1 == 2);
			//optimal_assignment.misclassifications = data->ComputeClassificationCost();
		}
		else
		{
			std::cout << "Unit tree depth strange: " << parameters_.unit_tree_depth << ", aborting\n";
			exit(1);
		}
		node->Finalise(optimal_assignments);
		
		//hacky solution for now to get the other solution cached: cache the other node budget; then proceed as usual
		if (node->GetNodeBudget() == 2 || node->GetNodeBudget() == 3)
		{
			//since we compute the optimal assignment for 2 and 3 nodes, better cache both
			int current_budget = node->GetNodeBudget();
			int other_budget = (current_budget == 2 ? 3 : 2);

			node->node_budget_selector_->SetNodeBudget_TemporaryHack(other_budget);
			node->assignments_best_ = (other_budget == 2 ? last_solver_used->optimal_at_most_two_nodes : last_solver_used->optimal_at_most_three_nodes);
			node->assignment_current_ = node->assignments_best_.GetStoredAssignments()[0]; //arbitrary 
			CacheBranch(node);

			//revert to the original assignment and proceed as usual
			node->node_budget_selector_->SetNodeBudget_TemporaryHack(current_budget);
			node->assignments_best_ = (current_budget == 2 ? last_solver_used->optimal_at_most_two_nodes : last_solver_used->optimal_at_most_three_nodes);
			node->assignment_current_ = node->assignments_best_.GetStoredAssignments()[0]; //arbitrary 
		}		

		PropagateNode(node);
	}

	AssignmentContainer State::GetSolutionsForNode(DecisionNode* node, DecisionTree &tree)
	{
		//runtime_assert(node->GetNodeBudget() == 1 || node->GetNodeBudget() == 0);

		if (node->GetNodeBudget() == 1)
		{
			return tree_solver_left_->tree_solver_depth1_.ComputeOptimalAssignments(*node->GetData());
		}
		else if (node->GetNodeBudget() == 0)
		{
			AssignmentContainer c;
			DecisionNodeAssignment a;
			a.left_node_count = 0;
			a.right_node_count = 0;
			a.feature = UINT32_MAX;
			//label node as zero
			a.num_false_negatives = node->GetData()->NumInstancesForLabel(1);
			a.num_false_positives = 0;
			c.AddAssignment(a);
			a.num_false_negatives = 0;
			a.num_false_positives = node->GetData()->NumInstancesForLabel(0);
			c.AddAssignment(a);
			return c;
		}
		else
		{
			bool is_terminal = false;
			if (node->GetNodeBudget() == 2)
			{
				is_terminal = true;
			}
			else if (node->GetNodeBudget() == 3 && (tree.GetNodeDepth(node) + 1 == parameters_.max_depth))
			{
				is_terminal = true;
			}
			else
			{
				is_terminal = false;
			}

			if (is_terminal == false)
			{
				BranchInfo branch_info = tree.GetBranchInfo(node);
				if (!cache_->IsOptimalAssignmentCached(branch_info, node->GetNodeBudget())
					&& !cache_backup_->IsOptimalAssignmentCached(branch_info, node->GetNodeBudget())
					)
				{
					return AssignmentContainer();
					std::cout << "wat\n";
					std::cout << branch_info.Size() << "\n";
					std::cout << node->Index() << "\n";
					std::cout << node->GetNodeBudget() << "\n";
					runtime_assert(1 == 2);
					system("pause");
				}
				if (cache_->IsOptimalAssignmentCached(branch_info, node->GetNodeBudget()))
					return cache_->RetrieveOptimalAssignment(branch_info, node->GetNodeBudget());
				else
					return cache_backup_->RetrieveOptimalAssignment(branch_info, node->GetNodeBudget());


				return cache_->RetrieveOptimalAssignment(branch_info, node->GetNodeBudget());
			}
			else
			{
				return tree_solver_left_->Solve(*node->GetData(), node->GetNodeBudget());
			}
		}
	}

	DecisionNode * State::GetRootNode()
	{
		return decision_tree_.GetRootNode();
	}

	bool State::IsTerminalNode(DecisionNode * node)
	{
		if (parameters_.unit_tree_depth != 2) { std::cout << "unit trees other than two are not enabled...todo\n"; }

		return tree_solver_left_->IsTerminalNode(node, this);

		//a terminal node is a node that should be processed with a specialised algorithm
		if (node->GetNodeBudget() == 1)
		{
			return true;
		}
		if (node->GetNodeBudget() == 2)
		{
			return true;
		}
		else if (node->GetNodeBudget() == 3 && (decision_tree_.GetNodeDepth(node) + 1 == parameters_.max_depth))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool State::IsRootNode(DecisionNode * node)
	{
		return node->Index() == 0;
	}

	bool State::IsRootNodeExhausted()
	{
		return GetRootNode()->IsExhausted();
	}

	DecisionTree * State::ReconstructBestSolution()
	{
		runtime_assert(1 == 2);
		return NULL;
		/*DecisionTree *best_tree = new DecisionTree(data_.NumLabels(), data_.NumFeatures(), parameters_.max_depth, decision_tree_.GetFeatureOrderingStrategy(), decision_tree_.GetSizeSplitStrategy());
		DecisionNode * root_copy = best_tree->GetRootNode();
		*root_copy->GetData() = data_; //hack, activating a node requires non empty data...
		DecisionNode *root = GetRootNode();
		auto assignment = root->GetBestAssignments();
		root_copy->Activate(assignment.NumNodes());
		root_copy->UpdateCurrentAssignment(assignment);
		ReconstructBestSolution_node(root_copy, *best_tree);
		return best_tree;*/
	}

	DecisionTree* State::ReconstructBestParetoTree(DecisionNodeAssignment root_assignment)
	{
		DecisionTree *best_tree = new DecisionTree(data_.NumLabels(), data_.NumFeatures(), parameters_.max_depth, decision_tree_.GetFeatureOrderingStrategy(), decision_tree_.GetSizeSplitStrategy());
		DecisionNode * root_copy = best_tree->GetRootNode();
		*root_copy->GetData() = data_; //hack, activating a node requires non empty data...
		DecisionNode *root = GetRootNode();
		auto assignment = root_assignment;
		root_copy->Activate(assignment.NumNodes());
		root_copy->UpdateCurrentAssignment(assignment);
		ReconstructBestSolution_node(root_copy, *best_tree, root_assignment);
		return best_tree;
	}

	bool State::ReconstructBestSolution_node(DecisionNode *node_copy, DecisionTree & tree, DecisionNodeAssignment target_assignment)
	{//assumes node_copy has been activated already with the appropriate data
		BranchInfo branch_info = tree.GetBranchInfo(node_copy);
		DecisionNodeAssignment assignment;

		bool is_terminal = false;
		if (node_copy->GetNodeBudget() == 0 || target_assignment.NumNodes() == 0)
		{
			if (target_assignment.num_false_negatives != 0 && target_assignment.num_false_positives != 0)
			{
				node_copy->hax_label_ = -1;
				return false;
			}

			if (node_copy->GetData()->NumInstancesForLabel(0) == target_assignment.num_false_positives)
			{
				//std::cout << "hehe\n";
				node_copy->hax_label_ = 1;
				node_copy->AssignFeatureTemporary(UINT32_MAX);
				return true;
			}

			if (node_copy->GetData()->NumInstancesForLabel(1) == target_assignment.num_false_negatives)
			{
				node_copy->hax_label_ = 0;
				//std::cout << "huhu\n";
				node_copy->AssignFeatureTemporary(UINT32_MAX);
				return true;
			}

			node_copy->hax_label_ = -1;
			return false;
		}
		else if (node_copy->GetNodeBudget() == 1)
		{
			AssignmentContainer c = tree_solver_left_->tree_solver_depth1_.ComputeOptimalAssignments(*node_copy->GetData());
			for (auto sol : c.GetStoredAssignments())
			{
				if (sol.num_false_negatives == target_assignment.num_false_negatives &&
					sol.num_false_positives == target_assignment.num_false_positives)
				{
					node_copy->UpdateCurrentAssignment(sol);

					//split for children
					DecisionNode* left_child_copy = tree.PopFreeNode();
					DecisionNode* right_child_copy = tree.PopFreeNode();

					tree.SetChildren(node_copy, left_child_copy, right_child_copy);

					//bool success_split = PrepareDataForChildren(*node_copy->GetData(), node_copy->GetAssignedFeature(), *left_child_copy->GetData(), *right_child_copy->GetData());
					node_copy->GetData()->SplitData(node_copy->GetAssignedFeature(), *left_child_copy->GetData(), *right_child_copy->GetData());
					
					left_child_copy->Activate(0);
					right_child_copy->Activate(0);

					runtime_assert(left_child_copy->assignments_best_.Size() == 2 || left_child_copy->GetData()->NumInstancesForLabel(0) == 0 || left_child_copy->GetData()->NumInstancesForLabel(1) == 0);
					runtime_assert(right_child_copy->assignments_best_.Size() == 2 || right_child_copy->GetData()->NumInstancesForLabel(0) == 0 || right_child_copy->GetData()->NumInstancesForLabel(1) == 0);

					for (auto left_assign : left_child_copy->assignments_best_.GetStoredAssignments())
					{
						for (auto right_assign : right_child_copy->assignments_best_.GetStoredAssignments())
						{
							if (left_assign.num_false_negatives + right_assign.num_false_negatives == target_assignment.num_false_negatives &&
								left_assign.num_false_positives + right_assign.num_false_positives == target_assignment.num_false_positives)
							{
								bool s1 = ReconstructBestSolution_node(left_child_copy, tree, left_assign);
								if (!s1) { continue; }
								bool s2 = ReconstructBestSolution_node(right_child_copy, tree, right_assign);
								if (!s2) { continue; }
								return true;
							}
						}
					}					
				}
			}
			return false;
		}
		if (node_copy->GetNodeBudget() == 2)
		{
			is_terminal = true;
		}
		else if (node_copy->GetNodeBudget() == 3 && (tree.GetNodeDepth(node_copy) + 1 == parameters_.max_depth))
		{
			is_terminal = true;
		}
		else
		{
			is_terminal = false;
		}
		
		AssignmentContainer pf;
		if (tree.IsRootNode(node_copy))
		{
			pf.AddAssignment(node_copy->GetCurrentAssignment());
		}
		else if (is_terminal)
		{
			if (node_copy->GetNodeBudget() == 3 || node_copy->GetNodeBudget() == 2)
				pf = tree_solver_left_->Solve(*node_copy->GetData(), node_copy->GetNodeBudget());
			else
			{
				runtime_assert(1 == 2);
			}
		}
		else 
		{
			if (!cache_->IsOptimalAssignmentCached(branch_info, node_copy->GetNodeBudget()) &&
				!cache_backup_->IsOptimalAssignmentCached(branch_info, node_copy->GetNodeBudget()))
			{
				std::cout << "wat\n";
				std::cout << branch_info.Size() << "\n";
				std::cout << node_copy->Index() << "\n";
				std::cout << node_copy->GetNodeBudget() << "\n";
				runtime_assert(1 == 2);
				return false;
				system("pause");
			}

			if (cache_->IsOptimalAssignmentCached(branch_info, node_copy->GetNodeBudget()))
				pf = cache_->RetrieveOptimalAssignment(branch_info, node_copy->GetNodeBudget());
			else
				pf = cache_backup_->RetrieveOptimalAssignment(branch_info, node_copy->GetNodeBudget());
		}
				
		for (auto a : pf.GetStoredAssignments())
		{
			if (a.num_false_negatives == target_assignment.num_false_negatives &&
				a.num_false_positives == target_assignment.num_false_positives)
			{
				node_copy->UpdateCurrentAssignment(a);

				//split for children
				DecisionNode* left_child_copy = tree.PopFreeNode();
				DecisionNode* right_child_copy = tree.PopFreeNode();

				tree.SetChildren(node_copy, left_child_copy, right_child_copy);

				//bool success_split = PrepareDataForChildren(*node_copy->GetData(), node_copy->GetAssignedFeature(), *left_child_copy->GetData(), *right_child_copy->GetData());
				node_copy->GetData()->SplitData(node_copy->GetAssignedFeature(), *left_child_copy->GetData(), *right_child_copy->GetData());

				if (left_child_copy->GetData()->IsEmpty() == false)
				{
					left_child_copy->Activate(a.left_node_count);
				}
				else
				{
					left_child_copy->Activate(0);
				}

				if (right_child_copy->GetData()->IsEmpty() == false)
				{
					right_child_copy->Activate(a.right_node_count);
				}
				else
				{
					right_child_copy->Activate(0);
				}

				AssignmentContainer left_solutions = GetSolutionsForNode(left_child_copy, tree);
				AssignmentContainer right_solutions = GetSolutionsForNode(right_child_copy, tree);

				if (left_solutions.IsEmpty() || right_solutions.IsEmpty())
				{
					return false;
				}

				for (auto left_assign : left_solutions.GetStoredAssignments())
				{
					for (auto right_assign : right_solutions.GetStoredAssignments())
					{
						if (left_assign.num_false_negatives + right_assign.num_false_negatives == target_assignment.num_false_negatives &&
							left_assign.num_false_positives + right_assign.num_false_positives == target_assignment.num_false_positives)
						{
							bool s1 = ReconstructBestSolution_node(left_child_copy, tree, left_assign);
							if (!s1) { continue; }
							bool s2 = ReconstructBestSolution_node(right_child_copy, tree, right_assign);
							if (!s2) { continue; }
							return true;
						}
					}
				}
				////----
			}
		}

		return false;

		runtime_assert(1 == 2);

		/*

		
		//reconstruct the assignment for the node. The relevant assignments are in the cache, with two exceptions:
			//Label nodes are not stored in cache since the assignment is directly present in the node
			//The root node is a special case since it is set before calling this method, and the assignment is in the node
		
		//therefore, first check if the assignment is already present in the node
		if (tree.GetRootNode() == node_copy || node_copy->GetNodeBudget() == 0) 
		{
			runtime_assert(1 == 2);
			//find the target assignment among the two combinations
			assignment = node_copy->GetBestAssignments();
		}
		//otherwise find the assignment in the cache
		else
		{
			if (!cache_->IsOptimalAssignmentCached(branch_info, node_copy->GetNodeBudget()))
			{
				std::cout << "wat\n";
				std::cout << branch_info.Size() << "\n";
				std::cout << node_copy->Index() << "\n";
				std::cout << node_copy->GetNodeBudget() << "\n";
				assert(1 == 2);
				return;
				system("pause");
			}
			else
			{
				assignment = cache_->RetrieveOptimalAssignment(branch_info, node_copy->GetNodeBudget());
			}
		}
		//node_copy->Finalise(assignment);
		node_copy->UpdateCurrentAssignment(assignment);

		if (parameters_.unit_tree_depth != 2) { std::cout << "Can only reconstruct solutions with unit tree of depth two\n"; system("pause"); exit(1); }

		bool is_terminal = false;
		if (node_copy->GetNodeBudget() == 1) 
		{
			is_terminal = true;
		}
		if (node_copy->GetNodeBudget() == 2)
		{
			is_terminal = true;
		}
		else if (node_copy->GetNodeBudget() == 3 && (tree.GetNodeDepth(node_copy) + 1 == parameters_.max_depth))
		{
			is_terminal = true;
		}
		else
		{
			is_terminal = false;
		}
				
		if (is_terminal)
		{//the issue is that the children of terminal nodes are not cached so they need to be reconstructed
			if (assignment.NumNodes() == 0) { return; } //nodes are by default classification nodes, so this is covered already
			//tree_solver_left_->SolveFull(node_copy, this, &tree);
			AssignmentContainer s = tree_solver_left_->Solve(*node_copy->GetData(), target_assignment.NumNodes());
			//assert(node_copy->GetMisclassification() == assignment.misclassifications);
			
			
			//in some cases the assignment compute now and before can differ but both should be of the same cost and within the node budget
			//this can happen if a node is given a big budget, but it finds a solution that meets the terminal node criteria

		}
		else if (assignment.NumNodes() > 1) //has children, solve recursively
		{
			DecisionNode * left_child_copy = tree.PopFreeNode();
			DecisionNode * right_child_copy = tree.PopFreeNode();

			tree.SetChildren(node_copy, left_child_copy, right_child_copy);

			PrepareDataForChildren(*node_copy->GetData(), node_copy->GetAssignedFeature(), *left_child_copy->GetData(), *right_child_copy->GetData());

			left_child_copy->Activate(assignment.left_node_count);
			right_child_copy->Activate(assignment.right_node_count);

			ReconstructBestSolution_node(left_child_copy, tree);
			ReconstructBestSolution_node(right_child_copy, tree);
		}
		else if (assignment.NumNodes() == 1) //simple assignment, no need to go further
		{
			DecisionNode * left_child_copy = tree.PopFreeNode();
			DecisionNode * right_child_copy = tree.PopFreeNode();

			PrepareDataForChildren(*node_copy->GetData(), node_copy->GetAssignedFeature(), *left_child_copy->GetData(), *right_child_copy->GetData());
			tree.SetChildren(node_copy, left_child_copy, right_child_copy);
			left_child_copy->Activate(0);
			right_child_copy->Activate(0);
		}
		else if (assignment.NumNodes() == 0) //simple assignment, no need to go further
		{
			//should be okay since nodes are label nodes by default
		}
		else //I think this can never happen, probably should delete this IF
		{
			std::cout << "problem??\n";
			system("pause");
		}*/
	}

	uint32_t State::DepthBelowNode(DecisionNode * node)
	{
		assert(node->IsActivated());
		if (node->IsExhausted() || IsTerminalNode(node) || !decision_tree_.HasChildren(node)) { return 0; }

		DecisionNode * left_child = decision_tree_.GetLeftChild(node);
		DecisionNode * right_child = decision_tree_.GetRightChild(node);

		return 1 + std::max(DepthBelowNode(left_child), DepthBelowNode(right_child));
	}

	AssignmentContainer State::GetBestAssignments()
	{
		return decision_tree_.GetRootNode()->GetBestAssignments();
	}

	bool State::IsStateCorrect()
	{//not really an exhaustive check but it does cover some parts
		return true; //need to modify with the new bi objective view
		
		/*static std::vector<DecisionNode*> nodes_to_process;
		nodes_to_process.clear();

		nodes_to_process.push_back(GetRootNode());
		while (!nodes_to_process.empty())
		{
			DecisionNode * current_node = nodes_to_process.back();
			nodes_to_process.pop_back();

			if (!decision_tree_.HasChildren(current_node)) { continue; }

			DecisionNode * left_child = decision_tree_.GetLeftChild(current_node);
			DecisionNode * right_child = decision_tree_.GetRightChild(current_node);

			if (left_child->HasFeasibleSolution() && right_child->HasFeasibleSolution())
			{
				uint32_t children_best = left_child->GetBestAssignments().misclassifications + right_child->GetBestAssignments().misclassifications;
				if (current_node->GetBestAssignments().misclassifications > children_best && children_best <= current_node->GetExternalMisclassificationBudget())
				{
					std::cout << "not great bug\n";
					assert(1 == 2);
					system("pause");
					exit(1);
				}
			}

			nodes_to_process.push_back(left_child);
			nodes_to_process.push_back(right_child);
		}

		return true;*/
	}

	void State::InitialiseNode(DecisionNode * node, uint32_t size_budget)
	{
		runtime_assert(node->GetData()->Size() > 0);

		if (size_budget == 0)
		{
			node->Activate(0);
			node->MarkExhausted();
		}
		else
		{
			bool restored_left_child = RestoreBranchFromCache(node, size_budget);
			if (!restored_left_child)
			{
				//compute the similiarity bound, and update if it is greater than zero. Note that the cache stores the greatest lb it has seen so far
				BranchInfo branch_info = decision_tree_.GetBranchInfo(node);
				AssignmentContainer similiarity_lb = similiarity_lower_bound_computer_.ComputeLowerBound(*node->GetData(), decision_tree_.GetNodeDepth(node), size_budget);
				/*if (1 || similiarity_lb > 0)*/ { cache_->StoreBranchLowerBound(branch_info, similiarity_lb, size_budget); } //todo: we should check if the bound is nontrivial to avoid computation

				node->Activate(size_budget);
				if (GetLowerBound(node) == node->assignments_best_) { node->MarkExhausted(); }
								
				//if (node->GetMisclassification() == GetLowerBound(node)) { node->MarkExhausted(); }
			}
		}
	}

	void State::CacheBranch(DecisionNode * node)
	{
		BranchInfo branch_info = decision_tree_.GetBranchInfo(node);
		runtime_assert(!cache_->IsOptimalAssignmentCached(branch_info, node->GetNodeBudget()));
		runtime_assert(node->IsExhausted());

		if (!(IsTerminalNode(node) || node->AreAllBestAssignmentsBorderline() || node->AreAllSolutionsFeasible()))
		{
			AssignmentContainer feasible;
			for (auto c : node->GetBestAssignments().GetStoredAssignments())
			{
				if (node->IsAssignmentFeasible(c))
				{
					feasible.AddAssignment(c);
				}
			}
			cache_backup_->StoreOptimalBranchAssignments(branch_info, node->GetBestAssignments(), node->GetNodeBudget());
		}

		if (IsTerminalNode(node) || node->AreAllBestAssignmentsBorderline() || node->AreAllSolutionsFeasible())// node->GetBestMisclassification() <= node->GetExternalBudget()) //optimal solution
		{
			//todo: reintroduce this assert...runtime_assert(node->GetAssignedFeature() != UINT32_MAX || node->GetAssignedFeature() == UINT32_MAX && node->GetData()->ComputeClassificationCost() == node->GetBestMisclassification());
			cache_->StoreOptimalBranchAssignments(branch_info, node->GetBestAssignments(), node->GetNodeBudget());
			similiarity_lower_bound_computer_.UpdateArchive(node->data_, node->GetBestAssignments(), decision_tree_.GetNodeDepth(node), node->GetNodeBudget());
		}
		else if (node->AreAllSolutionsInfeasible())
		{
			if (parameters_.use_infeasibility_lower_bounding)
			{
				cache_->StoreBranchLowerBound(branch_info, node->external_misclassification_budget_, node->GetNodeBudget());
			}
			similiarity_lower_bound_computer_.UpdateArchive(node->data_, node->external_misclassification_budget_, decision_tree_.GetNodeDepth(node), node->GetNodeBudget());
		}
		else if (0)
		{
			if (decision_tree_.HasChildren(node))
			{
				DecisionNode* left_child = decision_tree_.GetLeftChild(node);
				DecisionNode* right_child = decision_tree_.GetRightChild(node);

				if (IsTerminalNode(left_child) && IsTerminalNode(right_child) || node->AreAllSolutionsFeasible() && left_child->IsExhausted() && right_child->IsExhausted())
				{
					cache_->StoreOptimalBranchAssignments(branch_info, node->GetBestAssignments(), node->GetNodeBudget());
					similiarity_lower_bound_computer_.UpdateArchive(node->data_, node->GetBestAssignments(), decision_tree_.GetNodeDepth(node), node->GetNodeBudget());
				}
			}
		}
		else if (0 && node->HasFeasibleSolution())
		{
			runtime_assert(node->HasFeasibleSolution());
			//need to remove the best assignments that are not feasible
			AssignmentContainer optimal_solutions = node->GetBestAssignments();
			AssignmentContainer removed_solutions = optimal_solutions.RemoveSolutionsDominatedByInput(node->external_misclassification_budget_);

			for (auto& assignment : removed_solutions.GetStoredAssignments())
			{
				if (node->GetExternalMisclassificationBudget().IsEquivalentAssignmentPresent(assignment))
				{
					optimal_solutions.AddAssignment(assignment);
				}
			}
			
			if (optimal_solutions.IsEmpty())
			{
				cache_->StoreBranchLowerBound(branch_info, node->external_misclassification_budget_, node->GetNodeBudget());
				similiarity_lower_bound_computer_.UpdateArchive(node->data_, node->external_misclassification_budget_, decision_tree_.GetNodeDepth(node), node->GetNodeBudget());
			}
			//todo: not sure what to do here. Some of the solutions are definitely optimal, but since we pruned one, 
			//we do not know its true best value. Lower bounds assert that every other solution is covered 
			//by our definition...we can use these as some kind of 'partial' lower bound, but...hmm
			//partial means that if we reach this solution, no point going further
			else if (optimal_solutions.Size() < node->GetBestAssignments().Size())
			{
				AssignmentContainer lb = node->GetBestAssignments();
				lb.MergeMinimisation(node->GetExternalMisclassificationBudget());
				//todo: not sure this must hold: runtime_assert(lb.Size() == node->GetExternalMisclassificationBudget().Size());
				cache_->StoreBranchLowerBound(branch_info, lb, node->GetNodeBudget());
				similiarity_lower_bound_computer_.UpdateArchive(node->data_, lb, decision_tree_.GetNodeDepth(node), node->GetNodeBudget());
			}
			else if (optimal_solutions.Size() == node->GetBestAssignments().Size()) //no assignments were removed
			{
				cache_->StoreOptimalBranchAssignments(branch_info, optimal_solutions, node->GetNodeBudget());
				similiarity_lower_bound_computer_.UpdateArchive(node->data_, optimal_solutions, decision_tree_.GetNodeDepth(node), node->GetNodeBudget());
			}
		}
		//even if the node does not have a feasible solution, we will store its solution if its a terminal node, since these are optimal
		else if (0 && IsTerminalNode(node)) //todo: remove
		{
			//cache_->StoreOptimalBranchAssignment(branch_info, node->GetCurrentAssignment(), node->GetNodeBudget());
			//actually in this version this is not necessarily the case since the node can be prune due to the lower bound mechanism
			/*if (node->hax_pruned_by_lower_bound)
			{
				cache_->StoreBranchLowerBound(branch_info, node->hax_lower_bound, node->GetNodeBudget());
				similiarity_lower_bound_computer_.UpdateArchive(node->data_, node->hax_lower_bound, decision_tree_.GetNodeDepth(node));
			}
			else*/

			//the problem is that the terminal node computation may generate multiple solutions but they might not be feasible!
			
			//cache_->StoreOptimalBranchAssignments(branch_info, node->GetCurrentAssignment(), node->GetNodeBudget());
			//similiarity_lower_bound_computer_.UpdateArchive(node->data_, node->GetCurrentAssignment().misclassifications, decision_tree_.GetNodeDepth(node), node->GetNodeBudget());
		}
		//here the point is that we could have pruned some solutions that we know are the optimal value...so it makes sense to store them...
		else if (0 && node->AreAllBestAssignmentsBorderline())
		{
			//cache_->StoreOptimalBranchAssignments(branch_info, node->assignment_pruned_, node->GetNodeBudget());
			//similiarity_lower_bound_computer_.UpdateArchive(node->data_, node->assignment_pruned_.misclassifications, decision_tree_.GetNodeDepth(node), node->GetNodeBudget());
		}
		else if (0)
		{
			//cache_->StoreBranchLowerBound(branch_info, node->GetExternalMisclassificationBudget()+1, node->GetNodeBudget());
			cache_->StoreBranchLowerBound(branch_info, node->external_misclassification_budget_, node->GetNodeBudget());
			similiarity_lower_bound_computer_.UpdateArchive(node->data_, node->external_misclassification_budget_, decision_tree_.GetNodeDepth(node), node->GetNodeBudget());
		}
	}

	bool State::RestoreBranchFromCache(DecisionNode * node, uint32_t node_budget)
	{
		assert(node->IsActivated() == false);
		BranchInfo branch_info = decision_tree_.GetBranchInfo(node);
		bool cache_hit = cache_->IsOptimalAssignmentCached(branch_info, node_budget);

		statistics_.num_cache_hit_optimality += cache_hit;
		statistics_.num_cache_miss_optimality += !cache_hit;

		if (cache_hit)
		{
			AssignmentContainer optimal_assignment = cache_->RetrieveOptimalAssignment(branch_info, node_budget);
			node->Activate(node_budget);
			node->Finalise(optimal_assignment);
			return true;
		}
		else
		{
			return false;
		}
	}

	AssignmentContainer State::GetLowerBound(DecisionNode * node)
	{
		//I think we can avoid cache calls if the current node is optimal, probably makes little difference though
		BranchInfo branch_info = decision_tree_.GetBranchInfo(node);
		AssignmentContainer lb = cache_->RetrieveLowerBound(branch_info, node->GetNodeBudget());
		assert(!node->HasFeasibleSolution() || lb.DoInputAssignmentsContainStrictlyDominatingSolution(node->assignments_best_) == false);
		return lb;
	}
}