#pragma once

#include <stdint.h>
#include <vector>

namespace DecisionTreeSolverMur
{
	class BranchInfo
	{
	public:
		BranchInfo();
		
		void AddFeatureBranch(uint32_t feature, bool present);
		uint32_t Size() const;
		uint32_t operator[](uint32_t i) const;

	private:
		uint32_t GetCode(uint32_t feature, bool present);
		void ConvertIntoCanonicalRepresentation();

		std::vector<uint32_t> branch_codes_;
	};
}