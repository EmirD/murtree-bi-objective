#pragma once

#include "node_selector_abstract.h"

namespace DecisionTreeSolverMur
{
	class NodeSelectorPostOrderFlex : public NodeSelectorAbstract
	{
	public:
		NodeSelectorPostOrderFlex(State * state, bool larger_misclassification_side_first);
		DecisionNode * GetNextNode();

	private:
		DecisionNode * NextNodeInPostOrderSearch(DecisionNode * node);
		bool larger_misclassification_side_first_;
	};

	NodeSelectorPostOrderFlex::NodeSelectorPostOrderFlex(State * state, bool larger_misclassification_side_first) :
		NodeSelectorAbstract(state),
		larger_misclassification_side_first_(larger_misclassification_side_first)
	{}

	DecisionNode * NodeSelectorPostOrderFlex::GetNextNode()
	{
		DecisionNode *next_node = NextNodeInPostOrderSearch(state_->GetRootNode());
		assert(next_node != NULL);
		return next_node;
	}

	DecisionNode * NodeSelectorPostOrderFlex::NextNodeInPostOrderSearch(DecisionNode * node)
	{
		assert(node != NULL && node->IsActivated());

		if (node->IsExhausted()) { return NULL; }

		if (state_->IsTerminalNode(node) || state_->decision_tree_.HasChildren(node) == false) { return node; }

		DecisionNode * left_child = state_->decision_tree_.GetLeftChild(node);
		DecisionNode * right_child = state_->decision_tree_.GetRightChild(node);
		

		if (larger_misclassification_side_first_)
		{
			if (left_child->GetData()->ComputeClassificationCost() > right_child->GetData()->ComputeClassificationCost())
			{
				DecisionNode * left_result = NextNodeInPostOrderSearch(left_child);
				if (left_result != NULL) { return left_result; }

				DecisionNode * right_result = NextNodeInPostOrderSearch(right_child);
				if (right_result != NULL) { return right_result; }
			}
			else
			{
				DecisionNode * right_result = NextNodeInPostOrderSearch(right_child);
				if (right_result != NULL) { return right_result; }

				DecisionNode * left_result = NextNodeInPostOrderSearch(left_child);
				if (left_result != NULL) { return left_result; }
			}
		}
		else
		{
			if (left_child->GetData()->ComputeClassificationCost() <= right_child->GetData()->ComputeClassificationCost())
			{
				DecisionNode * left_result = NextNodeInPostOrderSearch(left_child);
				if (left_result != NULL) { return left_result; }

				DecisionNode * right_result = NextNodeInPostOrderSearch(right_child);
				if (right_result != NULL) { return right_result; }
			}
			else
			{
				DecisionNode * right_result = NextNodeInPostOrderSearch(right_child);
				if (right_result != NULL) { return right_result; }

				DecisionNode * left_result = NextNodeInPostOrderSearch(left_child);
				if (left_result != NULL) { return left_result; }
			}
		}

		return node;
	}
}