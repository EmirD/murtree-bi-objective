#pragma once

#include "state.h"

namespace DecisionTreeSolverMur
{
	class NodeSelectorAbstract
	{
	public:
		NodeSelectorAbstract(State * state) :
			state_(state)
		{}
		virtual DecisionNode * GetNextNode() = 0;

	protected:
		uint32_t GetNodeType(DecisionNode * node)
		{
			//type1: nodes with no descendents
			//type2: terminal nodes
			//type3: others 

			if (state_->IsTerminalNode(node)) { return 2; }
			
			if (!state_->decision_tree_.HasChildren(node) ||
				state_->decision_tree_.GetLeftChild(node)->IsExhausted() && state_->decision_tree_.GetRightChild(node)->IsExhausted())
			{
				return 1;
			}

			//if (state_->DepthBelowNode(node) == 0) { return 1; }
			return 3;
		}

		State *state_;
	};
}