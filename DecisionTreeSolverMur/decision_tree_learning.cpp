#include "decision_tree_learning.h"
#include "solver.h"

#include <algorithm>
#include <time.h>

namespace DecisionTreeSolverMur
{
	DecisionTree * DecisionTreeLearning::LearnDecisionTree(LearningData & data, uint32_t max_depth, uint32_t max_size)
	{
		/*
		the algorithm for learning is as follows:

		Divide the input data into a training and validation set
		For each optimal tree that fits the specification (depth <= depth_max && size <= size_max)
			Compute the tree using the training data
			Evaluate the tree on the validation set
		Return the tree that performed best on the validation set
		*/

		//this is a hack for dividing the data into 80% training and 20% validation
		std::vector<TrainTestSplit> temp = GetKSplits(data, 5);

		LearningData training_set = temp[0].training_set;
		LearningData validation_set = temp[0].testing_set;

		Parameters parameters; //default parameters
		
		SolverParameters solver_params;
		solver_params.max_depth = 1;
		solver_params.max_size = 1;
		Solver solver(training_set, parameters);

		DecisionTree *best_tree = NULL;
		uint32_t best_validation_misclassification_score = UINT32_MAX;
		uint32_t best_training_misclassification_score = UINT32_MAX;

		for (uint32_t depth = 1; depth <= max_depth; depth++)
		{
			std::cout << "Depth: " << depth << std::endl;
			uint32_t size_limit_for_depth = (uint32_t(1) << depth) - 1; //remember bottom leaf nodes don't count towards the node limit
			for (uint32_t size = 1; size <= std::min(max_size, size_limit_for_depth); size++)
			{
				std::cout << "\tSize: " << size << "\n";
				solver_params.max_depth = depth;
				solver_params.max_size = size;
				solver_params.verbose = false;
				//solver_params.upper_bound_constraint = best_training_misclassification_score-1;

				Result result = solver.Solve(solver_params);

				if (result.decision_tree_ != NULL)
				{
					best_training_misclassification_score = std::min(best_training_misclassification_score, result.misclassifications);

					uint32_t recomputed_misclassifications = result.decision_tree_->Evaluate(training_set);
					if (recomputed_misclassifications != result.misclassifications || result.decision_tree_->NumFeatureNodes() > size)
					{
						std::cout << "Error: the resulting tree is not what we expect it to be\n";
						std::cout << "Misclas: " << result.misclassifications << " vs recomp: " << recomputed_misclassifications << "\n";
						std::cout << "Size: " << size << " vs recomp: " << result.decision_tree_->NumFeatureNodes() << "\n";
						system("pause");
					}

					uint32_t validation_set_misclassification = result.decision_tree_->Evaluate(validation_set);
					if (validation_set_misclassification < best_validation_misclassification_score)
					{
						best_validation_misclassification_score = validation_set_misclassification;
						delete best_tree;
						best_tree = result.decision_tree_;
					}

					std::cout << "train / validation: " << double(training_set.Size() - result.misclassifications)/training_set.Size() << " // " << double(validation_set.Size() - validation_set_misclassification)/validation_set.Size() << "\n";

				}

				if (best_training_misclassification_score == 0) { break; }
			}
			if (best_training_misclassification_score == 0) { break; }
		}
		assert(best_validation_misclassification_score != UINT32_MAX);
		return best_tree;
	}

	DecisionTree * DecisionTreeLearning::LearnDecisionTree_noValidation(LearningData & data, uint32_t max_depth, uint32_t max_size)
	{
		/*
		the algorithm for learning is as follows:

		Divide the input data into a training and validation set
		For each optimal tree that fits the specification (depth <= depth_max && size <= size_max)
		Compute the tree using the training data
		Evaluate the tree on the validation set
		Return the tree that performed best on the validation set
		*/

		LearningData &training_set = data;

		Parameters parameters; //default parameters

		SolverParameters solver_params;
		solver_params.max_depth = 1;
		solver_params.max_size = 1;
		Solver solver(training_set, parameters);

		DecisionTree *best_tree = NULL;
		uint32_t best_training_misclassification_score = UINT32_MAX;

		for (uint32_t depth = 1; depth <= max_depth; depth++)
		{
			std::cout << "Depth: " << depth << std::endl;
			uint32_t size_limit_for_depth = (uint32_t(1) << depth) - 1; //remember bottom leaf nodes don't count towards the node limit
			for (uint32_t size = 1; size <= std::min(max_size, size_limit_for_depth); size++)
			{
				std::cout << "\tSize: " << size << "\n";
				solver_params.max_depth = depth;
				solver_params.max_size = size;
				solver_params.verbose = false;
				//solver_params.upper_bound_constraint = best_training_misclassification_score-1;

				Result result = solver.Solve(solver_params);

				if (result.decision_tree_ != NULL)
				{
					//best_training_misclassification_score = std::min(best_training_misclassification_score, result.misclassifications);

					uint32_t recomputed_misclassifications = result.decision_tree_->Evaluate(training_set);
					if (recomputed_misclassifications != result.misclassifications || result.decision_tree_->NumFeatureNodes() > size)
					{
						std::cout << "Error: the resulting tree is not what we expect it to be\n";
						std::cout << "Misclas: " << result.misclassifications << " vs recomp: " << recomputed_misclassifications << "\n";
						std::cout << "Size: " << size << " vs recomp: " << result.decision_tree_->NumFeatureNodes() << "\n";
						system("pause");
					}

					if (result.misclassifications < best_training_misclassification_score)
					{
						best_training_misclassification_score = result.misclassifications;
						delete best_tree;
						best_tree = result.decision_tree_;
					}

					std::cout << "train : " << double(training_set.Size() - result.misclassifications) / training_set.Size() << "\n";
				}

				if (best_training_misclassification_score == 0) { break; }
			}
			if (best_training_misclassification_score == 0) { break; }
		}
		assert(best_training_misclassification_score != UINT32_MAX);
		return best_tree;
	}

	TrainTestResult DecisionTreeLearning::ComputeLearningAccuracy(TrainTestSplit & split, uint32_t max_depth, uint32_t max_size)
	{
		clock_t clock_start = clock(); //need clock instead of time to capture small runtimes
		DecisionTree *tree = LearnDecisionTree_noValidation(split.training_set, max_depth, max_size);
		double time_spent = double(clock() - clock_start)/CLOCKS_PER_SEC;

		uint32_t num_misclassification_train = tree->Evaluate(split.training_set);
		uint32_t num_misclassification_test = tree->Evaluate(split.testing_set);

		TrainTestResult result;
		result.training_accuracy = double(split.training_set.Size() - num_misclassification_train)/split.training_set.Size();
		result.testing_accuracy = double(split.testing_set.Size() - num_misclassification_test)/split.testing_set.Size();
		result.tree_size = tree->NumFeatureNodes(); //todo, need to check if this results the correct value
		result.tree_depth = tree->GetMaxDepth();
		result.training_time = time_spent;

		return result;
	}

	TrainTestResult DecisionTreeLearning::KFoldCrossValidation(LearningData & data, uint32_t max_depth, uint32_t max_size, uint32_t k)
	{
		std::vector<TrainTestSplit> splits = GetKSplits(data, k);
		std::vector<TrainTestResult> train_test_results;
		for (size_t i = 0; i < splits.size(); i++)
		{
			std::cout << "Fold " << i << "\n--------------\n";
			TrainTestSplit &split = splits[i];
			TrainTestResult result = ComputeLearningAccuracy(split, max_depth, max_size);
			train_test_results.push_back(result);
		}
		return ComputeAverageResult(train_test_results);
	}

	std::vector<TrainTestSplit> DecisionTreeLearning::GetKSplits(LearningData & data, uint32_t k)
	{
		//helpers for this method---------------------------------------------
		struct PairFeatureVectorLabel
		{
			PairFeatureVectorLabel(FeatureVectorBinary fv, uint32_t lab) :feature_vector(fv), label(lab) {}
			FeatureVectorBinary feature_vector;
			uint32_t label;
		};

		struct PairStartEndPosition
		{
			PairStartEndPosition(size_t s_p, size_t e_p) :start_pos(s_p), end_pos(e_p) {}
			size_t start_pos, end_pos;
		};

		//results the indicies for the start and end position in the instances that belong to the i-th part in a k-fold 
		auto GetStartEndPositionsForPartition = [](size_t num_instances, uint32_t partition_index, uint32_t num_partitions)->PairStartEndPosition
		{
			assert(num_partitions > 0);

			//each partition has round_down(num_instances/k), 
			//	except the last partition which might have additional elements if num_instances is not divisible by k
			//might be a problem for very small num of instances but okay
			uint32_t num_elem = num_instances / num_partitions;
			if (partition_index == (num_partitions - 1)) { num_elem += (num_instances % num_partitions); }
			
			size_t start = partition_index * (num_instances / num_partitions);
			size_t end = start + num_elem;
			
			return PairStartEndPosition(start, end);
		};

		auto AddInstances = [](LearningData &data, std::vector<PairFeatureVectorLabel> &instances, size_t start_pos, size_t end_pos)->void
		{
			for (size_t pos = start_pos; pos < end_pos; pos++) 	
			{
				data.AddFeatureVector(instances[pos].feature_vector, instances[pos].label);
			}
		};

		//method starts here---------------------------------------------
		std::vector<PairFeatureVectorLabel> instances;
		for (uint32_t label = 0; label < data.NumLabels(); label++) 
		{
			for(const FeatureVectorBinary &fv: data[label])
			{
				instances.push_back(PairFeatureVectorLabel(fv, label));
			}
		}

		std::random_shuffle(instances.begin(), instances.end()); //could consider setting a seed ...

		//the instances are divided into k parts
		//each split consists of 1 part for testing and k-1 parts for training
		std::vector<TrainTestSplit> splits;
		for (uint32_t i = 0; i < k; i++)
		{
			TrainTestSplit split(data.NumLabels(), data.NumFeatures());
			
			PairStartEndPosition testing_part = GetStartEndPositionsForPartition(instances.size(), i, k);
			AddInstances(split.testing_set, instances, testing_part.start_pos, testing_part.end_pos);

			//set the training part
			for (uint32_t j = 0; j < k; j++)
			{
				if (i == j) { continue; }

				PairStartEndPosition jth_partition = GetStartEndPositionsForPartition(instances.size(), j, k);
				AddInstances(split.training_set, instances, jth_partition.start_pos, jth_partition.end_pos);
			}
			assert(split.training_set.Size() + split.testing_set.Size() == data.Size());
			splits.push_back(split);
		}
		return splits;
	}

	TrainTestResult DecisionTreeLearning::ComputeAverageResult(std::vector<TrainTestResult>& results)
	{
		assert(!results.empty());

		TrainTestResult average;
		for (TrainTestResult result : results)
		{
			average.training_accuracy += result.training_accuracy;
			average.testing_accuracy += result.testing_accuracy;
			average.tree_size += result.tree_size;
			average.tree_depth += result.tree_depth;
			average.training_time += result.training_time;
		}

		average.training_accuracy /= results.size();
		average.testing_accuracy /= results.size();
		average.tree_size /= results.size();
		average.tree_depth /= results.size();
		average.training_time /= results.size();

		return average;
	}
}