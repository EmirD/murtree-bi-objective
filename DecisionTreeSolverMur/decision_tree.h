#pragma once

#include "../learning_data.h"
#include "parameters.h"
#include "decision_node.h"
#include "branch_info.h"

#include <vector>

namespace DecisionTreeSolverMur{

	class DecisionTree
	{
	public:
		DecisionTree(uint32_t num_labels, uint32_t num_features, uint32_t max_depth, const std::string &feature_ordering_strategy, const std::string &size_split_strategy);
		~DecisionTree();

		void Initialise(uint32_t max_depth);

		DecisionNode * GetRootNode();
		DecisionNode * SpawnRootNode();

		DecisionNode * GetParent(DecisionNode *node); //every node has a parent
		DecisionNode * GetLeftChild(DecisionNode *node); //not every node has a left/right child: which ones don't?
		DecisionNode * GetRightChild(DecisionNode *node);
		uint32_t GetNodeDepth(DecisionNode *node);
		uint32_t GetSubtreeMaxFeatureHeight(DecisionNode *node);
		BranchInfo GetBranchInfo(DecisionNode * node);

		bool IsFeatureForbiddenForNode(DecisionNode * node, uint32_t target_feature);
		bool IsRootNode(DecisionNode *node);

		void SetChildren(DecisionNode *childless_node, DecisionNode *left_child, DecisionNode *right_child); //not every node has a left/right child: which ones don't?
		void DeactivateDescendents(DecisionNode * node);
		void DeactivateRoot();

		DecisionNode *PopFreeNode();
		void PushFreeNode(DecisionNode *node);
		DecisionNode * ExtendByOneNode();

		bool HasChildren(DecisionNode *node) const;
		bool IsLeftChild(DecisionNode *node);

		void EvaluateBiObj(LearningData& data, int &num_false_positives, int &num_false_negatives);
		uint32_t Evaluate(LearningData &data);
		uint32_t Classify(const FeatureVectorBinary &fv);
		uint32_t NumFeatureNodes(); //note that this may be misleading, since children of terminal nodes are not explicitly given, unless the tree has been reconstructed fully
		uint32_t NumFeatureNodesHelper(DecisionNode *node);
		uint32_t GetMaxDepth();//note that this may be misleading in the same way as above
		uint32_t GetMaxDepthHelper(DecisionNode *node);

		uint32_t NumTotalNodesIncludingClassificationNodes(); //note that this may be misleading, since children of terminal nodes are not explicitly given, unless the tree has been reconstructed fully
		uint32_t NumTotalNodesIncludingClassificationNodesHelper(DecisionNode *node);

		std::string GetFeatureOrderingStrategy() const;
		std::string GetSizeSplitStrategy() const;
	//private:	
	
		std::vector<DecisionNode*> nodes_;
		std::vector<DecisionNode*> parent_, left_child_, right_child_;
		std::vector<DecisionNode*> free_nodes_;
		uint32_t next_index_;

		struct DecisionTreeParameters
		{
			DecisionTreeParameters(uint32_t num_labels, uint32_t num_features, const std::string &feature_ordering_strategy, const std::string &size_split_strategy):
				num_labels(num_labels),
				num_features(num_features),
				feature_ordering_strategy(feature_ordering_strategy),
				size_split_strategy(size_split_strategy)		
			{}

			uint32_t max_depth;

			const uint32_t num_labels;
			const uint32_t num_features;
			const std::string feature_ordering_strategy;
			const std::string size_split_strategy;
		} parameters_;		
	};
}