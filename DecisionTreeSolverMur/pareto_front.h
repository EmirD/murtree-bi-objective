#pragma once

#include <vector>
#include <iostream>
#include <algorithm>

#include "decision_node_assignment.h"

//x -> false positives
//y -> false negatives

namespace DecisionTreeSolverMur
{
    class ParetoFront
    {
    public:

        int length() const { return int(front.size()); }
        void clear() {
            front.clear();
        }

        DecisionNodeAssignment& operator[](int i) { return front[i]; }
        friend std::ostream& operator<<(std::ostream& out, ParetoFront& pf);

        void insert(DecisionNodeAssignment p) {

            if (length() == 0) {
                front.push_back(p);
                return;
            }

            int l = 0;
            int u = length() - 1;
            int m;

            while (l <= u) {
                m = (l + u) / 2;
                if (eq(p, front[m]))
                    return;
                if (lt(p, front[m]))
                    u = m - 1;
                else
                    l = m + 1;
            }
            /* at the end of loop u is position just less than p, l is next position and > p */
            /* if the new point is dominated dont add */
            if (u >= 0 && dom(front[u], p)) return;
            /* Move past dominated points */
            for (; l < length() && p.num_false_negatives <= front[l].num_false_negatives; l++);
#ifdef DEBUG2
            printf("p = (%d,%d), u = %d, l = %d\n", p.x, p.y, u, l);
#endif
            /* no points dominated if u+1 == l */
            if (u + 1 == l) {
                bool trigger = false;
                front.resize(length() + 1);
                for (int i = length() - 1; i > u; i--)
                {
                    if (i == 0) {
                        trigger = true; 
                        continue;
                    }
                    front[i] = front[i - 1];
                }

                runtime_assert(trigger == false || l == 0)
                front[l] = p;
                return;
            }
            /* 1 point dominated if u+2 == l, simply replace */
            if (u + 2 == l) {
                front[u + 1] = p;
                return;
            }
            front[u + 1] = p;
            for (int i = u + 2; i < length() && i < (length() + u - l + 2); i++)
                front[i] = front[i + l - u - 2];
            front.resize(length() + u - l + 2);// ->length += u - l + 2;        
            return;
        }

        void add_and_merge_simple(ParetoFront& f2, uint32_t feature, uint32_t node_budget_left, uint32_t node_budget_right) {
            DecisionNodeAssignment p;
            p.feature = feature;
            p.left_node_count = node_budget_left;
            p.right_node_count = node_budget_right;
            ParetoFront f3;
            for (int i = 0; i < length(); i++)
                for (int j = 0; j < f2.length(); j++) {
                    p.num_false_positives = front[i].num_false_positives + f2.front[j].num_false_positives;
                    p.num_false_negatives = front[i].num_false_negatives + f2.front[j].num_false_negatives;
                    f3.insert(p);
#ifdef DEBUG
                    print_front(*f3);
#endif
                }
            front = f3.front;
        }

        /* inserts items in frontier f2 into f1 adding offset offset, starting at position i1, returns next starting posn*/
        int in_place_pareto_merge_with_offset_and_start(ParetoFront& f1, ParetoFront& f2, DecisionNodeAssignment offset, int i1, uint32_t feature, uint32_t node_budget_left, uint32_t node_budget_right) {
#ifdef DEBUG2
            printf("i1 = %d, offset = (%d,%d) : ", i1, offset.x, offset.y);
            print_front(*f2);
#endif
            DecisionNodeAssignment p;
            p.feature = feature;
            p.left_node_count = node_budget_left;
            p.right_node_count = node_budget_right;

            int new_i1 = 0;
            int i2 = 0;
            if (f2.length() == 0) return i1;
            while (i1 < f1.length() && i2 < f2.length()) {
                add(f2.front[i2], offset, p);
                if (lt(f1.front[i1], p)) {
                    for (; i2 < f2.length() && dom(f1.front[i1], p); i2++) {
                        add(f2.front[i2 + 1], offset, p);
                    }
                    i1++;
                    if (!new_i1) new_i1 = i1;
                }
                else {
                    int s = i1;
                    for (; i1 < f1.length() && dom(p, f1.front[i1]); i1++);
#ifdef DEBUG2
                    printf("s = %d i1 = %d i2 = %d\n", s, i1, i2);
#endif
                    if (s == i1) {
                        f1.front.resize(f1.length() + 1);
                        for (int i = f1.length() - 1; i > i1; i--)
                            f1.front[i] = f1.front[i - 1];
                        f1.front[i1] = p;
                        i1++;
                    }
                    else if (s + 1 == i1) {
                        f1.front[s] = p;
                    }
                    else {
                        for (int i = i1; i < f1.length() && i < f1.length() + s + 1 - i1; i++) //todo check if this is ok
                            f1.front[s + 1 + (i - i1)] = f1.front[i];
                        f1.front[s] = p;
                        //f1->length += s + 1 - i1;
                        f1.front.resize(f1.length() + s + 1 - i1);
                        i1 = s + 1;
                    }
                    if (!new_i1) new_i1 = i1;
                    i2++;
                }
#ifdef DEBUG2
                print_front(*f1);
                /* printf("i1 (%d,%d) i2 (%d,%d) offset (%d,%d)\n",f1->front[i1].x,f1->front[i1].y,
                   f2->front[i2].x, f2->front[i2].y, offset.x, offset.y); */
#endif
            }
            while (i2 < f2.length()) {
                add(f2.front[i2], offset, p);
                i2++;
                f1.front.push_back(p);
            }
#ifdef DEBUG
            /* printf("PMO "); */
            print_front(*f1);
#endif
            return new_i1;
        }

        void add_and_merge_complex(ParetoFront& front2, uint32_t feature, uint32_t node_budget_left, uint32_t node_budget_right) {
            DecisionNodeAssignment p;
            p.feature = feature;
            p.left_node_count = node_budget_left;
            p.right_node_count = node_budget_right;

            ParetoFront* f1 = this;
            ParetoFront* f2 = &front2;

            if (f1->length() > f2->length()) { std::swap(f1, f2); }

            ParetoFront f3;
            int i1 = 0;
            /* add initial entries to f3 */
            for (int j = 0; j < f2->length(); j++) {
                add(f1->front[0], f2->front[j], p);
                f3.front.push_back(p);
            }
            for (int i = 1; i < f1->length(); i++) {
                i1 = in_place_pareto_merge_with_offset_and_start(f3, *f2, f1->front[i], i1, feature, node_budget_left, node_budget_right);
#ifdef DEBUG
                print_front(*f3);
#endif
            }
            this->front = f3.front;
        }

        /* remove dominated elements in a sorted list of pairs */
        void remove_dominated() {
            int l = 0;
            for (int i = 0; i < length(); i++) {
                DecisionNodeAssignment p = front[i];
                front[l++] = p;
                for (; i < length() && dom(p, front[i]); i++);
                i--;
            }
            front.resize(l);
        }

        void add_and_merge_sort(ParetoFront& front2, uint32_t feature, uint32_t node_budget_left, uint32_t node_budget_right) {
            DecisionNodeAssignment p;
            p.feature = feature;
            p.left_node_count = node_budget_left;
            p.right_node_count = node_budget_right;

            ParetoFront* f1 = this;
            ParetoFront* f2 = &front2;
            ParetoFront f3;

            for (int i = 0; i < f1->length(); i++)
                for (int j = 0; j < f2->length(); j++) {
                    add(f1->front[i], f2->front[j], p);
                    f3.front.push_back(p);
                }
            
            std::sort(
                f3.front.begin(),
                f3.front.end(),
                [](const DecisionNodeAssignment& p1, const DecisionNodeAssignment& p2)->bool
                {
                    return p1.num_false_positives < p2.num_false_positives;
                }
            );
            f3.remove_dominated();
            this->front = f3.front;
        }

    //private:

        bool lt(DecisionNodeAssignment & p1, DecisionNodeAssignment & p2) { return ((p1).num_false_positives < (p2).num_false_positives || (p1).num_false_positives == (p2).num_false_positives && (p1).num_false_negatives < (p2).num_false_negatives); }
        bool eq(DecisionNodeAssignment& p1, DecisionNodeAssignment& p2) { return ((p1).num_false_positives == (p2).num_false_positives && (p1).num_false_negatives == (p2).num_false_negatives); }
        bool dom(DecisionNodeAssignment& p1, DecisionNodeAssignment& p2) { return ((p1).num_false_positives <= (p2).num_false_positives && (p1).num_false_negatives <= (p2).num_false_negatives); }
        void add(DecisionNodeAssignment& p1, DecisionNodeAssignment& p2, DecisionNodeAssignment& p3) { (p3).num_false_positives = (p1).num_false_positives + (p2).num_false_positives; (p3).num_false_negatives = (p1).num_false_negatives + (p2).num_false_negatives; };

        std::vector<DecisionNodeAssignment> front;
    };
}