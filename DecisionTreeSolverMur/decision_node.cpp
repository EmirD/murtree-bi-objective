#include "decision_node.h"
#include "feature_selector_abstract.h"
#include "runtime_assert.h"

namespace DecisionTreeSolverMur
{
	DecisionNode::DecisionNode(uint32_t num_labels, uint32_t num_features) :
		data_(num_labels, num_features),
		node_index_(UINT32_MAX),
		is_activated_(false),
		is_exhausted_(false),
		feature_selector_(0),
		node_budget_selector_(0),
		hax_last_node_budget(0, 0)
	{}

	DecisionNode::~DecisionNode() { delete feature_selector_; delete node_budget_selector_; }

	void DecisionNode::Activate(uint32_t node_budget)
	{
		runtime_assert(!IsActivated());
		//runtime_assert(!data_.IsEmpty());
		runtime_assert(assignment_current_.IsNull());
		runtime_assert(assignments_best_.IsEmpty());
		runtime_assert(external_misclassification_budget_.IsEmpty());

		DecisionNodeAssignment empty_assignment;
		empty_assignment.num_false_positives = data_.NumInstancesForLabel(0);
		empty_assignment.num_false_negatives = 0;
		UpdateCurrentAssignment(empty_assignment);

		empty_assignment.num_false_positives = 0;
		empty_assignment.num_false_negatives = data_.NumInstancesForLabel(1);
		UpdateCurrentAssignment(empty_assignment);

		external_misclassification_budget_.Clear();
		is_activated_ = true;
		is_exhausted_ = false;
		feature_selector_->Reset(data_);
		node_budget_selector_->Reset(node_budget);

		hax_flag = false;
	}

	void DecisionNode::Deactivate()
	{
		DeactivateKeepData();
		data_.Clear();
	}

	void DecisionNode::DeactivateKeepData()
	{
		assert(IsActivated());
		assert(!data_.IsEmpty());

		assignment_current_.Nullify();
		assignments_best_.Clear();
		external_misclassification_budget_.Clear();
		is_activated_ = false;
		is_exhausted_ = false;

		hax_flag = false;
	}

	void DecisionNode::MarkExhausted()
	{
		assert(IsActivated() && !IsExhausted());
		is_exhausted_ = true;
	}

	void DecisionNode::Finalise(AssignmentContainer& optimal_assignments)
	{
		for (const DecisionNodeAssignment& assignment : optimal_assignments.GetStoredAssignments())
		{
			UpdateCurrentAssignment(assignment);
		}
		MarkExhausted();
		runtime_assert(optimal_assignments == assignments_best_);
	}

	void DecisionNode::SetFeatureSelector(FeatureSelectorAbstract * fs)
	{
		assert(feature_selector_ == NULL);
		feature_selector_ = fs;
	}

	void DecisionNode::SetNodeBudgetSelector(NodeBudgetSelectorAbstract * nbs)
	{
		assert(node_budget_selector_ == NULL);
		node_budget_selector_ = nbs;
	}

	void DecisionNode::AssignFeatureTemporary(uint32_t feature)
	{
		assignment_current_.feature = feature;
	}

	void DecisionNode::UpdateCurrentAssignment(const DecisionNodeAssignment &new_assignment)
	{
		if (!(node_budget_selector_->GetNodeBudget() >= new_assignment.NumNodes()))
		{
			std::cout << node_budget_selector_->GetNodeBudget() << "\n";
			std::cout << new_assignment.NumNodes();
		}


		runtime_assert(!new_assignment.IsNull());
		runtime_assert(node_budget_selector_->GetNodeBudget() >= new_assignment.NumNodes());
		
		//this is a bit hacky, todo
		//if we find a solution that is one above the misclassification budget, we can keep it
		//since if later we do not find a solution satisfying the misclassification budget
		//but we find a solution directly one above, we can use that as the optimal solution
		//>0 misclass is needed since otherwise the modified budget does not make sense, and this sol is anyway optimal
		//but we shouldn't take the new solution if it is not better than what we already have stored in the pruned assignment
		//otherwise we might store something with some misclassification cost but higher node count
		
		//todo need to rethink if this makes sense with the new assignment container
		/*if (new_assignment.Misclassifications() > 0 
			&& external_misclassification_budget_ != UINT32_MAX 
			&& GetModifiedMisclassificationBudget() + 1 == new_assignment.misclassifications && new_assignment.IsBetterThan(assignment_pruned_))
		{
			assignment_pruned_ = new_assignment;
		}*/

		UpdateBestAssignment(new_assignment);
		assignment_current_ = new_assignment;
	}

	void DecisionNode::UpdateBestAssignment(const DecisionNodeAssignment &suggested_assignment)
	{
		//assert(!HasFeasibleSolution() || assignment_current_.misclassifications >= assignments_best_.misclassifications);
		//assert(!HasFeasibleSolution() || assignments_best_.IsInputAssignmentDominated(assignment_current_));
		//not sure what these asserts were suppose to check?

		//if (IsAssignmentFeasible(suggested_assignment)) 
		{ 
			assignments_best_.AddAssignment(suggested_assignment);//note that it will only be added if it is better, and that best assigments do not necessarily need to be feasible
		}
	}

	void DecisionNode::UpdateBestAssignments(AssignmentContainer& candidate_assignments)
	{
		assignments_best_.MergeMinimisation(candidate_assignments);
	}

	void DecisionNode::UpdateExternalBudget(AssignmentContainer new_external_budget)
	{
		//todo enable the assert, it should be strictly dominating not just dominating
		//runtime_assert(new_external_budget.DoInputAssignmentsContainDominatingSolution(external_misclassification_budget_) == false);
		external_misclassification_budget_ = new_external_budget;
		//assignments_best_.RemoveAssignmentsDominatedByInput(external_misclassification_budget_); //not sure I need this anymore

		/*if (assignments_best_.IsEmpty())// !IsAssignmentFeasible(assignments_best_) //note that this checks feasibility with respect to the new budget
		{
			assignment_pruned_.MergeMinimisation(assignments_best_);
			if (assignment_pruned_.IsEmpty())
			{
				assignment_pruned_ = assignments_best_;
			}
			else if (assignments_best_.IsBetterThan(assignment_pruned_))
			{
				assignment_pruned_ = assignments_best_;
			}
			assignments_best_.Nullify(); 
		}*/
	}

	void DecisionNode::SetIndex(uint32_t index)
	{
		node_index_ = index;
	}

	uint32_t DecisionNode::Index() const
	{
		return node_index_;
	}

	DecisionNodeAssignment DecisionNode::GetCurrentAssignment() const
	{
		return assignment_current_;
	}

	const AssignmentContainer& DecisionNode::GetBestAssignments() const
	{
		return assignments_best_;
	}

	uint32_t DecisionNode::GetAssignedFeature() const
	{
		return assignment_current_.feature;
	}

	uint32_t DecisionNode::GetBestFeature() const
	{
		runtime_assert(1 == 2);
		return UINT32_MAX;
	}

	uint32_t DecisionNode::GetNumFalsePositives() const
	{
		return assignment_current_.num_false_positives;
	}

	uint32_t DecisionNode::GetNumFalseNegatives() const
	{
		return assignment_current_.num_false_negatives;
	}

	/*uint32_t DecisionNode::GetMisclassification() const
	{
		return assignment_current_.misclassifications;
	}*/

	/*AssignmentContainer DecisionNode::GetBestMisclassification() const
	{
		runtime_assert(HasFeasibleSolution());
		return assignments_best_;
	}*/

	const AssignmentContainer DecisionNode::GetModifiedMisclassificationBudget() const
	{
		AssignmentContainer temp(external_misclassification_budget_);
		temp.MergeMinimisation(assignments_best_);
		runtime_assert(temp.Size() > 0);
		return temp;

		//I think the code below is no longer used?
		if (assignments_best_.Size() > 0) { return assignments_best_; }
		else { return external_misclassification_budget_; }

		/*uint32_t budget = external_misclassification_budget_;
		if (HasFeasibleSolution())
		{			
			if (IsExhausted()) 
			{ 
				budget = std::min(budget, GetBestMisclassification()); 
			}
			else 
			{ 
				assert(GetBestMisclassification() > 0); 
				budget = std::min(budget, GetBestMisclassification() - 1); 
			}
		}
		return budget;*/
	}

	const AssignmentContainer& DecisionNode::GetExternalMisclassificationBudget() const
	{
		return external_misclassification_budget_;
	}

	FeatureSelectorAbstract * DecisionNode::GetFeatureSelector()
	{
		return feature_selector_;
	}

	NodeBudgetSelectorAbstract * DecisionNode::GetNodeBudgetSelector()
	{
		return node_budget_selector_;
	}

	LearningData * DecisionNode::GetData()
	{
		return &data_;
	}

	uint32_t DecisionNode::GetNodeBudget() const
	{
		assert(IsActivated());
		return node_budget_selector_->GetNodeBudget();
	}

	PairNodeBudget DecisionNode::GetChildrenNodeCounts() const
	{
		return PairNodeBudget(assignment_current_.left_node_count, assignment_current_.right_node_count);
	}

	uint32_t DecisionNode::GetLabel() const
	{
		runtime_assert(IsLabelNode());
		runtime_assert(hax_label_ == 0 || hax_label_ == 1);
		return hax_label_;
		return data_.GetMajorityLabel();
	}

	bool DecisionNode::IsActivated() const
	{
		//assert check other members too
		return is_activated_;
	}

	bool DecisionNode::IsExhausted() const
	{
		return is_exhausted_;
	}

	bool DecisionNode::HasFeasibleSolution() const
	{
		return assignments_best_.ContainsNonDominatedSolution(external_misclassification_budget_);
	}

	bool DecisionNode::AreAllSolutionsFeasible() const
	{
		for (auto& assignment : assignments_best_.GetStoredAssignments())
		{
			if (!IsAssignmentFeasible(assignment)) { return false; }
		}
		return true;
	}

	bool DecisionNode::AreAllSolutionsInfeasible() const
	{
		for (auto& assignment : assignments_best_.GetStoredAssignments())
		{
			if (IsAssignmentFeasible(assignment)) { return false; }
		}
		return true;
	}

	bool DecisionNode::AreThereAnyAssignmentsLeft() const
	{
		return node_budget_selector_->AreThereAnyOptionsLeft() || feature_selector_->AreThereAnyFeaturesLeft();
	}

	bool DecisionNode::AreThereAnyFeaturesLeft() const
	{
		 return feature_selector_->AreThereAnyFeaturesLeft();
	}

	bool DecisionNode::AreThereAnyNodeBudgetOptionsLeft() const
	{
		return node_budget_selector_->AreThereAnyOptionsLeft();
	}

	bool DecisionNode::IsAssignmentFeasible(const DecisionNodeAssignment & assignment) const
	{
		runtime_assert(assignment.IsNull() || assignment.NumNodes() <= node_budget_selector_->GetNodeBudget());//this is assumed to be true, i.e. we would never ask if an assignment that goes beyond the node limit is feasible
		return !assignment.IsNull() &&
			(external_misclassification_budget_.IsInputAssignmentDominated(assignment) == false
				|| external_misclassification_budget_.IsEquivalentAssignmentPresent(assignment));
	}

	bool DecisionNode::IsLabelNode() const
	{
		return assignment_current_.feature == UINT32_MAX;
	}

	bool DecisionNode::IsAssignedFeatureNull() const
	{
		return GetAssignedFeature() == UINT32_MAX;
	}

	bool DecisionNode::AreAllBestAssignmentsBorderline() const
	{
		return assignments_best_ == external_misclassification_budget_;

		for (auto assignment : assignments_best_.GetStoredAssignments())
		{
			if (external_misclassification_budget_.IsEquivalentAssignmentPresent(assignment) == false)
			{
				return false;
			}
		}
		return true;
	}

	bool DecisionNode::operator==(const DecisionNode & rhs) const
	{
		return this->node_index_ == rhs.node_index_;
	}	
}