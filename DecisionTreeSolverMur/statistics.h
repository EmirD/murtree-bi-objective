#pragma once

#include <iostream>
#include <stdint.h>

namespace DecisionTreeSolverMur
{
	struct Statistics
	{
		Statistics()
		{
			num_nodes_processed_terminal = 0;
			num_nodes_processed_nonterminal = 0;

			num_nodes_pruned_weighted = 0;
			num_nodes_pruned_children_bound = 0;
			num_nodes_pruned_sibling_bound = 0;
			num_nodes_pruned_sibling_bound_lb = 0;
			num_nodes_pruned_node_bound = 0;
			num_nodes_closed_trivially = 0;

			num_cache_hit_optimality = 0;
			num_cache_miss_optimality = 0;
			num_cache_hit_optimality_weighted = 0;
			num_cache_hit_nonzero_bound = 0;
		}

		void Print()
		{
			std::cout << "num_nodes_processed_terminal\n";
			std::cout << num_nodes_processed_terminal << "\n";
			std::cout << "num_nodes_processed_nonterminal\n";
			std::cout << num_nodes_processed_nonterminal << "\n";

			std::cout << "num_nodes_closed_trivially\n";
			std::cout << num_nodes_closed_trivially << "\n";

			std::cout << "num_nodes_pruned_weighted\n";
			std::cout << num_nodes_pruned_weighted << "\n";
			std::cout << "num_nodes_pruned_children_bound\n";
			std::cout << num_nodes_pruned_children_bound << "\n";
			std::cout << "num_nodes_pruned_sibling_bound\n";
			std::cout << num_nodes_pruned_sibling_bound << "\n";
			std::cout << "num_nodes_pruned_lower_bound\n";
			std::cout << num_nodes_pruned_sibling_bound_lb << "\n";
			std::cout << "num_nodes_pruned_node_bound\n";
			std::cout << num_nodes_pruned_node_bound << "\n";

			std::cout << "num_cache_hit\n";
			std::cout << num_cache_hit_optimality << "\n";
			std::cout << "num_cache_miss\n";
			std::cout << num_cache_miss_optimality << "\n";
			std::cout << "num_cache_hit_weighted\n";
			std::cout << num_cache_hit_optimality_weighted << "\n";
			std::cout << "num_cache_hit_nonzero_bound\n";
			std::cout << num_cache_hit_nonzero_bound << "\n";
		}


		uint64_t num_nodes_processed_terminal, num_nodes_processed_nonterminal;
		uint64_t num_nodes_pruned_weighted, num_nodes_pruned_children_bound;
		uint64_t num_nodes_pruned_sibling_bound, num_nodes_pruned_sibling_bound_lb;
		uint64_t num_nodes_pruned_node_bound;
		uint64_t num_nodes_closed, num_nodes_closed_weighted, num_nodes_closed_trivially;
		uint64_t num_cache_hit_optimality, num_cache_miss_optimality, num_cache_hit_optimality_weighted, num_cache_hit_nonzero_bound;
	};

}