#include "decision_tree.h"
#include "feature_selector_in_order.h"
#include "feature_selector_information_gain.h"
#include "feature_selector_misclassification_score.h"
#include "node_budget_selector_simple.h"
#include "node_budget_selector_balanced.h"
#include "node_budget_selector_progressively_increasing.h"

#include <assert.h>

namespace DecisionTreeSolverMur 
{
	DecisionTree::DecisionTree(uint32_t num_labels, uint32_t num_features, uint32_t max_depth, const std::string &feature_ordering_strategy, const std::string &size_split_strategy) :
		parameters_(num_labels, num_features, feature_ordering_strategy, size_split_strategy)
	{
		Initialise(max_depth);
	}

	DecisionTree::~DecisionTree()
	{
		for (DecisionNode *node : nodes_) { delete node; }
	}

	void DecisionTree::Initialise(uint32_t max_depth)
	{
		parameters_.max_depth = max_depth;		

		next_index_ = 0;
		free_nodes_.clear();
		nodes_.clear();
		parent_.clear();
		left_child_.clear();
		right_child_.clear();

		SpawnRootNode();
	}

	DecisionNode * DecisionTreeSolverMur::DecisionTree::GetRootNode()
	{
		assert(!nodes_.empty() && nodes_[0]->Index() == 0);
		return nodes_[0];
	}

	DecisionNode * DecisionTree::SpawnRootNode()
	{
		DecisionNode * root_node = PopFreeNode();
		assert(root_node->Index() == 0);
		return root_node;
	}

	DecisionNode * DecisionTree::GetParent(DecisionNode * node)
	{
		assert(parent_[node->Index()] != NULL);
		return parent_[node->Index()];
	}

	DecisionNode * DecisionTree::GetLeftChild(DecisionNode * node)
	{
		assert(left_child_[node->Index()] != NULL && right_child_[node->Index()] != NULL);
		return left_child_[node->Index()];
	}

	DecisionNode * DecisionTree::GetRightChild(DecisionNode * node)
	{
		assert(left_child_[node->Index()] != NULL && right_child_[node->Index()] != NULL);
		return right_child_[node->Index()];
	}

	uint32_t DecisionTree::GetNodeDepth(DecisionNode * node)
	{
		uint32_t level = 1;
		while (IsRootNode(node) == false)
		{
			level++;
			node = GetParent(node);
		}
		return level;
	}

	uint32_t DecisionTree::GetSubtreeMaxFeatureHeight(DecisionNode * node)
	{
		assert(parameters_.max_depth >= GetNodeDepth(node));
		return parameters_.max_depth - GetNodeDepth(node);
	}

	BranchInfo DecisionTree::GetBranchInfo(DecisionNode * node)
	{
		BranchInfo branch;
		while (IsRootNode(node) == false)
		{
			DecisionNode * parent = GetParent(node);
			uint32_t parent_feature = parent->GetAssignedFeature();
			bool is_present = (node == GetRightChild(parent)); //if the right child is 'node', then the parent feature is present in each instance in the data of 'node'
			branch.AddFeatureBranch(parent_feature, is_present);
			node = parent;
		}
		return branch;
	}

	bool DecisionTree::IsFeatureForbiddenForNode(DecisionNode * node, uint32_t target_feature)
	{
		while (IsRootNode(node) == false)
		{
			DecisionNode * parent = GetParent(node);
			uint32_t parent_feature = parent->GetAssignedFeature();
			if (parent_feature == target_feature) { return true; }
			node = parent;
		}
		return false;
	}

	bool DecisionTree::IsRootNode(DecisionNode * node)
	{
		return GetRootNode() == node;
	}

	void DecisionTree::DeactivateDescendents(DecisionNode * node)
	{
		if (HasChildren(node) == false) { return; }

		DecisionNode *previous_left_child = GetLeftChild(node);
		DecisionNode *previous_right_child = GetRightChild(node);

		left_child_[node->Index()] = NULL;
		right_child_[node->Index()] = NULL;

		parent_[previous_left_child->Index()] = NULL;
		parent_[previous_right_child->Index()] = NULL;

		previous_left_child->Deactivate();
		previous_right_child->Deactivate();

		DeactivateDescendents(previous_left_child);
		DeactivateDescendents(previous_right_child);

		PushFreeNode(previous_left_child);
		PushFreeNode(previous_right_child);				
	}

	void DecisionTree::DeactivateRoot()
	{
		DecisionNode *root = GetRootNode();
		if (root->IsActivated())
		{
			DeactivateDescendents(root);
			root->Deactivate();
			PushFreeNode(root);
		}
	}

	bool DecisionTree::HasChildren(DecisionNode * node) const
	{
		assert(node != NULL);
		assert(left_child_[node->Index()] != NULL && right_child_[node->Index()] != NULL || left_child_[node->Index()] == NULL && right_child_[node->Index()] == NULL);
		return left_child_[node->Index()] != NULL;
	}

	bool DecisionTree::IsLeftChild(DecisionNode * node)
	{
		if (node == GetRootNode()) { return false; }

		DecisionNode * parent = GetParent(node);
		return GetLeftChild(parent) == node;
	}

	void DecisionTree::EvaluateBiObj(LearningData& data, int& num_false_positives, int& num_false_negatives)
	{
		num_false_positives = 0;
		num_false_negatives = 0;
		for (uint32_t label = 0; label < 2; label++)
		{
			for (const FeatureVectorBinary& fv : data[label])
			{
				uint32_t predicted_label = Classify(fv);
				if (predicted_label != label)
				{
					num_false_positives += (predicted_label == 1);
					num_false_negatives += (predicted_label == 0);
				}
			}
		}
	}

	uint32_t DecisionTree::Evaluate(LearningData & data)
	{
		uint32_t misclassifications = 0;
		for (uint32_t label = 0; label < parameters_.num_labels; label++)
		{
			for(const FeatureVectorBinary &fv: data[label])
			{			
				uint32_t predicted_label = Classify(fv);
				misclassifications += (predicted_label != label);
			}
		}
		return misclassifications;
	}

	uint32_t DecisionTree::Classify(const FeatureVectorBinary &fv)
	{
		DecisionNode *node = GetRootNode();		
		while (!node->IsLabelNode())
		{			
			assert(HasChildren(node));
			if (fv[node->GetCurrentAssignment().feature])
			{
				node = GetRightChild(node);
			}
			else
			{
				node = GetLeftChild(node);
			}
		}
		return node->GetLabel();
	}

	uint32_t DecisionTree::NumFeatureNodes()
	{
		return NumFeatureNodesHelper(GetRootNode());
	}

	uint32_t DecisionTree::NumFeatureNodesHelper(DecisionNode * node)
	{
		if (node->IsLabelNode()) { return 0; }
		assert(HasChildren(node));
		return 1 + NumFeatureNodesHelper(GetLeftChild(node)) + NumFeatureNodesHelper(GetRightChild(node));
	}

	uint32_t DecisionTree::GetMaxDepth()
	{
		return GetMaxDepthHelper(GetRootNode());
	}

	uint32_t DecisionTree::GetMaxDepthHelper(DecisionNode * node)
	{
		if (node->IsLabelNode()) { return 0; }
		assert(HasChildren(node));
		return 1 + std::max(GetMaxDepthHelper(GetLeftChild(node)), GetMaxDepthHelper(GetRightChild(node)));
	}

	uint32_t DecisionTree::NumTotalNodesIncludingClassificationNodes()
	{
		return NumTotalNodesIncludingClassificationNodesHelper(GetRootNode());
	}

	uint32_t DecisionTree::NumTotalNodesIncludingClassificationNodesHelper(DecisionNode * node)
	{
		if (node->IsLabelNode()) { return 1; }
		assert(HasChildren(node));
		return 1 + NumTotalNodesIncludingClassificationNodesHelper(GetLeftChild(node)) + NumTotalNodesIncludingClassificationNodesHelper(GetRightChild(node));
	}

	std::string DecisionTree::GetFeatureOrderingStrategy() const
	{
		return parameters_.feature_ordering_strategy;
	}

	std::string DecisionTree::GetSizeSplitStrategy() const
	{
		return parameters_.size_split_strategy;
	}

	void DecisionTree::SetChildren(DecisionNode * childless_node, DecisionNode * left_child, DecisionNode * right_child)
	{
		//assert(!HasChildren(childless_node));
		//assert(parent_[left_child->Index()] == NULL);
		//assert(parent_[right_child->Index()] == NULL);

		left_child_[childless_node->Index()] = left_child;
		right_child_[childless_node->Index()] = right_child;

		parent_[left_child->Index()] = childless_node;
		parent_[right_child->Index()] = childless_node;
	}

	DecisionNode * DecisionTree::PopFreeNode()
	{
		if (free_nodes_.empty()) { free_nodes_.push_back(ExtendByOneNode()); }		
		DecisionNode *node = free_nodes_.back();
		free_nodes_.pop_back();
		return node;
	}

	void DecisionTree::PushFreeNode(DecisionNode * node)
	{
		assert(node->Index() < nodes_.size());
		assert(parent_[node->Index()] == NULL && left_child_[node->Index()] == NULL && right_child_[node->Index()] == NULL);

		free_nodes_.push_back(node);
	}

	DecisionNode * DecisionTree::ExtendByOneNode()
	{
		DecisionNode *new_node = new DecisionNode(parameters_.num_labels, parameters_.num_features);
		
		nodes_.push_back(new_node);
		parent_.push_back(NULL);
		left_child_.push_back(NULL);
		right_child_.push_back(NULL);
		
		new_node->SetIndex(next_index_);
		next_index_++;

		if (parameters_.feature_ordering_strategy == "in_order")
		{
			new_node->SetFeatureSelector(new FeatureSelectorInOrder(this, new_node, parameters_.num_features));
		}
		else if (parameters_.feature_ordering_strategy == "information_gain_ascending")
		{
			new_node->SetFeatureSelector(new FeatureSelectorInformationGain(this, new_node, parameters_.num_labels, parameters_.num_features, true));
		}
		else if (parameters_.feature_ordering_strategy == "information_gain_descending")
		{
			new_node->SetFeatureSelector(new FeatureSelectorInformationGain(this, new_node, parameters_.num_labels, parameters_.num_features, false));
		}
		else if (parameters_.feature_ordering_strategy == "misclassification_score_ascending")
		{
			new_node->SetFeatureSelector(new FeatureSelectorMisclassificationScore(this, new_node, parameters_.num_features, true));
		}
		else if (parameters_.feature_ordering_strategy == "misclassification_score_descending")
		{
			new_node->SetFeatureSelector(new FeatureSelectorMisclassificationScore(this, new_node, parameters_.num_features, false));
		}
		/*else if (parameters_.feature_ordering_strategy == "random")
		{
		nodes_[node_index].feature_selector_ = new FeatureSelectorRandom(*this, node_index, data.NumFeatures());
		}*/
		else
		{
			std::cout << "Unknown feature selection strategy: " << parameters_.feature_ordering_strategy << "\n";
			exit(1);
		}

		if (parameters_.size_split_strategy == "simple")
		{
			new_node->SetNodeBudgetSelector(new NodeBudgetSelectorSimple(this, new_node));
		}
		else if (parameters_.size_split_strategy == "balanced")
		{
			new_node->SetNodeBudgetSelector(new NodeBudgetSelectorBalanced(this, new_node));
		}
		else if (parameters_.size_split_strategy == "progressively_increasing")
		{
			new_node->SetNodeBudgetSelector(new NodeBudgetSelectorProgressivelyIncreasing(this, new_node));
		}
		else
		{
			std::cout << "Unknown node budget selection strategy: " << parameters_.size_split_strategy << "\n";
			exit(1);
		}

		return new_node;
	}
}
