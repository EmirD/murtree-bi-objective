#pragma once

#include <stdint.h>
#include <vector>

struct PairPositivesNegatives
{
	PairPositivesNegatives(uint32_t p, uint32_t n) :positives(p), negatives(n) {};
	uint32_t positives, negatives;
};

class SymmetricMatrixPositiveNegativeCounter3D
{
public:
	SymmetricMatrixPositiveNegativeCounter3D(uint32_t num_rows);
	
	uint32_t &Positives(uint32_t f1, uint32_t f2, uint32_t f3);
	uint32_t &Negatives(uint32_t f1, uint32_t f2, uint32_t f3);

	uint32_t &Positives(uint32_t f1, uint32_t f2);
	uint32_t &Negatives(uint32_t f1, uint32_t f2);

	uint32_t &Positives(uint32_t f);
	uint32_t &Negatives(uint32_t f);

	void ResetToZeros();

private:
	void SortInput(uint32_t &f1, uint32_t &f2, uint32_t &f3);
	void SortInput(uint32_t &f1, uint32_t &f2);

	uint32_t num_rows_;
	std::vector<std::vector<std::vector<PairPositivesNegatives> > > data3d_;
	std::vector<std::vector<PairPositivesNegatives> > data2d_;
	std::vector<PairPositivesNegatives> data1d_;
};