#include "feature_selector_abstract.h"
#include "decision_tree.h"

namespace DecisionTreeSolverMur
{
	FeatureSelectorAbstract::FeatureSelectorAbstract(DecisionTree * tree, DecisionNode * node, uint32_t num_features) :
		tree_(tree),
		node_(node),
		num_features_(num_features),
		num_features_popped_(0)
	{}

	FeatureSelectorAbstract::~FeatureSelectorAbstract()
	{
	}

	bool FeatureSelectorAbstract::AreThereAnyFeaturesLeft()
	{
		return (num_features_popped_ + tree_->GetNodeDepth(node_)) - 1 != num_features_;
	}

	void FeatureSelectorAbstract::Reset(LearningData &data)
	{
		num_features_popped_ = 0;
		ResetInternal(data);
	}

	uint32_t FeatureSelectorAbstract::PopNextFeature()
	{
		assert(AreThereAnyFeaturesLeft());

		uint32_t next_feature = PopNextFeatureInternal();
		while (tree_->IsFeatureForbiddenForNode(node_, next_feature))
		{
			assert(AreThereAnyFeaturesLeft());
			next_feature = PopNextFeatureInternal();
		}
		num_features_popped_++;
		return next_feature;
	}

}