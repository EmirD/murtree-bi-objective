#pragma once

#include "../learning_data.h"
#include "feature_selector_abstract.h"
#include "node_budget_selector_abstract.h"
#include "decision_node_assignment.h"
#include "assignment_container.h"

namespace DecisionTreeSolverMur
{
	class DecisionNode
	{
	public:
		DecisionNode(uint32_t num_labels, uint32_t num_features);
		~DecisionNode();

		void Activate(uint32_t node_budget);
		void Deactivate();
		void DeactivateKeepData();
		void MarkExhausted();
		void Finalise(AssignmentContainer &optimal_assignment);

		void SetFeatureSelector(FeatureSelectorAbstract *fs);
		void SetNodeBudgetSelector(NodeBudgetSelectorAbstract *nbs);
		void AssignFeatureTemporary(uint32_t feature); //should be removed but at the moment its used
		void UpdateCurrentAssignment(const DecisionNodeAssignment &new_assignment);
		void UpdateBestAssignment(const DecisionNodeAssignment &suggested_assignment);
		void UpdateBestAssignments(AssignmentContainer& candidate_assignments);
		void UpdateExternalBudget(AssignmentContainer new_external_budget);
		void SetIndex(uint32_t index);		
		
		uint32_t Index() const;
		DecisionNodeAssignment GetCurrentAssignment() const;
		const AssignmentContainer& GetBestAssignments() const;
		uint32_t GetAssignedFeature() const;
		uint32_t GetBestFeature() const;
		//uint32_t GetMisclassification() const;
		uint32_t GetNumFalsePositives() const;
		uint32_t GetNumFalseNegatives() const;
		//AssignmentContainer GetBestMisclassification() const;
		const AssignmentContainer GetModifiedMisclassificationBudget() const;
		const AssignmentContainer& GetExternalMisclassificationBudget() const;
		FeatureSelectorAbstract *GetFeatureSelector();
		NodeBudgetSelectorAbstract *GetNodeBudgetSelector();
		LearningData * GetData();
		uint32_t GetNodeBudget() const;
		PairNodeBudget GetChildrenNodeCounts() const;
		uint32_t GetLabel() const;

		bool IsActivated() const;
		bool IsExhausted() const;
		bool HasFeasibleSolution() const;
		bool AreAllSolutionsFeasible() const;
		bool AreAllSolutionsInfeasible() const;
		bool AreThereAnyAssignmentsLeft() const;
		bool AreThereAnyFeaturesLeft() const;
		bool AreThereAnyNodeBudgetOptionsLeft() const;
		bool IsAssignmentFeasible(const DecisionNodeAssignment &assignment) const;
		bool IsLabelNode() const;
		bool IsAssignedFeatureNull() const;
		bool AreAllBestAssignmentsBorderline() const;

		bool operator==(const DecisionNode &rhs) const;

		//AssignmentContainer assignment_pruned_;

	//private:		

		LearningData data_;

		uint32_t node_index_;

		DecisionNodeAssignment assignment_current_;
		AssignmentContainer assignments_best_;
		
		AssignmentContainer external_misclassification_budget_;

		bool is_activated_;
		bool is_exhausted_;

		FeatureSelectorAbstract *feature_selector_;
		NodeBudgetSelectorAbstract *node_budget_selector_;

		bool hax_flag;
		PairNodeBudget hax_last_node_budget;

		int hax_label_;
	};
}