#pragma once

#include "solver.h"

#include "../random_generator_decision_tree_data.h"
#include "../file_reader.h"
#include "optimal_decision_tree_constructor_depth3.h"

#include <string>
#include <vector>
#include <fstream>

namespace DecisionTreeSolverMur
{
	class Tester
	{
	public:

		void RunTests();
	};

	void Tester::RunTests()
	{
		std::cout << "Running tests!\n";

		std::vector<std::string> files;
		files.push_back("datasetCP4IM\\soybean-un.csv");
		files.push_back("datasetCP4IM\\lymph-un.csv");
		files.push_back("datasetCP4IM\\primary-tumor-un.csv");
		files.push_back("datasetCP4IM\\vote-un.csv");
		files.push_back("datasetCP4IM\\zoo-1-un.csv");
		files.push_back("datasetCP4IM\\splice-1-un.csv");
		files.push_back("datasetCP4IM\\anneal-un.csv");
		files.push_back("datasetCP4IM\\audiology-un.csv");
		files.push_back("datasetCP4IM\\mushroom-un.csv");
		files.push_back("datasetCP4IM\\australian-credit-un.csv");
		files.push_back("datasetCP4IM\\hypothyroid-un.csv");
		files.push_back("datasetCP4IM\\kr-vs-kp-un.csv");
		files.push_back("datasetCP4IM\\heart-cleveland-un.csv");

		//cout << "Running " << files.size() << " real benchmark tests three times each!\n";
		srand(2);
		for (uint32_t i = 0; i < 10000; i++)
		{
			if ((i + 1) % 1000 == 0) { std::cout << "i: " << i+1 << "\n"; }

			LearningData data = RandomGeneratorDecisionTreeData::GenerateRandomData(100, 2, 10);//18, 5 / 25, 6 //previous problematic case: 6 2 4
			//LearningData *data_pointer;
			//std::string file_location = "datasetsDL\\soybean.txt";
			//file_location = "datasetsDL\\anneal.txt";
			//file_location = "datasetsDL\\german-credit.txt";
			//if (file_location.find(".csv") != std::string::npos) { data_pointer = new LearningData(FileReader::ReadDataHao(file_location, false)); }
			//else { data_pointer = new LearningData(FileReader::ReadDataDL(file_location)); }
			//LearningData data = *data_pointer;

			//if (i != 3262) { continue; } 276

			Parameters params;
			//params.unit_tree_depth = 0;
			params.max_depth = 3;
			params.unit_tree_depth = 2;
			params.node_selection_strategy = "post_order";// "best_first";// "random";
			params.feature_ordering_strategy = "in_order";//"random";
			params.cache_technique = "bucketing_hash";
			params.use_lower_bound_caching = true;
			params.use_upper_bound_caching = true;
			params.use_pruning = true;
			params.branch_length_hash_bucketing = 5;// (params.target_depth - params.unit_tree_depth);
			params.preprocess_data = false;
			params.verbose = false;
			params.max_size = (uint32_t(1) << params.max_depth) - 1;

			SolverParameters solver_params;
			solver_params.max_depth = params.max_depth;
			solver_params.max_size = params.max_size;
			solver_params.verbose = false;

			Solver tree_builder(data, params);
			Result result = tree_builder.Solve(solver_params);
			uint32_t penalty = result.misclassifications;
			uint32_t reference_penalty = 0;
			
			AssignmentContainer sols;
			if (params.max_depth == 3)
			{
				/*OptimalDecisionTreeConstructorDepth3 decision_tree_three(data.NumFeatures());
				decision_tree_three.ComputeOptimalDecisionTree(data);
				reference_penalty = decision_tree_three.GetOptimalPenalty();*/
				DecisionNodeAssignment dummy(UINT32_MAX, 0, 0, data.Size() + 1, data.Size() + 1);
				sols.AddAssignment(dummy);
				SpecialisedOptimalBinaryClassificationTreeSolverDepth2 tree_solver(data.NumFeatures(), (i+1)*data.Size());
				for (int feature = 0; feature < data.NumFeatures(); feature++)
				{
					LearningData data_without_feature(2, data.NumFeatures()), data_with_feature(2, data.NumFeatures());
					data.SplitData(feature, data_without_feature, data_with_feature);
					AssignmentContainer left = tree_solver.Solve(data_without_feature, 3);
					AssignmentContainer right = tree_solver.Solve(data_with_feature, 3);
					AssignmentContainer solutions(left);
					solutions.CombineUsingAddition(right, feature, 1, 1);
					sols.AddAssignments(solutions);
				}
			}
			/*else if (params.max_depth == 4)
			{
				DecisionTreeComputerDepth4 decision_tree_four(data.NumFeatures());
				reference_penalty = decision_tree_four.ComputeOptimalDecisionTree(data);
			}*/
			else
			{
				std::cout << "kekw test wrong parameter\n";
				system("pause");
				exit(1);
			}

			if (sols != result.best_assignments)
			{
				std::cout << "PROBLEM!\n";
				std::cout << "i = " << i << std::endl;
				std::cout << result.best_assignments << "\nvs ref\n" << sols << std::endl;
				std::cout << data << "\n";

				//Solver wr(data, params);
				//uint32_t penalty = wr.Solve(solver_params).misclassifications;
				system("pause");
			}
			else
			{
			//	std::cout << "All gucci: " << sols << "\n";
			}
		}
	}
}