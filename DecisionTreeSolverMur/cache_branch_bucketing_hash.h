#pragma once

#include "branch_info.h"
#include "decision_node.h"
#include "cache_branch_abstract.h"
#include "assignment_container.h"
#include "runtime_assert.h"

#include <stdint.h>
#include <vector>
#include <unordered_map>
#include <assert.h>

namespace DecisionTreeSolverMur
{
	struct CacheEntry
	{
		CacheEntry(uint32_t node_budget):
			node_budget(node_budget)
		{
			DecisionNodeAssignment trivial_assignment_bound;
			trivial_assignment_bound.num_false_negatives = 0;
			trivial_assignment_bound.num_false_positives = 0;
			lower_bound.AddAssignment(trivial_assignment_bound);
		}
		
		const AssignmentContainer& GetOptimalSolutions() const
		{ 
			assert(IsOptimal()); 
			return optimal_assignments;
		}

		const AssignmentContainer &GetLowerBound() const { return lower_bound; }
				
		void SetOptimalAssignments(const AssignmentContainer &assignments)
		{ 
			runtime_assert(optimal_assignments.Size() == 0); 
			optimal_assignments = assignments;
			lower_bound = assignments;
		}

		void UpdateLowerBound(AssignmentContainer &lb)
		{
			assert(!IsOptimal());
			lower_bound.MergeMaximisation(lb);
		}

		bool IsOptimal() const { return optimal_assignments.Size() > 0; }

		uint32_t GetNodeBudget() const { return node_budget; }

	private:
		AssignmentContainer optimal_assignments;
		AssignmentContainer lower_bound;
		uint32_t node_budget;		
	};

	//adapted from https://stackoverflow.com/questions/20511347/a-good-hash-function-for-a-vector
	struct BranchInfoHashFunction
	{
		//todo check about overflows
		int operator()(BranchInfo const &branch_info) const
		{
			int seed = int(branch_info.Size());
			for (uint32_t i = 0; i < branch_info.Size(); i++)
			{
				uint32_t code = branch_info[i];
				seed ^= code + 0x9e3779b9 + (seed << 6) + (seed >> 2);
			}
			return seed;
		}
	};

	//assumes that both inputs are in canonical representation
	struct BranchInfoEquality
	{
		bool operator()(BranchInfo const &branch1, BranchInfo const &branch2) const //(std::vector<int> &literals1, std::vector<int> &literals2)
		{
			assert(branch1.Size() == branch2.Size());
			for (uint32_t i = 0; i < branch1.Size(); i++)
			{
				if (branch1[i] != branch2[i])
				{
					return false;
				}
			}
			return true;
		}
	};

	//key: a branch
	//value: cached value contains the optimal value and the lower bound
	class CacheBranchBucketingHash// : public CacheBranchAbstract
	{
	public:
		CacheBranchBucketingHash(uint32_t max_branch_length);

		bool IsOptimalAssignmentCached(const BranchInfo &branch, uint32_t node_budget);
		void StoreOptimalBranchAssignments(const BranchInfo& branch, const AssignmentContainer& optimal_assignments, uint32_t total_node_budget);
		const AssignmentContainer& RetrieveOptimalAssignment(const BranchInfo &branch, uint32_t node_budget);

		void StoreBranchLowerBound(const BranchInfo &branch, AssignmentContainer& lower_bound, uint32_t node_budget);
		AssignmentContainer RetrieveLowerBound(const BranchInfo &branch, uint32_t node_budget);

		uint32_t RetrieveUpperBound(const BranchInfo &branch, uint32_t node_budget);

		uint32_t NumEntries() const;

		void ClearLargeBranches(uint32_t num_nodes);//removes branches with num_nodes nodes or more. Used for incremental solving, when going from depth k to k+1, all branches of size k and greater must be removed

		void DisableUpperBounding();
		void DisableLowerBounding();

	private:
		std::vector<
			std::unordered_map<BranchInfo, std::vector<CacheEntry>, BranchInfoHashFunction, BranchInfoEquality >
		> cache_; //cache_[i] is a hash table with branches of size i

		bool use_upper_bound_caching_;
		bool use_lower_bound_caching_;
	};

}