#pragma once

#include "decision_node.h"

namespace DecisionTreeSolverMur
{

	class State;
	/*struct SolutionNode
	{
		SolutionNode() :feature(UINT32_MAX), nodes_left(0), nodes_right(0), total_penalty(0), feature_left_child(UINT32_MAX), feature_right_child(UINT32_MAX) {}
		uint32_t feature;
		uint32_t nodes_left;
		uint32_t nodes_right;
		uint32_t total_penalty;

		uint32_t feature_left_child, feature_right_child;
	};*/

	class SpecialisedOptimalDecisionTreeSolverAbstract
	{
	public:
		virtual DecisionNodeAssignment Solve(LearningData &data, uint32_t max_node_budget) = 0;
		virtual void SolveFull(DecisionNode *node, State *state, DecisionTree *tree) = 0;
		virtual bool IsTerminalNode(DecisionNode *node, State *state) = 0;
		virtual int ProbeDifference(LearningData &data) = 0;

		/*
		void ComputeOptimalDecisionTree(LearningData &data);
		OptimalDecisionTreeConstructorDepth2Solution ComputeOptimalDecisionTreeNodeBudget(LearningData &data, uint32_t max_node_budget);

		uint32_t GetOptimalPenalty() const;
		uint32_t GetOptimalFeatureForRoot() const;
		*/

		//depth 1 solver
		/*

			void ComputeOptimalDecisionTree(LearningData &data);
			uint32_t GetOptimalPenalty() const;
			uint32_t GetOptimalFeature() const;
			TreeDepth1 GetTreeInfoForRootFeature(uint32_t feature);
		*/
	};

}