#pragma once

#include <vector>

//a class merely for convenience, so that I can index vectors using my data types directly
template <class ObjectType, class DataType>
class VectorObjectIndexed
{
public:
	VectorObjectIndexed(size_t num_entries = 0, const DataType &initial_values = DataType()) :vector_(num_entries, initial_values) {}
	void push_back(const DataType &val) { vector_.push_back(val); }

	const DataType& operator[](const ObjectType &obj) const
	{
		return vector_[obj.Index()];
	}

	DataType& operator[](const ObjectType &obj)
	{
		return vector_[obj.Index()];
	}

	uint32_t size() const { return uint32_t(vector_.size()); }
	void resize(uint32_t new_size) { vector_.resize(new_size); }

	typename std::vector<DataType>::const_iterator begin() const { return vector_.begin(); }
	typename std::vector<DataType>::const_iterator end() const { return vector_.end(); }

private:
	std::vector<DataType> vector_;
};