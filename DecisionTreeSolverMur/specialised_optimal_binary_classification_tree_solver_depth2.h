#pragma once

#include "specialised_optimal_decision_tree_solver_abstract.h"
#include "penalty_branch_computer_binary_depth2.h"
#include "..//biobjective_classification_tree_constructor_depth1.h"
#include "assignment_container.h"

namespace DecisionTreeSolverMur
{
	class SpecialisedOptimalBinaryClassificationTreeSolverDepth2/* : public SpecialisedOptimalDecisionTreeSolverAbstract*/
	{
	public:
		SpecialisedOptimalBinaryClassificationTreeSolverDepth2(size_t num_features, uint32_t num_total_data_points = 0); //todo make it mandatory not an optional parameter?
		
		AssignmentContainer Solve(LearningData &data, uint32_t max_node_budget);
		void SolveFull(DecisionNode *node, State *state, DecisionTree *tree);

		bool IsTerminalNode(DecisionNode *node, State *state);
		int ProbeDifference(LearningData &data);

		BiobjectiveClassificationTreeConstructorDepth1 tree_solver_depth1_;

		//DecisionNodeAssignment optimal_at_most_one_node, optimal_at_most_two_nodes;
		AssignmentContainer optimal_at_most_three_nodes, optimal_at_most_two_nodes;

	private:

		bool CheckPerfectLeftChild(int feature, DecisionNodeAssignment& assignment, LearningData &data);

		struct ChildrenInformation
		{
			AssignmentContainer left_child_assignments, right_child_assignments;
		};

		struct BestPenalties
		{
			BestPenalties() :zero_nodes(UINT32_MAX), one_node(UINT32_MAX), two_nodes(UINT32_MAX), three_nodes(UINT32_MAX) {}
			uint32_t zero_nodes;
			uint32_t one_node;
			uint32_t two_nodes;
			uint32_t three_nodes;
		};

		double GetFeatureSimiliarity(uint32_t f1, uint32_t f2, LearningData &data);
		bool AreFeaturesEquivalent(uint32_t f1, uint32_t f2);

		void InitialiseDataStructures(LearningData &data);
		void InitialiseChildrenInfo();

		void UpdateBestLeftChild(uint32_t root_feature, DecisionNodeAssignment &left_child_assignment);
		void UpdateBestRightChild(uint32_t root_feature, DecisionNodeAssignment& right_child_assignment);
		void UpdateBestTwoNodeAssignment(uint32_t root_feature);
		void UpdateBestThreeNodeAssignment(uint32_t root_feature);

		uint32_t num_features_;
		PenaltyBranchComputerBinaryDepth2 penalty_computer_;
		std::vector<ChildrenInformation> best_children_info_;
		
//		friend OptimalDecisionTreeConstructorDepth2Tester;
	};
}