#pragma once

#include "..\\learning_data.h"
#include "assignment_container.h"

#include <stdint.h>
#include <vector>

namespace DecisionTreeSolverMur
{
	class LowerBoundComputer
	{
	public:
		LowerBoundComputer(uint32_t max_depth, uint32_t size, uint32_t num_instances);

		AssignmentContainer ComputeLowerBound(LearningData &data, uint32_t depth, uint32_t size);
		void UpdateArchive(LearningData &data, const AssignmentContainer &lower_bound, uint32_t depth, uint32_t size);
		void Initialise(uint32_t max_depth, uint32_t size);

		void Disable();

	private:

		struct ArchieveEntry
		{
			ArchieveEntry(LearningData &data, AssignmentContainer optimal_misclas) :
				data(data), lower_bound(optimal_misclas) {}

			LearningData data;
			AssignmentContainer lower_bound;
		};

		struct SimiliarityMetrics
		{
			uint32_t num_removals, total_difference;
			uint32_t num_positive_class_removals, num_negative_class_removals;
		};

		bool IsThereSpaceForNewData(uint32_t depth, uint32_t size);
		ArchieveEntry& GetMostSimilarStoredData(LearningData &data, uint32_t depth, uint32_t size);
		AssignmentContainer ComputeLowerBoundBasedOnArchieveEntry(ArchieveEntry & entry, LearningData & data);

		SimiliarityMetrics ComputeSimiliarityMetrics(LearningData &data_old, LearningData &data_new);

		std::vector<bool> seen_old_, seen_new_;
		std::vector<std::vector<std::vector<ArchieveEntry> > > archive_; //archieve[depth][num_nodes][i] 
		bool disabled_;
	};
}