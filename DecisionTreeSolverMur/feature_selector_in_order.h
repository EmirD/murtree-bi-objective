#pragma once

#include "feature_selector_abstract.h"

namespace DecisionTreeSolverMur
{
	class FeatureSelectorInOrder : public FeatureSelectorAbstract
	{
	public:
		FeatureSelectorInOrder(DecisionTree * tree, DecisionNode * node, uint32_t num_features) :
			FeatureSelectorAbstract(tree, node, num_features),
			next_(0)
		{
			assert(num_features_ > 0);
		}

	protected:

		uint32_t PopNextFeatureInternal()
		{
			return next_++;
		}

		void ResetInternal(LearningData &data)
		{
			next_ = 0;
		}

		uint32_t next_;
	};

}