#pragma once

#include <stdint.h>
#include <assert.h>
#include <algorithm>

namespace DecisionTreeSolverMur
{

	class DecisionTree;
	class DecisionNode;

	struct PairNodeBudget
	{
		PairNodeBudget(uint32_t left_budget, uint32_t right_budget) :
			left_node_budget(left_budget),
			right_node_budget(right_budget)
		{}

		uint32_t left_node_budget, right_node_budget;
	};

	class NodeBudgetSelectorAbstract
	{
	public:
		NodeBudgetSelectorAbstract(DecisionTree * tree, DecisionNode * node);

		virtual ~NodeBudgetSelectorAbstract();

		virtual PairNodeBudget PopNextBudgetSplit() = 0;
		virtual void Reset(uint32_t new_node_budget) = 0;
		virtual bool AreThereAnyOptionsLeft() const = 0;

		uint32_t GetNodeBudget() const;

		//this is used as a workaround and will be replaced in the future
		void SetNodeBudget_TemporaryHack(uint32_t new_budget)
		{
			assert(new_budget == 2 || new_budget == 3);
			allowed_node_budget_ = new_budget;
		}

		uint32_t MaxNumNodesInChildSubTree();

		uint32_t allowed_node_budget_;
		DecisionNode *node_;
		DecisionTree *tree_;
	};
}