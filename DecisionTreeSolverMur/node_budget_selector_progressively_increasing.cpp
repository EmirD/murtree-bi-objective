#include "node_budget_selector_progressively_increasing.h"
#include "decision_node.h"

namespace DecisionTreeSolverMur
{
	DecisionTreeSolverMur::NodeBudgetSelectorProgressivelyIncreasing::NodeBudgetSelectorProgressivelyIncreasing(DecisionTree * tree, DecisionNode * node):
		NodeBudgetSelectorAbstract(tree, node)
	{}

	PairNodeBudget DecisionTreeSolverMur::NodeBudgetSelectorProgressivelyIncreasing::PopNextBudgetSplit()
	{
		PairNodeBudget next_option = possible_budget_splits_.back();
		possible_budget_splits_.pop_back();
		return next_option;
	}

	void DecisionTreeSolverMur::NodeBudgetSelectorProgressivelyIncreasing::Reset(uint32_t new_node_budget)
	{
		allowed_node_budget_ = new_node_budget;

		InitialiseOptions();
	}

	bool DecisionTreeSolverMur::NodeBudgetSelectorProgressivelyIncreasing::AreThereAnyOptionsLeft() const
	{
		return possible_budget_splits_.empty() == false;
	}

	void NodeBudgetSelectorProgressivelyIncreasing::InitialiseOptions()
	{
		if (allowed_node_budget_ == 0) { return; }

		possible_budget_splits_.clear();

		uint32_t allowed_hax = allowed_node_budget_;

		for (uint32_t i = 1; i <= allowed_hax; i++)
		{
			allowed_node_budget_ = i;
			uint32_t max_nodes_in_subtree = MaxNumNodesInChildSubTree();
			uint32_t min_nodes_in_subtree = allowed_node_budget_ - 1 - max_nodes_in_subtree; //minus one since we have to take into account this node
			for (uint32_t num_left_subtree = min_nodes_in_subtree; num_left_subtree <= max_nodes_in_subtree; num_left_subtree++)
			{
				possible_budget_splits_.push_back(PairNodeBudget(num_left_subtree, allowed_node_budget_ - 1 - num_left_subtree)); //the minus one to take into account the root node
			}
		}
		allowed_node_budget_ = allowed_hax;

		std::vector<PairNodeBudget> temp;
		for (auto i = possible_budget_splits_.rbegin(); i != possible_budget_splits_.rend(); ++i)
		{
			temp.push_back(*i);
		}
		possible_budget_splits_ = temp;

		return; //not sure why but there is some issue with the code below?
		
		std::sort(
			possible_budget_splits_.begin(),
			possible_budget_splits_.end(),
			[](const PairNodeBudget &p1, const PairNodeBudget &p2)
			{
			return true;
				if (p1.left_node_budget + p1.right_node_budget < p2.left_node_budget + p2.right_node_budget) 
				{
					return false;
				}

				if (p1.left_node_budget + p1.right_node_budget > p2.left_node_budget + p2.right_node_budget)
				{
					return true;
				}

				return abs(int(p1.left_node_budget) - int(p1.right_node_budget)) >= abs(int(p2.left_node_budget) - int(p2.right_node_budget));
			}
		);
	}

}