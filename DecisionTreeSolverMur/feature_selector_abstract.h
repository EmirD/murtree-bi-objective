#pragma once

#include "../learning_data.h"
#include <stdint.h>

namespace DecisionTreeSolverMur
{
	class DecisionTree;
	class DecisionNode;

	class FeatureSelectorAbstract
	{
	public:
		FeatureSelectorAbstract(DecisionTree * tree, DecisionNode * node, uint32_t num_features);
		virtual ~FeatureSelectorAbstract();
		
		void Reset(LearningData &data);
		uint32_t PopNextFeature();
		bool AreThereAnyFeaturesLeft();

	protected:
		virtual uint32_t PopNextFeatureInternal() = 0;
		virtual void ResetInternal(LearningData &data) = 0;

		DecisionTree *tree_;
		DecisionNode * node_;
		uint32_t num_features_;

		uint32_t num_features_popped_;
	};

}