#include "node_budget_selector_simple.h"

namespace DecisionTreeSolverMur
{

	NodeBudgetSelectorSimple::NodeBudgetSelectorSimple(DecisionTree * tree, DecisionNode * node) :
		NodeBudgetSelectorAbstract(tree, node)
	{
	}

	NodeBudgetSelectorSimple::~NodeBudgetSelectorSimple()
	{
	}

	PairNodeBudget NodeBudgetSelectorSimple::PopNextBudgetSplit()
	{
		PairNodeBudget next_option = possible_budget_splits_.back();
		possible_budget_splits_.pop_back();
		return next_option;
	}

	void NodeBudgetSelectorSimple::Reset(uint32_t new_node_budget)
	{
		allowed_node_budget_ = new_node_budget;

		InitialiseOptions();
	}

	bool NodeBudgetSelectorSimple::AreThereAnyOptionsLeft() const
	{
		return possible_budget_splits_.empty() == false;
	}

	void NodeBudgetSelectorSimple::InitialiseOptions()
	{
		if (allowed_node_budget_ == 0) { return; }

		possible_budget_splits_.clear();

		uint32_t max_nodes_in_subtree = MaxNumNodesInChildSubTree();
		uint32_t min_nodes_in_subtree = allowed_node_budget_ - 1 - max_nodes_in_subtree; //minus one since we have to take into account this node
		assert(max_nodes_in_subtree >= min_nodes_in_subtree);
		for (uint32_t num_left_subtree = min_nodes_in_subtree; num_left_subtree <= max_nodes_in_subtree; num_left_subtree++)
		{
			possible_budget_splits_.push_back(PairNodeBudget(num_left_subtree, allowed_node_budget_ - 1 - num_left_subtree)); //the minus one to take into account the root node
		}
	}

}