#include "symmetric_matrix_positive_negative_counter_3d.h"

#include <assert.h>

SymmetricMatrixPositiveNegativeCounter3D::SymmetricMatrixPositiveNegativeCounter3D(uint32_t num_rows):
	num_rows_(num_rows)
{
	ResetToZeros();
}

uint32_t & SymmetricMatrixPositiveNegativeCounter3D::Positives(uint32_t f1, uint32_t f2, uint32_t f3)
{
	SortInput(f1, f2, f3);
	return data3d_[f1][f2][f3].positives;
}

uint32_t & SymmetricMatrixPositiveNegativeCounter3D::Negatives(uint32_t f1, uint32_t f2, uint32_t f3)
{
	SortInput(f1, f2, f3);
	return data3d_[f1][f2][f3].negatives;
}

uint32_t & SymmetricMatrixPositiveNegativeCounter3D::Positives(uint32_t f1, uint32_t f2)
{
	SortInput(f1, f2);
	return data2d_[f1][f2].positives;
}

uint32_t & SymmetricMatrixPositiveNegativeCounter3D::Negatives(uint32_t f1, uint32_t f2)
{
	SortInput(f1, f2);
	return data2d_[f1][f2].negatives;
}

uint32_t & SymmetricMatrixPositiveNegativeCounter3D::Positives(uint32_t f)
{
	return data1d_[f].positives;
}

uint32_t & SymmetricMatrixPositiveNegativeCounter3D::Negatives(uint32_t f)
{
	return data1d_[f].negatives;
}

void SymmetricMatrixPositiveNegativeCounter3D::ResetToZeros()
{
	data3d_ = std::vector<std::vector<std::vector<PairPositivesNegatives> > >(num_rows_, std::vector<std::vector<PairPositivesNegatives> >(num_rows_, std::vector<PairPositivesNegatives>(num_rows_, PairPositivesNegatives(0, 0))));
	data2d_ = std::vector<std::vector<PairPositivesNegatives> >(num_rows_, std::vector<PairPositivesNegatives>(num_rows_, PairPositivesNegatives(0, 0)));
	data1d_ = std::vector<PairPositivesNegatives>(num_rows_, PairPositivesNegatives(0, 0));
}

void SymmetricMatrixPositiveNegativeCounter3D::SortInput(uint32_t & f1, uint32_t & f2, uint32_t & f3)
{
	if (f1 > f2) { std::swap(f1, f2); }
	if (f2 > f3) { std::swap(f2, f3); }
	if (f1 > f2) { std::swap(f1, f2); }
	assert(f1 < f2 && f2 < f3);
}

void SymmetricMatrixPositiveNegativeCounter3D::SortInput(uint32_t & f1, uint32_t & f2)
{
	if (f1 > f2) { std::swap(f1, f2); }
}
