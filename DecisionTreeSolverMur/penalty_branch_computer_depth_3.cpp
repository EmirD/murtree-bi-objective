#include "penalty_branch_computer_depth_3.h"

namespace DecisionTreeSolverMur
{
	PenaltyBranchComputerDepth3::PenaltyBranchComputerDepth3(uint32_t num_features) :
		counts_(num_features)
	{}

	void PenaltyBranchComputerDepth3::Initialise(LearningData & data)
	{
		assert(data.NumLabels() == 2);
		counts_.ResetToZeros();
		num_positives_ = data.NumInstancesForLabel(1);
		num_negatives_ = data.NumInstancesForLabel(0);

		for (FeatureVectorBinary positive_data_point : data[1])
		{
			uint32_t num_present_features = positive_data_point.NumPresentFeatures();
			for (size_t i = 0; i < num_present_features; i++)
			{
				size_t f1 = positive_data_point.GetJthPresentFeature(i);
				counts_.Positives(f1)++;
				for (size_t j = i + 1; j < num_present_features; j++)
				{
					size_t f2 = positive_data_point.GetJthPresentFeature(j);
					assert(f1 < f2);
					counts_.Positives(f1, f2)++;
					for (size_t k = j + 1; k < num_present_features; k++)
					{
						size_t f3 = positive_data_point.GetJthPresentFeature(k);
						assert(f2 < f3);
						counts_.Positives(f1, f2, f3)++;
					}
				}
			}
		}

		for (FeatureVectorBinary negative_data_point : data[0])
		{
			uint32_t num_present_features = negative_data_point.NumPresentFeatures();
			for (size_t i = 0; i < num_present_features; i++)
			{
				size_t f1 = negative_data_point.GetJthPresentFeature(i);
				counts_.Negatives(f1)++;
				for (size_t j = i + 1; j < num_present_features; j++)
				{
					size_t f2 = negative_data_point.GetJthPresentFeature(j);
					assert(f1 <= f2);
					counts_.Negatives(f1, f2)++;
					for (size_t k = j + 1; k < num_present_features; k++)
					{
						size_t f3 = negative_data_point.GetJthPresentFeature(k);
						assert(f2 < f3);
						counts_.Negatives(f1, f2, f3)++;
					}
				}
			}
		}

	}

	uint32_t PenaltyBranchComputerDepth3::PenaltyBranchOneOneOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return std::min(PositivesOneOneOne(f1, f2, f3), NegativesOneOneOne(f1, f2, f3));
	}

	uint32_t PenaltyBranchComputerDepth3::PenaltyBranchOneOneZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return std::min(PositivesOneOneZero(f1, f2, f3), NegativesOneOneZero(f1, f2, f3));
	}

	uint32_t PenaltyBranchComputerDepth3::PenaltyBranchOneZeroOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return std::min(PositivesOneZeroOne(f1, f2, f3), NegativesOneZeroOne(f1, f2, f3));
	}

	uint32_t PenaltyBranchComputerDepth3::PenaltyBranchOneZeroZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return std::min(PositivesOneZeroZero(f1, f2, f3), NegativesOneZeroZero(f1, f2, f3));
	}

	uint32_t PenaltyBranchComputerDepth3::PenaltyBranchZeroOneOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return std::min(PositivesZeroOneOne(f1, f2, f3), NegativesZeroOneOne(f1, f2, f3));
	}

	uint32_t PenaltyBranchComputerDepth3::PenaltyBranchZeroOneZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return std::min(PositivesZeroOneZero(f1, f2, f3), NegativesZeroOneZero(f1, f2, f3));
	}

	uint32_t PenaltyBranchComputerDepth3::PenaltyBranchZeroZeroOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return std::min(PositivesZeroZeroOne(f1, f2, f3), NegativesZeroZeroOne(f1, f2, f3));
	}

	uint32_t PenaltyBranchComputerDepth3::PenaltyBranchZeroZeroZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		uint32_t a = PositivesZeroZeroZero(f1, f2, f3);
		uint32_t b = NegativesZeroZeroZero(f1, f2, f3);
		return std::min(a, b);
	}

	//-----

	uint32_t PenaltyBranchComputerDepth3::PositivesOneOneOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return counts_.Positives(f1, f2, f3);
	}

	uint32_t PenaltyBranchComputerDepth3::PositivesOneOneZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return PositivesOneOne(f1, f2) - PositivesOneOneOne(f1, f2, f3);
	}

	uint32_t PenaltyBranchComputerDepth3::PositivesOneZeroOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return PositivesOneOneZero(f1, f3, f2);
	}

	uint32_t PenaltyBranchComputerDepth3::PositivesOneZeroZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		uint32_t a = PositivesOneZero(f1, f2);
		uint32_t b = PositivesOneZeroOne(f1, f2, f3);
		return a - b;
	}

	uint32_t PenaltyBranchComputerDepth3::PositivesZeroOneOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return PositivesOneOneZero(f2, f3, f1);
	}

	uint32_t PenaltyBranchComputerDepth3::PositivesZeroOneZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return PositivesOneZeroZero(f2, f1, f3);
	}

	uint32_t PenaltyBranchComputerDepth3::PositivesZeroZeroOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return PositivesOneZeroZero(f3, f1, f2);
	}

	uint32_t PenaltyBranchComputerDepth3::PositivesZeroZeroZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		uint32_t a = PositivesZeroZero(f1, f2);
		uint32_t b = PositivesZeroZeroOne(f1, f2, f3);
		return a - b;
	}

	uint32_t PenaltyBranchComputerDepth3::PositivesOneOne(size_t f1, size_t f2)
	{
		return counts_.Positives(f1, f2);
	}

	uint32_t PenaltyBranchComputerDepth3::PositivesOneZero(size_t f1, size_t f2)
	{
		return counts_.Positives(f1) - counts_.Positives(f1, f2);
	}

	uint32_t PenaltyBranchComputerDepth3::PositivesZeroOne(size_t f1, size_t f2)
	{
		return counts_.Positives(f2) - counts_.Positives(f1, f2);
	}

	uint32_t PenaltyBranchComputerDepth3::PositivesZeroZero(size_t f1, size_t f2)
	{
		return num_positives_ - (counts_.Positives(f1) + counts_.Positives(f2) - PositivesOneOne(f1, f2));
	}

	//-----

	uint32_t PenaltyBranchComputerDepth3::NegativesOneOneOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return counts_.Negatives(f1, f2, f3);
	}

	uint32_t PenaltyBranchComputerDepth3::NegativesOneOneZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return NegativesOneOne(f1, f2) - NegativesOneOneOne(f1, f2, f3);
	}

	uint32_t PenaltyBranchComputerDepth3::NegativesOneZeroOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return NegativesOneOneZero(f1, f3, f2);
	}

	uint32_t PenaltyBranchComputerDepth3::NegativesOneZeroZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return NegativesOneZero(f1, f2) - NegativesOneZeroOne(f1, f2, f3);
	}

	uint32_t PenaltyBranchComputerDepth3::NegativesZeroOneOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return NegativesOneOneZero(f2, f3, f1);
	}

	uint32_t PenaltyBranchComputerDepth3::NegativesZeroOneZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return NegativesOneZeroZero(f2, f1, f3);
	}

	uint32_t PenaltyBranchComputerDepth3::NegativesZeroZeroOne(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return NegativesOneZeroZero(f3, f1, f2);
	}

	uint32_t PenaltyBranchComputerDepth3::NegativesZeroZeroZero(uint32_t f1, uint32_t f2, uint32_t f3)
	{
		return NegativesZeroZero(f1, f2) - NegativesZeroZeroOne(f1, f2, f3);
	}

	uint32_t PenaltyBranchComputerDepth3::NegativesOneOne(size_t f1, size_t f2)
	{
		return counts_.Negatives(f1, f2);
	}

	uint32_t PenaltyBranchComputerDepth3::NegativesOneZero(size_t f1, size_t f2)
	{
		return counts_.Negatives(f1) - counts_.Negatives(f1, f2);
	}

	uint32_t PenaltyBranchComputerDepth3::NegativesZeroOne(size_t f1, size_t f2)
	{
		return counts_.Negatives(f2) - counts_.Negatives(f1, f2);
	}

	uint32_t PenaltyBranchComputerDepth3::NegativesZeroZero(size_t f1, size_t f2)
	{
		return num_negatives_ - (counts_.Negatives(f1) + counts_.Negatives(f2) - NegativesOneOne(f1, f2));
	}
}
