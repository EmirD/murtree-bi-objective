#pragma once

#include "runtime_assert.h"

#include <stdint.h>

namespace DecisionTreeSolverMur
{
	struct DecisionNodeAssignment
	{
		DecisionNodeAssignment() :
			feature(UINT32_MAX)
		{
			Nullify();
		}

		DecisionNodeAssignment(uint32_t feature, uint32_t left_node_count, uint32_t right_node_count, uint32_t num_false_positives, uint32_t num_false_negatives) :
			feature(feature),
			left_node_count(left_node_count),
			right_node_count(right_node_count),
			num_false_positives(num_false_positives),
			num_false_negatives(num_false_negatives)
		{
			runtime_assert((feature != UINT32_MAX) || left_node_count + right_node_count == 0);
		}

		void Nullify()
		{
			feature = UINT32_MAX;
			num_false_positives = UINT32_MAX;
			num_false_negatives = UINT32_MAX;
			left_node_count = 0;
			right_node_count = 0;
		}

		uint32_t Misclassifications() const { return num_false_positives + num_false_negatives; }

		bool IsNull() const
		{
			return feature == UINT32_MAX && (num_false_positives == UINT32_MAX) && (left_node_count == 0) && (right_node_count == 0);
		}

		uint32_t NumNodes() const
		{
			assert(!IsNull());
			assert(feature != UINT32_MAX || (left_node_count + right_node_count == 0));
			return (feature != UINT32_MAX) + left_node_count + right_node_count;
		}

		bool IsBetterThan(DecisionNodeAssignment& comparing_assignment) const
		{
			if (this->Misclassifications() < comparing_assignment.Misclassifications()) { return true; }
			if (this->Misclassifications() > comparing_assignment.Misclassifications()) { return false; }
			return this->NumNodes() < comparing_assignment.NumNodes();
		}

		uint32_t feature;
		uint32_t num_false_positives, num_false_negatives;
		uint32_t left_node_count, right_node_count;
	};
}