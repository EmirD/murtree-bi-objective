#include "lower_bound_computer.h"

namespace DecisionTreeSolverMur
{
	LowerBoundComputer::LowerBoundComputer(uint32_t max_depth, uint32_t size, uint32_t num_instances):
		seen_old_(num_instances, false),
		seen_new_(num_instances, false),
		disabled_(false)
	{		
		Initialise(max_depth, size);
	}

	AssignmentContainer LowerBoundComputer::ComputeLowerBound(LearningData & data, uint32_t depth, uint32_t size)
	{
		DecisionNodeAssignment dummy(UINT32_MAX, 0, 0, 0, 0);
		AssignmentContainer lower_bound;
		lower_bound.AddAssignment(dummy);

		if (disabled_) { return lower_bound; }

		for (ArchieveEntry &archieve_entry : archive_[depth][size])
		{
			AssignmentContainer candidate_lb = ComputeLowerBoundBasedOnArchieveEntry(archieve_entry, data);
			lower_bound.MergeMaximisation(candidate_lb);
		}
		return lower_bound;
	}

	void LowerBoundComputer::UpdateArchive(LearningData & data, const AssignmentContainer& lower_bound, uint32_t depth, uint32_t size)
	{
		if (disabled_) { return; }

		if (archive_[depth].size() <= size) { return; } //temporary hack to allow caching nodes 2 and 3 upon computation

		if (IsThereSpaceForNewData(depth, size))
		{
			archive_[depth][size].push_back(ArchieveEntry(data, lower_bound));
		}
		else
		{
			GetMostSimilarStoredData(data, depth, size) = ArchieveEntry(data, lower_bound);
		}
	}

	void LowerBoundComputer::Initialise(uint32_t max_depth, uint32_t size)
	{
		if (disabled_) { return; }

		archive_ = std::vector<std::vector<std::vector<ArchieveEntry> > >(max_depth + 1, std::vector<std::vector<ArchieveEntry> >(size + 1));
	}

	void LowerBoundComputer::Disable()
	{
		disabled_ = true;
	}

	bool LowerBoundComputer::IsThereSpaceForNewData(uint32_t depth, uint32_t size)
	{
		return archive_[depth][size].size() < 2;
	}

	LowerBoundComputer::ArchieveEntry & LowerBoundComputer::GetMostSimilarStoredData(LearningData & reference_data, uint32_t depth, uint32_t size)
	{
		assert(archive_[depth][size].size() > 0);

		LowerBoundComputer::ArchieveEntry *best_entry = NULL;
		uint32_t best_similiarity_score = UINT32_MAX;
		for (ArchieveEntry &archieve_entry : archive_[depth][size])
		{
			uint32_t similiarity_score = ComputeSimiliarityMetrics(archieve_entry.data, reference_data).total_difference;
			if (similiarity_score < best_similiarity_score)
			{
				best_entry = &archieve_entry;
				best_similiarity_score = similiarity_score;
			}
		}
		assert(best_similiarity_score < UINT32_MAX);
		return *best_entry;
	}

	AssignmentContainer LowerBoundComputer::ComputeLowerBoundBasedOnArchieveEntry(ArchieveEntry & entry, LearningData & data)
	{
		//the first condition is necessary since we are using unsigned ints. Todo change unsigned to signed.
		//todo re-establish this check. It was meant to improve the performance by not computing for cases where there it can immediately be determined that the benefit is not there		
		//if (entry.data.Size() > data.Size() && entry.data.Size() - data.Size() >= entry.lower_bound) { return 0; }

		/*DecisionNodeAssignment dummy(UINT32_MAX, 0, 0, 0, 0);
		AssignmentContainer lower_bound;
		lower_bound.AddAssignment(dummy);
		return lower_bound;*/

		//for the single objective version that computes two components of the objective, this does not hold and should be disabled

		SimiliarityMetrics metrics = ComputeSimiliarityMetrics(entry.data, data);

		AssignmentContainer lb;		
		for (DecisionNodeAssignment assignment : entry.lower_bound.GetStoredAssignments())
		{
			//note: false positives happen because instances of the negative class are mislabelled. Here are assuming that every removed negative instance was incorrectly classified
			if (assignment.num_false_positives >= metrics.num_negative_class_removals)
			{
				assignment.num_false_positives -= metrics.num_negative_class_removals;
			}
			else
			{
				assignment.num_false_positives = 0;
			}

			//same but for the other class
			if (assignment.num_false_negatives >= metrics.num_positive_class_removals)
			{
				assignment.num_false_negatives -= metrics.num_positive_class_removals;
			}
			else
			{
				assignment.num_false_negatives = 0;
			}

			lb.AddAssignment(assignment);
		}
		return lb;
		/*uint32_t lb = entry.lower_bound;
		if (lb >= metrics.num_removals)
		{
			lb -= metrics.num_removals;
		}
		else
		{
			lb = 0;
		}
		return lb;*/
	}

	LowerBoundComputer::SimiliarityMetrics LowerBoundComputer::ComputeSimiliarityMetrics(LearningData & data_old, LearningData & data_new)
	{
		runtime_assert(data_old.NumLabels() == 2);

		uint32_t difference = 0;
		std::vector<uint32_t> label_removals(data_old.NumLabels(), 0);

		//todo could consider not going through every element but just those in the data. This way we iterate through the maximum number of instances each time.
		//could exploit the ordering of the IDs in the data to do it faster

		//initialise seen old
		for (uint32_t label = 0; label < data_old.NumLabels(); label++)
		{
			for (FeatureVectorBinary fv : data_old.instances[label])
			{
				seen_old_[fv.GetID()] = true;
			}
		}

		//initialise seen new
		for (uint32_t label = 0; label < data_new.NumLabels(); label++)
		{
			for (FeatureVectorBinary fv : data_new.instances[label])
			{
				seen_new_[fv.GetID()] = true;
			}
		}

		//increase the counter for each instance that the new data has but the old data has not
		for (uint32_t label = 0; label < data_new.NumLabels(); label++)
		{
			for (FeatureVectorBinary fv : data_new.instances[label])
			{
				difference += (seen_old_[fv.GetID()] == false);
			}
		}

		//increase the counter for each instances that the old data has but the new data has not
		uint32_t difference_before_counting_removals = difference;
		for (uint32_t label = 0; label < data_old.NumLabels(); label++)
		{
			for (FeatureVectorBinary fv : data_old.instances[label])
			{
				difference += (seen_new_[fv.GetID()] == false);
				label_removals[label] += (seen_new_[fv.GetID()] == false);
			}
		}

		SimiliarityMetrics metrics;
		metrics.total_difference = difference;
		metrics.num_removals = difference - difference_before_counting_removals;
		metrics.num_positive_class_removals = label_removals[1];
		metrics.num_negative_class_removals = label_removals[0];
		
		runtime_assert(metrics.num_positive_class_removals + metrics.num_negative_class_removals == metrics.num_removals);
	
		//ensure the helper vectors contain only false entries
		for (uint32_t label = 0; label < data_old.NumLabels(); label++)
		{
			for (FeatureVectorBinary fv : data_old.instances[label])
			{
				seen_old_[fv.GetID()] = false;
			}
		}

		for (uint32_t label = 0; label < data_new.NumLabels(); label++)
		{
			for (FeatureVectorBinary fv : data_new.instances[label])
			{
				seen_new_[fv.GetID()] = false;
			}
		}

		return metrics;
	}
}