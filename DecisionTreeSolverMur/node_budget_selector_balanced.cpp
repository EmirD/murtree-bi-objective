#include "node_budget_selector_balanced.h"
#include <math.h>

namespace DecisionTreeSolverMur
{
	NodeBudgetSelectorBalanced::NodeBudgetSelectorBalanced(DecisionTree * tree, DecisionNode * node) :
		NodeBudgetSelectorAbstract(tree, node)
	{
	}

	PairNodeBudget NodeBudgetSelectorBalanced::PopNextBudgetSplit()
	{
		PairNodeBudget next_option = possible_budget_splits_.back();
		possible_budget_splits_.pop_back();
		return next_option;
	}

	void NodeBudgetSelectorBalanced::Reset(uint32_t new_node_budget)
	{
		allowed_node_budget_ = new_node_budget;

		InitialiseOptions();
	}

	bool NodeBudgetSelectorBalanced::AreThereAnyOptionsLeft() const
	{
		return possible_budget_splits_.empty() == false;
	}

	void NodeBudgetSelectorBalanced::InitialiseOptions()
	{
		if (allowed_node_budget_ == 0) { return; }

		possible_budget_splits_.clear();

		uint32_t max_nodes_in_subtree = MaxNumNodesInChildSubTree();
		uint32_t min_nodes_in_subtree = allowed_node_budget_ - 1 - max_nodes_in_subtree; //minus one since we have to take into account this node
		for (uint32_t num_left_subtree = min_nodes_in_subtree; num_left_subtree <= max_nodes_in_subtree; num_left_subtree++)
		{
			possible_budget_splits_.push_back(PairNodeBudget(num_left_subtree, allowed_node_budget_ - 1 - num_left_subtree)); //the minus one to take into account the root node
		}

		std::sort(
			possible_budget_splits_.begin(), 
			possible_budget_splits_.end(),
			[](auto &p1, auto &p2) 
			{
				return abs(int(p1.left_node_budget) - int(p1.right_node_budget)) >= abs(int(p2.left_node_budget) - int(p2.right_node_budget)); 
			}
		);
	}
}