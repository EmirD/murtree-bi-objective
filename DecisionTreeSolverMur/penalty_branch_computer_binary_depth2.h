#pragma once

#include "symmetric_matrix_positive_negative_counter_2d.h"
#include "../feature_vector_difference_computer.h"
#include "../learning_data.h"

#include <stdint.h>

namespace DecisionTreeSolverMur
{
	class PenaltyBranchComputerBinaryDepth2
	{
	public:
		PenaltyBranchComputerBinaryDepth2(uint32_t num_features, uint32_t num_total_data_points);

		void Initialise(LearningData &data);

		bool IsNonDiscriminatoryFeature(uint32_t f);

		uint32_t PenaltyBranchOneOne(uint32_t f1, uint32_t f2);
		uint32_t PenaltyBranchOneZero(uint32_t f1, uint32_t f2);
		uint32_t PenaltyBranchZeroOne(uint32_t f1, uint32_t f2);
		uint32_t PenaltyBranchZeroZero(uint32_t f1, uint32_t f2);

		//private:
		uint32_t PositivesOneOne(size_t f1, size_t f2);
		uint32_t PositivesOneZero(size_t f1, size_t f2);
		uint32_t PositivesZeroOne(size_t f1, size_t f2);
		uint32_t PositivesZeroZero(size_t f1, size_t f2);

		uint32_t NegativesOneOne(size_t f1, size_t f2);
		uint32_t NegativesOneZero(size_t f1, size_t f2);
		uint32_t NegativesZeroOne(size_t f1, size_t f2);
		uint32_t NegativesZeroZero(size_t f1, size_t f2);

		void UpdateCounts(LearningData &data, int value);
		void UpdateCountsLabel(uint32_t label, LearningData &data, int value);

		FeatureVectorDifferenceComputer difference_computer_;
		int32_t num_positives_, num_negatives_;
		SymmetricMatrixPositiveNegativeCounter2D counts_;
	};
}