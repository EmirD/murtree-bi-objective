#pragma once

#include "../learning_data.h"
#include "branch_info.h"

namespace DecisionTreeSolverMur
{
	class CacheBranchAbstract
	{
	public:
		CacheBranchAbstract() {};

		virtual bool IsOptimalAssignmentCached(const BranchInfo &branch, uint32_t node_budget) = 0;
		virtual void StoreOptimalBranchAssignment(const BranchInfo &branch, const DecisionNodeAssignment &optimal_assignment,  uint32_t total_node_budget) = 0;
		virtual DecisionNodeAssignment RetrieveOptimalAssignment(const BranchInfo &branch, uint32_t node_budget) = 0;
		//virtual uint32_t RetrieveUpperBound(BranchInfo &branch, uint32_t node_budget) = 0;

		virtual void StoreBranchLowerBound(const BranchInfo &branch, uint32_t bound, uint32_t node_budget) = 0;
		virtual uint32_t RetrieveLowerBound(const BranchInfo &branch, uint32_t node_budget) = 0;

		virtual uint32_t RetrieveUpperBound(const BranchInfo &branch, uint32_t node_budget) = 0;

		virtual uint32_t NumEntries() const = 0;

		virtual void ClearLargeBranches(uint32_t num_nodes) = 0; //removes branches with num_nodes nodes or more. Used for incremental solving, when going from depth k to k+1, all branches of size k and greater must be removed
		virtual void DisableUpperBounding() = 0;
		virtual void DisableLowerBounding() = 0;
	};
}