#pragma once

#include "../learning_data.h"
#include "symmetric_matrix_positive_negative_counter_3d.h"

#include <stdint.h>
#include <vector>

namespace DecisionTreeSolverMur
{
	class PenaltyBranchComputerDepth3
	{
	public:
		PenaltyBranchComputerDepth3(uint32_t num_features);

		void Initialise(LearningData &data);

		uint32_t PenaltyBranchOneOneOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PenaltyBranchOneOneZero(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PenaltyBranchOneZeroOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PenaltyBranchOneZeroZero(uint32_t f1, uint32_t f2, uint32_t f3);

		uint32_t PenaltyBranchZeroOneOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PenaltyBranchZeroOneZero(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PenaltyBranchZeroZeroOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PenaltyBranchZeroZeroZero(uint32_t f1, uint32_t f2, uint32_t f3);

	private:

		uint32_t PositivesOneOneOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PositivesOneOneZero(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PositivesOneZeroOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PositivesOneZeroZero(uint32_t f1, uint32_t f2, uint32_t f3);

		uint32_t PositivesZeroOneOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PositivesZeroOneZero(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PositivesZeroZeroOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t PositivesZeroZeroZero(uint32_t f1, uint32_t f2, uint32_t f3);

		uint32_t PositivesOneOne(size_t f1, size_t f2);
		uint32_t PositivesOneZero(size_t f1, size_t f2);
		uint32_t PositivesZeroOne(size_t f1, size_t f2);
		uint32_t PositivesZeroZero(size_t f1, size_t f2);

		uint32_t NegativesOneOneOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t NegativesOneOneZero(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t NegativesOneZeroOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t NegativesOneZeroZero(uint32_t f1, uint32_t f2, uint32_t f3);

		uint32_t NegativesZeroOneOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t NegativesZeroOneZero(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t NegativesZeroZeroOne(uint32_t f1, uint32_t f2, uint32_t f3);
		uint32_t NegativesZeroZeroZero(uint32_t f1, uint32_t f2, uint32_t f3);

		uint32_t NegativesOneOne(size_t f1, size_t f2);
		uint32_t NegativesOneZero(size_t f1, size_t f2);
		uint32_t NegativesZeroOne(size_t f1, size_t f2);
		uint32_t NegativesZeroZero(size_t f1, size_t f2);

		int32_t num_positives_, num_negatives_;
		SymmetricMatrixPositiveNegativeCounter3D counts_;
	};
}