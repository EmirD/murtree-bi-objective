#include "biobjective_classification_tree_constructor_depth1.h"
#include "DecisionTreeSolverMur/runtime_assert.h"
#include "DecisionTreeSolverMur/assignment_container.h"

DecisionTreeSolverMur::BiobjectiveClassificationTreeConstructorDepth1::BiobjectiveClassificationTreeConstructorDepth1()
{
}

/*AssignmentContainer DecisionTreeSolverMur::BiobjectiveClassificationTreeConstructorDepth1::ComputeOptimalAssignments(LearningData& data)
{
	runtime_assert(data.NumLabels() == 2);

	InitialiseDataStructures(data);
	for (uint32_t feature = 0; feature < data.NumFeatures(); feature++)
	{
		ComputeTreeForRootFeature(feature, data);
	}
	return best_assignments_;
}*/

void DecisionTreeSolverMur::BiobjectiveClassificationTreeConstructorDepth1::ComputeTreeForRootFeature(uint32_t feature, LearningData& data)
{
	DecisionNodeAssignment assignment;
	assignment.feature = feature;
	assignment.left_node_count = 0;
	assignment.right_node_count = 0;

	uint32_t num_positive_instances_left = data.NumInstancesForLabel(1) - counts_positive_[feature];
	uint32_t num_negative_instances_left = data.NumInstancesForLabel(0) - counts_negative_[feature];

	uint32_t num_positive_instances_right = counts_positive_[feature];
	uint32_t num_negative_instances_right = counts_negative_[feature];

	//left node variations
	uint32_t left_label0_num_false_negatives = num_positive_instances_left;
	uint32_t left_label0_num_false_positives = 0;

	uint32_t left_label1_num_false_negatives = 0;
	uint32_t left_label1_num_false_positives = num_negative_instances_left;

	//right node variations
	uint32_t right_label0_num_false_negatives = num_positive_instances_right;
	uint32_t right_label0_num_false_positives = 0;

	uint32_t right_label1_num_false_negatives = 0;
	uint32_t right_label1_num_false_positives = num_negative_instances_right;


	//labels 0 0
	assignment.num_false_negatives = left_label0_num_false_negatives + right_label0_num_false_negatives;
	assignment.num_false_positives = left_label0_num_false_positives + right_label0_num_false_positives;
	best_assignments_.AddAssignment(assignment);

	//labels 0 1
	assignment.num_false_negatives = left_label0_num_false_negatives + right_label1_num_false_negatives;
	assignment.num_false_positives = left_label0_num_false_positives + right_label1_num_false_positives;
	best_assignments_.AddAssignment(assignment);

	//labels 1 0
	assignment.num_false_negatives = left_label1_num_false_negatives + right_label0_num_false_negatives;
	assignment.num_false_positives = left_label1_num_false_positives + right_label0_num_false_positives;
	best_assignments_.AddAssignment(assignment);

	//labels 1 1
	assignment.num_false_negatives = left_label1_num_false_negatives + right_label1_num_false_negatives;
	assignment.num_false_positives = left_label1_num_false_positives + right_label1_num_false_positives;
	best_assignments_.AddAssignment(assignment);
}

void DecisionTreeSolverMur::BiobjectiveClassificationTreeConstructorDepth1::InitialiseDataStructures(LearningData& data)
{
	best_assignments_.Clear();
	
	counts_negative_ = std::vector<uint32_t>(data.NumFeatures(), 0); //counts_[label][feature] shows the number of times the feature appears in instances with the label
	for (const FeatureVectorBinary& fv : data[0])
	{
		for (uint32_t feature : fv)
		{
			counts_negative_[feature]++;
		}
	}

	counts_positive_ = std::vector<uint32_t>(data.NumFeatures(), 0);
	for (const FeatureVectorBinary& fv : data[1])
	{
		for (uint32_t feature : fv)
		{
			counts_positive_[feature]++;
		}
	}
}
