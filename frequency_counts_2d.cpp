#include "frequency_counts_2d.h"

#include <memory>
#include <assert.h>
#include <iostream>

namespace DecisionTreeSolverMur
{
	FrequencyCounts2D::FrequencyCounts2D(uint32_t num_labels, uint32_t num_features):
		counts_(0),
		num_labels_(num_labels),
		num_features_(num_features)
	{
		counts_ = new uint32_t[NumElements()];
		ResetToZeros();
	}

	FrequencyCounts2D::~FrequencyCounts2D()
	{
		delete[] counts_;
		counts_ = 0;
	}

	uint32_t FrequencyCounts2D::operator()(uint32_t label, uint32_t feature1, uint32_t feature2) const
	{
		assert(feature1 <= feature2);
		uint32_t index = num_labels_ * IndexSymmetricMatrix(feature1, feature2) + label;
		return counts_[index];
	}

	uint32_t &FrequencyCounts2D::operator()(uint32_t label, uint32_t feature1, uint32_t feature2)
	{
		assert(feature1 <= feature2);
		uint32_t index = num_labels_ * IndexSymmetricMatrix(feature1, feature2) + label;
		return counts_[index];
	}

	void FrequencyCounts2D::ResetToZeros()
	{
		for (uint32_t i = 0; i < NumElements(); i++) { counts_[i] = 0; }
		//memset(data2d_, 0, sizeof(uint32_t)*NumElements());
	}

	bool FrequencyCounts2D::operator==(const FrequencyCounts2D & reference)
	{
		if (num_features_ != reference.num_features_) { return false; }

		for (uint32_t i = 0; i < NumElements(); i++)
		{
			if (counts_[i] != reference.counts_[i]) { return false; }
		}
		return true;
	}

	uint32_t FrequencyCounts2D::NumElements() const
	{
		//the matrix is symmetric, and effectively each entry stores num_labels entries, corresponding the counts for each label
		return num_labels_*((num_features_ * (num_features_ + 1))/2);
	}

	uint32_t FrequencyCounts2D::IndexSymmetricMatrix(uint32_t index_row, uint32_t index_column) const
	{
		assert(index_row <= index_column);
		return num_features_ * index_row + index_column - index_row * (index_row + 1) / 2;
	}

	std::ostream & operator<<(std::ostream & os, const FrequencyCounts2D & fq)
	{
		os << fq.num_labels_ << ", " << fq.num_features_ << "\n";
		for (uint32_t i = 0; i < fq.num_labels_; i++)
		{
			os << "label " << i << "\n";
			for (uint32_t j = 0; j < fq.num_features_; j++)
			{
				for (uint32_t k = j; k < fq.num_features_; k++)
				{
					os << "\t" << j << ", " << k << ": " << fq(i, j, k) << "\n";
				}
			}
		}
		return os;
	}
}