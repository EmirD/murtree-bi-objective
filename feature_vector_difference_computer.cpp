#include "feature_vector_difference_computer.h"

namespace DecisionTreeSolverMur
{
	FeatureVectorDifferenceComputer::FeatureVectorDifferenceComputer(uint32_t num_labels, uint32_t num_features, uint32_t num_feature_vectors) :
		seen_last_time_(num_feature_vectors, false),
		feature_vector_present_(num_feature_vectors, false),
		data_last_time_(num_labels, num_features),
		data_to_remove_(num_labels, num_features),
		data_to_add_(num_labels, num_features),
		num_labels_(num_labels),
		num_features_(num_features)
	{
	}

	void FeatureVectorDifferenceComputer::Update(LearningData & data)
	{
		assert(seen_last_time_.size() > 0);

		for (size_t i = 0; i < feature_vector_present_.size(); i++) { feature_vector_present_[i] = false; }
		data_to_add_.Clear();
		data_to_remove_.Clear();

		//update the 'to add' feature vectors
		for (uint32_t label = 0; label < num_labels_; label++)
		{
			for (const FeatureVectorBinary &fv : data[label])
			{
				feature_vector_present_[fv.GetID()] = true;
				if (!seen_last_time_[fv.GetID()]) { data_to_add_.AddFeatureVector(fv, label); }
			}
		}

		//update the 'to remove' feature vectors
		for (uint32_t label = 0; label < num_labels_; label++)
		{
			for (const FeatureVectorBinary &fv : data_last_time_[label])
			{
				if (!feature_vector_present_[fv.GetID()]) { data_to_remove_.AddFeatureVector(fv, label); };
			}
		}

		seen_last_time_ = feature_vector_present_;
		data_last_time_ = data;
	}

	uint32_t FeatureVectorDifferenceComputer::ProbeDifference(LearningData & data)
	{
		//hax todo
		if (seen_last_time_.size() == 0) { return UINT32_MAX; }

		uint32_t difference = 0;

		for (size_t i = 0; i < feature_vector_present_.size(); i++) { feature_vector_present_[i] = false; }

		for (uint32_t label = 0; label < num_labels_; label++)
		{
			for (const FeatureVectorBinary &fv : data[label])
			{
				feature_vector_present_[fv.GetID()] = true;
				difference += !seen_last_time_[fv.GetID()];
			}
		}

		for (uint32_t label = 0; label < num_labels_; label++)
		{
			for (const FeatureVectorBinary &fv : data_last_time_[label])
			{
				difference += !feature_vector_present_[fv.GetID()];
			}
		}

		return difference;
	}

	uint32_t FeatureVectorDifferenceComputer::NumDifferences()
	{
		return data_to_add_.Size() + data_to_remove_.Size();
	}
}
