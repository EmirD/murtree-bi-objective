#pragma once

#include "learning_data.h"
#include "machine_learning_dataset.h"
#include "DecisionTreeSolverMur/decision_tree_learning.h"

#include <fstream>
#include <string>
#include <sstream>

namespace DecisionTreeSolverMur
{
	class FileReader
	{
	public:

		struct FeatureVectorLabelPair
		{
			FeatureVectorLabelPair(FeatureVectorBinary fv, uint32_t label) :features(fv), label(label) {}
			FeatureVectorBinary features;
			uint32_t label;
		};

		struct LearningDataRaw
		{
			uint32_t num_labels, num_features;
			std::vector<FeatureVectorLabelPair> data;
		};

		static LearningData ReadDataHao(std::string filename, bool verbose = true)
		{
			std::cout << "No supported, need to reimplement\n";
			exit(1);
			/*std::ifstream file(filename.c_str());
			assert(file);

			if (!file) { std::cout << "Error: File " << filename << " does not exist!\n"; exit(1); }

			if (verbose) std::cout << filename << std::endl;

			LearningData data;

			char c('a');
			size_t num_features = 0;
			while (c != 'l' && c != 'r' && c != 't') {
				file >> c;
				num_features += (c == ',');
			}

			if (c == 'l')
			{
				file >> c; assert(c == 'a');
				file >> c; assert(c == 'b');
				file >> c; assert(c == 'e');
				file >> c; assert(c == 'l');
			}
			else if (c == 'r')
			{
				file >> c; assert(c == 'e');
				file >> c; assert(c == 's');
			}
			else if (c == 't')
			{
				file >> c; assert(c == 'a');
				file >> c; assert(c == 'r');
				file >> c; assert(c == 'g');
				file >> c; assert(c == 'e');
				file >> c; assert(c == 't');
			}
			else
			{
				std::cout << "some problem with the input file, probably the name of the label\n";
				system("pause");
				exit(1);
			}

			while (file >> c)
			{
				assert(c == '0' || c == '1');
				std::vector<bool> row;
				row.push_back(c == '1');
				for (size_t i = 0; i < num_features; i++)
				{
					file >> c;
					assert(c == ',');
					file >> c;
					assert(c == '0' || c == '1');
					row.push_back(c == '1');
				}
				//the last entry is actually the label, not a feature
				bool label = row.back();
				row.pop_back();
				FeatureVectorBinary *features = new FeatureVectorBinary(std::vector<bool>(row));
				if (label)
				{
					data.positives.push_back(features);
				}
				else
				{
					data.negatives.push_back(features);
				}
			}

			if (verbose) data.PrintStats();

			return data;*/
		}

		static LearningData ReadDataDL(std::string filename, bool verbose = true)
		{
			LearningDataRaw raw_data = ReadDataDL_Raw(filename);			
			LearningData data(raw_data.num_labels, raw_data.num_features);
			for (auto &p : raw_data.data) { data.AddFeatureVector(p.features, p.label); }
			if (verbose) { data.PrintStats(); }
			return data;
		}
		
		//assumes a five fold split
		//the split file names are 'splits_location_prefix' + '_train[i].txt', where [i] in [0, 4]. The test are analoguous but 'test' instead of 'train'
		//the split files contain indicies of the instances in data
		static std::vector<TrainTestSplit> ReadSplits(std::string data_file, std::string splits_location_prefix);

		private:

			static LearningData ReadSplits_ReadPartitionFile(LearningDataRaw &data, std::string file);	
			static LearningDataRaw ReadDataDL_Raw(std::string filename)
			{
				//the first value is the label
				std::ifstream file(filename.c_str());
				assert(file);

				if (!file) { std::cout << "Error: File " << filename << " does not exist!\n"; exit(1); }

				std::cout << filename << std::endl;

				std::string line;
				std::set<uint32_t> labels;
				LearningDataRaw raw_data;
				raw_data.num_features = UINT32_MAX;
				while (std::getline(file, line))
				{
					assert(raw_data.num_features == UINT32_MAX || raw_data.num_features == (line.length() - 1) / 2);
					if (raw_data.num_features == UINT32_MAX) { raw_data.num_features = (line.length() - 1) / 2; }

					std::istringstream iss(line);

					uint32_t label;
					iss >> label;
					labels.insert(label);
					std::vector<bool> v;
					for (uint32_t i = 0; i < raw_data.num_features; i++)
					{
						uint32_t temp;
						iss >> temp;
						assert(temp == 0 || temp == 1);
						v.push_back(temp);
					}					
					raw_data.data.push_back(FeatureVectorLabelPair(FeatureVectorBinary(v), label));
				}
				raw_data.num_labels = labels.size();
				return raw_data;
			}
	};
}