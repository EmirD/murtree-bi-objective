#include "biobjective_tree_depth1_container.h"

BiObjectiveTreeDepth1Container::BiObjectiveTreeDepth1Container()
{
}

//todo improve the efficiency of this method
void BiObjectiveTreeDepth1Container::AddTree(BiObjectiveTreeDepth1& input_tree)
{
	//check if this solution is already dominated by some other solution
	for (BiObjectiveTreeDepth1& stored_tree : stored_trees_)
	{
		//once we discover that a strictly better solution is already present, we abort
		if (IsTreeDominated(input_tree, stored_tree)) { return; }
	}

	//solution is not dominated, we can add it to the pool of solutions
	//	and possibly remove other dominated solutions
	std::vector<BiObjectiveTreeDepth1> old_trees(stored_trees_);
	stored_trees_.clear();
	stored_trees_.push_back(input_tree);
	//readd the old solutions only if they are not dominated by the input
	//	note that old solutions do not dominate each other
	for (BiObjectiveTreeDepth1& old_tree : old_trees)
	{
		if (IsTreeDominated(old_tree, input_tree) == false)
		{
			stored_trees_.push_back(old_tree);
		}
	}
}

void BiObjectiveTreeDepth1Container::Clear()
{
	stored_trees_.clear();
}

std::vector<BiObjectiveTreeDepth1>& BiObjectiveTreeDepth1Container::GetStoresTrees()
{
	return stored_trees_;
}

bool BiObjectiveTreeDepth1Container::IsTreeDominated(BiObjectiveTreeDepth1& tree1, BiObjectiveTreeDepth1& tree2)
{
	return tree1.num_false_positives >= tree2.num_false_positives &&
		tree1.num_false_negatives >= tree2.num_false_negatives;
}
