#include "optimal_decision_tree_constructor_depth2.h"

namespace DecisionTreeSolverMur
{
	OptimalDecisionTreeConstructorDepth2::OptimalDecisionTreeConstructorDepth2(uint32_t num_labels, uint32_t num_features, uint32_t num_total_data_points) :
		num_features_(num_features),
		optimal_feature_(UINT32_MAX),
		optimal_penalty_(UINT32_MAX),
		penalty_computer_(num_labels, num_features, num_total_data_points),
		children_info_(num_features)
	{
	}

	void OptimalDecisionTreeConstructorDepth2::ComputeOptimalDecisionTree(LearningData & data)
	{
		InitialiseDataStructures(data);

		for (uint32_t feature1 = 0; feature1 < num_features_; feature1++)
		{
			for (uint32_t feature2 = feature1 + 1; feature2 < num_features_; feature2++)
			{
				uint32_t penalty_branch_one_one = penalty_computer_.PenaltyBranchOneOne(feature1, feature2);
				uint32_t penalty_branch_one_zero = penalty_computer_.PenaltyBranchOneZero(feature1, feature2);
				uint32_t penalty_branch_zero_one = penalty_computer_.PenaltyBranchZeroOne(feature1, feature2);
				uint32_t penalty_branch_zero_zero = penalty_computer_.PenaltyBranchZeroZero(feature1, feature2);

				UpdateBestLeftChild(feature1, feature2, penalty_branch_zero_one + penalty_branch_zero_zero);
				UpdateBestRightChild(feature1, feature2, penalty_branch_one_one + penalty_branch_one_zero);
				UpdateBestPenalty(feature1);

				UpdateBestLeftChild(feature2, feature1, penalty_branch_one_zero + penalty_branch_zero_zero);
				UpdateBestRightChild(feature2, feature1, penalty_branch_one_one + penalty_branch_zero_one);
				UpdateBestPenalty(feature2);
			}
		}
	}

	//todo fix the penalty counter hax
	SolutionNode OptimalDecisionTreeConstructorDepth2::ComputeOptimalDecisionTreeNodeBudget(LearningData & data, uint32_t max_node_budget)
	{
		assert(max_node_budget == 2 || max_node_budget == 3);

		InitialiseDataStructures(data);

		if (penalty_computer_.difference_computer_.seen_last_time_.size() > 0
			&& penalty_computer_.difference_computer_.NumDifferences() == 0)
		{
			if (max_node_budget == 2)
			{
				return last_sol_at_most_two_nodes;
			}
			else
			{
				assert(max_node_budget == 3);
				return last_sol_at_most_three_nodes;
			}
		}

		BestPenalties best_penalties(data.ComputeClassificationCost());

		uint32_t best_feature_one_node = UINT32_MAX;
		uint32_t best_feature_two_node_root = UINT32_MAX;
		uint32_t best_feature_two_node_left = UINT32_MAX;
		uint32_t best_feature_two_node_right = UINT32_MAX;

		for (uint32_t feature1 = 0; feature1 < num_features_; feature1++)
		{
			//update the penalty using one node
			uint32_t penalty_one_node_left = penalty_computer_.PenaltyBranchZeroZero(feature1, feature1);
			uint32_t penalty_one_node_right = penalty_computer_.PenaltyBranchOneOne(feature1, feature1);
			uint32_t penalty_one_node = penalty_one_node_left + penalty_one_node_right;
			if (penalty_one_node < best_penalties.one_node)
			{
				best_feature_one_node = feature1;
				best_penalties.one_node = penalty_one_node;
			}
			//move on to more than one node
			for (uint32_t feature2 = feature1 + 1; feature2 < num_features_; feature2++)
			{
				uint32_t penalty_branch_one_one = penalty_computer_.PenaltyBranchOneOne(feature1, feature2);
				uint32_t penalty_branch_one_zero = penalty_computer_.PenaltyBranchOneZero(feature1, feature2);
				uint32_t penalty_branch_zero_one = penalty_computer_.PenaltyBranchZeroOne(feature1, feature2);
				uint32_t penalty_branch_zero_zero = penalty_computer_.PenaltyBranchZeroZero(feature1, feature2);

				UpdateBestLeftChild(feature1, feature2, penalty_branch_zero_one + penalty_branch_zero_zero);
				UpdateBestRightChild(feature1, feature2, penalty_branch_one_one + penalty_branch_one_zero);
				UpdateBestPenalty(feature1);

				UpdateBestLeftChild(feature2, feature1, penalty_branch_one_zero + penalty_branch_zero_zero);
				UpdateBestRightChild(feature2, feature1, penalty_branch_one_one + penalty_branch_zero_one);
				UpdateBestPenalty(feature2);

				//find the best option using only two nodes

				if (best_penalties.two_nodes > children_info_[feature1].left_child_penalty + penalty_one_node_right)
				{
					best_penalties.two_nodes = children_info_[feature1].left_child_penalty + penalty_one_node_right;
					best_feature_two_node_root = feature1;
					best_feature_two_node_left = children_info_[feature1].left_child_feature;
					best_feature_two_node_right = UINT32_MAX;
				}

				if (best_penalties.two_nodes > children_info_[feature1].right_child_penalty + penalty_one_node_left)
				{
					best_penalties.two_nodes = children_info_[feature1].right_child_penalty + penalty_one_node_left;
					best_feature_two_node_root = feature1;
					best_feature_two_node_left = UINT32_MAX;
					best_feature_two_node_right = children_info_[feature1].right_child_feature;
				}

				//feature2 as root
				uint32_t feature2_root_right_feature = children_info_[feature2].right_child_penalty + penalty_computer_.PenaltyBranchZeroZero(feature2, feature2);
				if (best_penalties.two_nodes > feature2_root_right_feature)
				{
					best_penalties.two_nodes = feature2_root_right_feature;
					best_feature_two_node_root = feature2;
					best_feature_two_node_left = UINT32_MAX;
					best_feature_two_node_right = children_info_[feature2].right_child_feature;
				}

				uint32_t feature2_root_left_feature = children_info_[feature2].left_child_penalty + penalty_computer_.PenaltyBranchOneOne(feature2, feature2);
				if (best_penalties.two_nodes > feature2_root_left_feature)
				{
					best_penalties.two_nodes = feature2_root_left_feature;
					best_feature_two_node_root = feature2;
					best_feature_two_node_left = children_info_[feature2].left_child_feature;
					best_feature_two_node_right = UINT32_MAX;
				}
			}
		}
		if (max_node_budget == 3) { best_penalties.three_nodes = GetOptimalPenalty(); }

		//default assumes using no feature is best
		SolutionNode sol;
		sol.total_penalty = best_penalties.zero_nodes;

		//check the cost using one node
		if (sol.total_penalty > best_penalties.one_node)
		{
			sol.feature = best_feature_one_node;
			sol.total_penalty = best_penalties.one_node;
		}

		//check the cost using two nodes
		if (sol.total_penalty > best_penalties.two_nodes)
		{
			assert((best_feature_two_node_left == UINT32_MAX) + (best_feature_two_node_right == UINT32_MAX) == 1);

			sol.feature = best_feature_two_node_root;
			sol.total_penalty = best_penalties.two_nodes;

			if (best_feature_two_node_left != UINT32_MAX)
			{
				sol.nodes_left = 1;
				sol.feature_left_child = best_feature_two_node_left;
			}
			else
			{
				sol.nodes_left = 0;
				sol.feature_left_child = UINT32_MAX;
			}

			if (best_feature_two_node_right != UINT32_MAX)
			{
				sol.nodes_right = 1;
				sol.feature_right_child = best_feature_two_node_right;
			}
			else
			{
				sol.nodes_right = 0;
				sol.feature_right_child = UINT32_MAX;
			}
		}

		last_sol_at_most_two_nodes = sol;

		if (sol.total_penalty > best_penalties.three_nodes)
		{
			sol.total_penalty = best_penalties.three_nodes;
			sol.feature = optimal_feature_;
			sol.nodes_left = 1;
			sol.nodes_right = 1;

			sol.feature_left_child = children_info_[sol.feature].left_child_feature;
			sol.feature_right_child = children_info_[sol.feature].right_child_feature;
		}

		last_sol_at_most_three_nodes = sol;

		if (max_node_budget == 2) { return last_sol_at_most_two_nodes; }
		else { assert(max_node_budget == 3); return last_sol_at_most_three_nodes; }
	}

	uint32_t OptimalDecisionTreeConstructorDepth2::GetOptimalPenalty() const
	{
		assert(optimal_penalty_ != UINT32_MAX);
		return optimal_penalty_;
	}

	uint32_t OptimalDecisionTreeConstructorDepth2::GetOptimalFeatureForRoot() const
	{
		return optimal_feature_;
	}

	void OptimalDecisionTreeConstructorDepth2::InitialiseDataStructures(LearningData & data)
	{
		optimal_feature_ = UINT32_MAX;
		optimal_penalty_ = UINT32_MAX;
		InitialiseChildrenInfo();
		penalty_computer_.Initialise(data);
	}

	void OptimalDecisionTreeConstructorDepth2::InitialiseChildrenInfo()
	{
		//memset(&children_info_[0], UINT32_MAX, sizeof(ChildrenInformation)*num_features_);
		for (uint32_t f = 0; f < num_features_; f++)
		{
			children_info_[f].left_child_feature = UINT32_MAX;
			children_info_[f].left_child_penalty = UINT32_MAX;;
			children_info_[f].right_child_feature = UINT32_MAX;
			children_info_[f].right_child_penalty = UINT32_MAX;
		}
	}

	void OptimalDecisionTreeConstructorDepth2::UpdateBestLeftChild(uint32_t root_feature, uint32_t left_child_feature, uint32_t penalty)
	{
		if (children_info_[root_feature].left_child_penalty > penalty)
		{
			children_info_[root_feature].left_child_feature = left_child_feature;
			children_info_[root_feature].left_child_penalty = penalty;
		}
	}

	void OptimalDecisionTreeConstructorDepth2::UpdateBestRightChild(uint32_t root_feature, uint32_t right_child_feature, uint32_t penalty)
	{
		if (children_info_[root_feature].right_child_penalty > penalty)
		{
			children_info_[root_feature].right_child_feature = right_child_feature;
			children_info_[root_feature].right_child_penalty = penalty;
		}
	}

	void OptimalDecisionTreeConstructorDepth2::UpdateBestPenalty(uint32_t root_feature)
	{
		assert(children_info_[root_feature].left_child_penalty != UINT32_MAX);
		assert(children_info_[root_feature].right_child_penalty != UINT32_MAX);

		uint32_t new_penalty = children_info_[root_feature].left_child_penalty + children_info_[root_feature].right_child_penalty;
		if (optimal_penalty_ > new_penalty)
		{
			optimal_penalty_ = new_penalty;
			optimal_feature_ = root_feature;
		}
	}	
}