#pragma once

#include "learning_data.h"
#include "biobjective_tree_depth1_container.h"
#include "DecisionTreeSolverMur/assignment_container.h"

namespace DecisionTreeSolverMur
{
	class BiobjectiveClassificationTreeConstructorDepth1
	{
	public:
		BiobjectiveClassificationTreeConstructorDepth1();

		AssignmentContainer ComputeOptimalAssignments(LearningData& data)
		{
			runtime_assert(data.NumLabels() == 2);

			InitialiseDataStructures(data);
			for (uint32_t feature = 0; feature < data.NumFeatures(); feature++)
			{
				ComputeTreeForRootFeature(feature, data);
			}
			return best_assignments_;
		}
		
	private:

		void InitialiseDataStructures(LearningData& data);
		void ComputeTreeForRootFeature(uint32_t feature, LearningData &data);

		std::vector<uint32_t> counts_positive_, counts_negative_; //counts_[label][feature] shows the number of times the feature appears in instances with the label
		//BiObjectiveTreeDepth1Container best_trees_;
		AssignmentContainer best_assignments_;
	};
}