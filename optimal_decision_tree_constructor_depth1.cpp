#include "optimal_decision_tree_constructor_depth1.h"

namespace DecisionTreeSolverMur
{
	OptimalDecisionTreeConstructorDepth1::OptimalDecisionTreeConstructorDepth1() :
		num_labels_(0),
		num_features_(0)
	{}

	void OptimalDecisionTreeConstructorDepth1::ComputeOptimalDecisionTree(LearningData & data)
	{
		InitialiseDataStructures(data);

		for (uint32_t feature = 0; feature < num_features_; feature++)
		{
			TreeDepth1 sol = GetTreeInfoForRootFeature(feature);
			if (sol.Misclassifications() < best_tree_.Misclassifications())
			{
				best_tree_ = sol;
			}
		}
	}

	uint32_t OptimalDecisionTreeConstructorDepth1::GetOptimalPenalty() const
	{
		return best_tree_.Misclassifications();
	}

	uint32_t OptimalDecisionTreeConstructorDepth1::GetOptimalFeature() const
	{
		return best_tree_.root_feature;
	}

	OptimalDecisionTreeConstructorDepth1::TreeDepth1 OptimalDecisionTreeConstructorDepth1::GetTreeInfoForRootFeature(uint32_t feature)
	{
		TreeDepth1 sol(feature, UINT32_MAX, UINT32_MAX, UINT32_MAX, UINT32_MAX);
		uint32_t num_instances_left = 0, num_instances_right = 0;
		for (uint32_t label = 0; label < num_labels_; label++)
		{
			num_instances_left += (label_counts_[label] - feature_counts_[label][feature]);
			num_instances_right += feature_counts_[label][feature];
		}
		assert(num_instances_left + num_instances_right == num_instances_);
		
		for (uint32_t label = 0; label < num_labels_; label++)
		{
			assert(label_counts_[label] >= feature_counts_[label][feature]);
						
			//uint32_t miscla_left = label_counts_[label] - feature_counts_[label][feature];
			uint32_t miscla_left = num_instances_left - (label_counts_[label] - feature_counts_[label][feature]);
			if (miscla_left < sol.left_misclassifications)
			{
				sol.left_label = label;
				sol.left_misclassifications = miscla_left;
			}

			//uint32_t miscla_right = feature_counts_[label][feature];
			uint32_t miscla_right = num_instances_right - feature_counts_[label][feature];
			if (miscla_right < sol.right_misclassifications)
			{
				sol.right_label = label;
				sol.right_misclassifications = miscla_right;
			}
		}
		return sol;
	}

	void OptimalDecisionTreeConstructorDepth1::InitialiseDataStructures(LearningData & data)
	{
		//assign zero to all counts
		num_labels_ = data.NumLabels();
		num_features_ = data.NumFeatures();
		feature_counts_.resize(data.NumLabels());
		label_counts_.resize(data.NumLabels());
		num_instances_ = 0;
		for (uint32_t label = 0; label < num_labels_; label++)
		{
			num_instances_ += data.NumInstancesForLabel(label);
			label_counts_[label] = data.NumInstancesForLabel(label);
			feature_counts_[label].resize(data.NumFeatures());
			for (uint32_t feature = 0; feature < num_features_; feature++) { feature_counts_[label][feature] = 0; }
		}

		best_tree_ = TreeDepth1();

		for (uint32_t label = 0; label < num_labels_; label++)
		{
			for (const FeatureVectorBinary &fv : data[label])
			{
				for (uint32_t feature : fv)
				{
					feature_counts_[label][feature]++;
				}
			}
		}
	}
}
