#pragma once

#include "learning_data.h"

namespace DecisionTreeSolverMur
{
	class OptimalDecisionTreeConstructorDepth1
	{
	public:

		struct TreeDepth1
		{
			TreeDepth1():
				root_feature(UINT32_MAX),
				left_misclassifications(UINT32_MAX), 
				right_misclassifications(UINT32_MAX), 
				left_label(UINT32_MAX),
				right_label(UINT32_MAX){}
			
			
			TreeDepth1(uint32_t root_feature, uint32_t left_miscla, uint32_t right_miscla, uint32_t left_label, uint32_t right_label):
				root_feature(root_feature),
				left_misclassifications(left_miscla),
				right_misclassifications(right_miscla),
				left_label(left_label),
				right_label(right_label) {}
			
			uint32_t Misclassifications() const { return left_misclassifications + right_misclassifications; }
			
			uint32_t root_feature;
			uint32_t left_misclassifications, right_misclassifications;
			uint32_t left_label, right_label;
		};

		OptimalDecisionTreeConstructorDepth1();

		void ComputeOptimalDecisionTree(LearningData &data);
		uint32_t GetOptimalPenalty() const;
		uint32_t GetOptimalFeature() const;
		TreeDepth1 GetTreeInfoForRootFeature(uint32_t feature);

	private:

		void InitialiseDataStructures(LearningData &data);

		uint32_t num_labels_, num_features_;
		uint32_t num_instances_;
		std::vector<uint32_t> label_counts_;
		std::vector<std::vector<uint32_t> > feature_counts_; //counts_[label][feature] shows the number of times the feature appears in instances with the label
		TreeDepth1 best_tree_;
	};
}